package com.abacusdesk.instafeed.instafeed.Adpater;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Models.DataFavorites;
import com.abacusdesk.instafeed.instafeed.Models.DataFavorites;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Instafeed2 on 9/4/2019.
 */

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.MyViewHolder> {
    Context context;
    ArrayList<DataFavorites> data;
    public static String comment = "", comment_id;
    Dialog dialog;
    String upperString;
    String token, username;
    @NonNull
    @Override
    public FavoritesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorites_item_view, parent, false);
        token = SaveSharedPreference.getToken(context);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoritesAdapter.MyViewHolder holder, int position) {
        Picasso.with(context).load(data.get(position).getAvatar()).into(holder.imge);

        holder.name.setText(data.get(position).getFirst_name());
      //  holder.dis.setText("REDEEM WITH " + data.get(position).getTotalPoints() + " pts");
        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "follow", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public FavoritesAdapter(ArrayList<DataFavorites> data, Context context) {
        this.context = context;
        this.data = data;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, follow;
        public ImageButton imge;

        public MyViewHolder(View view) {
            super(view);
            this.imge = (ImageButton) itemView.findViewById(R.id.icon_fav);
            this.name = (TextView) itemView.findViewById(R.id.title_fav);
            this.follow = (TextView) itemView.findViewById(R.id.follow_fav);
        }
    }
}
