package com.abacusdesk.instafeed.instafeed.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.abacusdesk.instafeed.instafeed.Adpater.TabAdapter;
import com.abacusdesk.instafeed.instafeed.Fragments.Follow;
import com.abacusdesk.instafeed.instafeed.Fragments.Followers;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.R;

public class Followandfollowers  extends FragmentActivity {
    private TabAdapter adapter;
    private TabLayout tabLayoutF;
    private ViewPager viewPagerF;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.folloandfollwing);
        viewPagerF = (ViewPager)  findViewById(R.id.viewPager);
        tabLayoutF = (TabLayout)  findViewById(R.id.tabLayout);
        findViewById(R.id.fo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.Close();
                finish();
            }
        });
        adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new Follow(), "followers");
        adapter.addFragment(new Followers(), "following");
        viewPagerF.setAdapter(adapter);
        tabLayoutF.setupWithViewPager(viewPagerF);
    }
}
