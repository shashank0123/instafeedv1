package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class isfollowmodel {
    @SerializedName("status")
   
    private Integer status;
    @SerializedName("message")
   
    private String message;
    @SerializedName("data")
   
    private DataFoll data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataFoll getData() {
        return data;
    }

    public void setData(DataFoll data) {
        this.data = data;
    }
}
