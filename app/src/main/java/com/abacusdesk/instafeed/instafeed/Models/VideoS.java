package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class VideoS {
    @SerializedName("id")
    
    private String id;
    @SerializedName("video")
    
    private String video;
    @SerializedName("video_thumb")
    
    private String videoThumb;
    @SerializedName("vimeo_video_id")
    
    private Object vimeoVideoId;
    @SerializedName("vimeo_response")
    
    private Object vimeoResponse;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    public Object getVimeoVideoId() {
        return vimeoVideoId;
    }

    public void setVimeoVideoId(Object vimeoVideoId) {
        this.vimeoVideoId = vimeoVideoId;
    }

    public Object getVimeoResponse() {
        return vimeoResponse;
    }

    public void setVimeoResponse(Object vimeoResponse) {
        this.vimeoResponse = vimeoResponse;
    }
}
