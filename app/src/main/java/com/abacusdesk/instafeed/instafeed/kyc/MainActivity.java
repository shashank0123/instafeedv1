package com.abacusdesk.instafeed.instafeed.kyc;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.abacusdesk.instafeed.instafeed.R;

/**
 * Created by Instafeed2 on 9/5/2019.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.brand);
    }
}
