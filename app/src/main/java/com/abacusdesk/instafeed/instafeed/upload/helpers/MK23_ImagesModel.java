package com.abacusdesk.instafeed.instafeed.upload.helpers;

import android.net.Uri;

public class MK23_ImagesModel {
    Uri imageURI;

    public MK23_ImagesModel(Uri imageURI) {
        this.imageURI = imageURI;
    }

    public Uri getImageURI() {
        return imageURI;
    }
}
