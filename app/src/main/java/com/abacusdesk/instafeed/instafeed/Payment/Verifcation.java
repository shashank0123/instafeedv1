package com.abacusdesk.instafeed.instafeed.Payment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.abacusdesk.instafeed.instafeed.R;

/**
 * Created by Instafeed2 on 7/16/2019.
 */

public class Verifcation extends Activity {
    LinearLayout email,check,pan,account,mobile;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification_screen);
        email=(LinearLayout)findViewById(R.id.email);
        check=(LinearLayout)findViewById(R.id.check);
        pan=(LinearLayout)findViewById(R.id.pan);
        account=(LinearLayout)findViewById(R.id.account);
        mobile=(LinearLayout)findViewById(R.id.mobile);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Verifcation.this,AccountDetails.class);
                startActivity(in);
            }
        });
        mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Verifcation.this,MobileVerification.class);
                startActivity(in);
            }
        });





        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Verifcation.this,EmailVerfication.class);
                startActivity(in);
            }
        });
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Verifcation.this,CheckUpload.class);
                startActivity(in);
            }
        });
        pan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Verifcation.this,PanVerification.class);
                startActivity(in);
            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
