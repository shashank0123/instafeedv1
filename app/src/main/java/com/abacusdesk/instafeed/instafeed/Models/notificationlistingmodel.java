package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class notificationlistingmodel {

    @SerializedName("status")
         
    private Integer status;
    @SerializedName("message")
         
    private String message;
    @SerializedName("notify_count")
         
    private String notifyCount;
    @SerializedName("data")
         
    private ArrayList<DataNoti> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNotifyCount() {
        return notifyCount;
    }

    public void setNotifyCount(String notifyCount) {
        this.notifyCount = notifyCount;
    }

    public ArrayList<DataNoti> getData() {
        return data;
    }

    public void setData(ArrayList<DataNoti> data) {
        this.data = data;
    }
}
