package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.Toast;


import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.Models.followlistingmodel;
import com.abacusdesk.instafeed.instafeed.Mycode.CommonMethod;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Responsemodel.forgotmodel;
import com.abacusdesk.instafeed.instafeed.Responsemodel.signupresponse;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.notification.Config;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends FragmentActivity implements  GoogleApiClient.OnConnectionFailedListener {
   // TextInputLayout inputfname
    LinearLayout social,emaillogin,l1,l2;
    TextInputEditText name,emailid,mobile,password;
    Button signupB;
    String token;
    Button facebook,google;
    private CallbackManager callbackManager;
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 100;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String fname="",lname="",semail="",socialid="",provider="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.signup);
        try{
            callbackManager = CallbackManager.Factory.create();
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            //Initializing google api client
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }catch (Exception e){

        }
findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        finish();
    }
});
        facebook=(Button)findViewById(R.id.facebook);
        google=(Button)findViewById(R.id.google);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facaBook();
            }
        });
        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInS();
            }
        });
        social=(LinearLayout)findViewById(R.id.social);
        emaillogin=(LinearLayout)findViewById(R.id.emaillogin);
         signupB=(Button)findViewById(R.id.signup);
        name=(TextInputEditText)findViewById(R.id.name);
        emailid=(TextInputEditText)findViewById(R.id.emailid);
        mobile=(TextInputEditText)findViewById(R.id.mobile);
        password=(TextInputEditText)findViewById(R.id.password);
        TextInputEditText editor = new TextInputEditText(SignUp.this);
        editor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        l1=(LinearLayout)findViewById(R.id.l1);
        l2=(LinearLayout)findViewById(R.id.l2);
        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                l1.setBackgroundResource(R.drawable.leftside);
                l2.setBackgroundResource(R.drawable.rightside);
                hideupper(v);
            }
        });
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                l2.setBackgroundResource(R.drawable.rightsidegray);
                l1.setBackgroundResource(R.drawable.leftsidewithe);
                hidelower(v);
            }
        });
        signupB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name.getText().toString().trim();
                emailid.getText().toString().trim();
                mobile.getText().toString().trim();
                password.getText().toString().trim();

                Signup();
            }
        });
    }
    public void hideupper(View v) {
        social.setVisibility(View.GONE);
        emaillogin.setVisibility(View.VISIBLE);
    }
    public void hidelower(View v) {
        social.setVisibility(View.VISIBLE);
        emaillogin.setVisibility(View.GONE);
    }
    private void Signup() {

        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(SignUp.this, RequestInterface.class);
        RequestBody Name =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, name.getText().toString().trim());
        RequestBody Email =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, emailid.getText().toString().trim());
        RequestBody Mobile =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, mobile.getText().toString().trim());

        RequestBody Password =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, password.getText().toString().trim());
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, "Gaurav123456");

        Call<signupresponse> call = requestInterface.Signup(Name, Email, Mobile,Password,Token);
        call.enqueue(new Callback<signupresponse>() {
            @Override
            public void onResponse(Call<signupresponse> call,
                                   Response<signupresponse> response) {
                if (response.isSuccessful()){
                    signupresponse loginResponse = response.body();
                    if (!loginResponse.isError()) {
                        SaveSharedPreference.setToken(SignUp.this, response.body().getData().getToken());
                        Intent in = new Intent(SignUp.this, acontytype.class);
                        startActivity(in);
                        finish();
                    } else {
                        Log.e("loginResponse1",loginResponse.toString());
                        Toast.makeText(SignUp.this, loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }else{
                    CommonMethod.showAlert1("Email already exists", SignUp.this);
                    ResponseBody errorBody = response.errorBody();
                    Log.e("loginResponse3",errorBody.toString());
                }
               /* if (response.body().getMessage().toString().equals("success")) {
                    Log.e("Strring", "TEST"+response.body().getData().toString());
                    SaveSharedPreference.setToken(SignUp.this,response.body().getData().getToken());
                } else {
                    Log.e("String", "TEST111");
                }*/
            }

            @Override
            public void onFailure(Call<signupresponse> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });

    }
    public void onTokenRefresh() {

        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        token=refreshedToken;
        final Intent intent = new Intent("tokenReceiver");
        // You can also include some extra data.
        final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        intent.putExtra("token", refreshedToken);
        broadcastManager.sendBroadcast(intent);
          Log.e("token",token);

    }
    public void facaBook() {

        LoginManager.getInstance().logInWithReadPermissions(SignUp.this, Arrays.asList("public_profile", "user_friends", "email"));
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("access", "" + AccessToken.getCurrentAccessToken().getToken().toString());
                RequestData1();
            }

            @Override
            public void onCancel() {
                //Toast.makeText(Login.this, "Login cancelled by user!", Toast.LENGTH_LONG).show();
                System.out.println("Facebook Login failed!!");
            }

            @Override
            public void onError(FacebookException exception) {
                // Toast.makeText(Login.this, "Facebook Login failed!!", Toast.LENGTH_LONG).show();
                System.out.println("Facebook Login failed!!");
                System.out.println("Facebook Login " + exception.toString());
                if (exception instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });
    }


    public void RequestData1() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                Log.e("Log_json", "" + json);
                try {
                    if (json != null) {
                        fname= json.getString("first_name");
                        lname= json.getString("last_name");
                        socialid=  json.getString("id");
                        provider="facebook";
                        if (json.has("email")) {
                            semail=json.getString("email");
                        }
                        Log.e("responsesuccess",""+fname+lname+socialid+provider);
                        socialLogin();

                        // ed_last.setText(json.getString("last_name"));
                    }
                } catch (Exception e) {
                    // e.printStackTrace();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void socialLogin() {
        RequestInterface requestInterface = ApiFactory.createService(SignUp.this, RequestInterface.class);
        RequestBody Sc = RequestBody.create(okhttp3.MultipartBody.FORM, socialid);
        RequestBody Pro = RequestBody.create(okhttp3.MultipartBody.FORM, provider);
        RequestBody Fname =RequestBody.create(okhttp3.MultipartBody.FORM, fname);
        RequestBody LName =RequestBody.create(okhttp3.MultipartBody.FORM, lname);
        RequestBody EName =RequestBody.create(okhttp3.MultipartBody.FORM, semail);
        RequestBody Device =RequestBody.create(okhttp3.MultipartBody.FORM, "device");
        Call<forgotmodel> call = requestInterface.social(Sc, Pro,Fname,LName,EName,Device);
        call.enqueue(new Callback<forgotmodel>() {
            @Override
            public void onResponse(Call<forgotmodel> call, Response<forgotmodel> response) {

                if (response.body().getMessage().equals("success")) {
                    try {
                        checkSocial(response.body().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CommonMethod.showAlert(response.body().getData().getMessage().toString().trim(),  SignUp.this);
                } else {

                }

            }
            @Override
            public void onFailure(Call<forgotmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    private void signInS() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling add new function to handle signin
            Log.e("res", "" + result.getStatus());
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.d("social googlweplus",""+acct.getId()+acct.getGivenName()+acct.getEmail());
            socialid=acct.getId();
            fname=acct.getGivenName();
            lname="";
            semail= acct.getEmail();
            provider="google";

            socialLogin();

            try{
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            }catch (Exception e){

            }
        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }

    }
    private void checkSocial(String response) throws JSONException {
        JSONObject jsonObject= new JSONObject(response);
        if(!jsonObject.getJSONObject("data").has("error")){
            Log.e("log", "" + response);
            Toast.makeText(SignUp.this,"Successfully logged into", Toast.LENGTH_SHORT).show();
            SaveSharedPreference.setUserID(SignUp.this,jsonObject.getJSONObject("data").getString("user_id"));
            SaveSharedPreference.setPrefToken(SignUp.this,jsonObject.getJSONObject("data").getString("token"));
            SaveSharedPreference.setUserIMAGE(SignUp.this,"");
            /*   if (email!=null && email.matches("[0-9]+")){*/
            SaveSharedPreference.setMobileLogin(SignUp.this,"true");
            /*}else{
                SaveSharedPreference.setMobileLogin(login.this,"false");
            }*/
            startActivity(new Intent(SignUp.this,acontytype.class));
            finish();
        }
        else if (jsonObject.getJSONObject("data").has("error")){
            //  Dialogs.showDialog(Login_Activity.this, jsonObject.getJSONObject("data").getString("error"));
        } else {
            //  Dialogs.showDialog(Login_Activity.this, "Server Failed");
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void Sub() {

        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(SignUp.this, RequestInterface.class);
        RequestBody Token=RequestBody.create(MultipartBody.FORM,token);

        Call<followlistingmodel> call = requestInterface.followwer(Token);
        call.enqueue(new Callback<followlistingmodel>() {
            @Override
            public void onResponse(Call<followlistingmodel> call, final Response<followlistingmodel> response) {
                if(response.isSuccessful()){
                    Intent in = new Intent(SignUp.this, MainActivity.class);
                    startActivity(in);
                    finish();
                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                }else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }
            @Override
            public void onFailure(Call<followlistingmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}
