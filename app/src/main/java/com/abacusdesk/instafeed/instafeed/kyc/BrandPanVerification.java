package com.abacusdesk.instafeed.instafeed.kyc;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.R;

import java.time.chrono.AbstractChronology;

/**
 * Created by Instafeed2 on 8/23/2019.
 */

public class BrandPanVerification  extends AppCompatActivity{
    ImageView back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.brand_pan_verify);
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
