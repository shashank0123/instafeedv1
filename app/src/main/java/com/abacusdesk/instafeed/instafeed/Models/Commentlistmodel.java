package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Commentlistmodel {
    @SerializedName("status")
     
    private Integer status;
    @SerializedName("message")
     
    private String message;
    @SerializedName("data")
     
    private ArrayList<DATAComment> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DATAComment> getData() {
        return data;
    }

    public void setData(ArrayList<DATAComment> data) {
        this.data = data;
    }

}
