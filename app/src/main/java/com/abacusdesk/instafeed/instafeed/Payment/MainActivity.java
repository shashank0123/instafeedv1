package com.abacusdesk.instafeed.instafeed.Payment;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;

import com.abacusdesk.instafeed.instafeed.Activity.Account;
import com.abacusdesk.instafeed.instafeed.R;
import com.rilixtech.CountryCodePicker;

public class MainActivity extends Activity {
    CountryCodePicker ccp;
    AppCompatEditText edtPhoneNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payments);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.paymethod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this,PaymentsMethods.class);
                startActivity(in);
            }
        });

        findViewById(R.id.account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, AccountDetails.class);
                startActivity(in);
            }
        });
        findViewById(R.id.very).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, Verifcation.class);
                startActivity(in);
            }
        });


     //   ccp = (CountryCodePicker) findViewById(R.id.ccp);
      //  edtPhoneNumber = (AppCompatEditText) findViewById(R.id.phone_number_edt);
      //  ccp.registerPhoneNumberTextView(edtPhoneNumber);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
