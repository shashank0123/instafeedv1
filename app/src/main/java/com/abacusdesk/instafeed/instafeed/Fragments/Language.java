package com.abacusdesk.instafeed.instafeed.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.Seachactivity;
import com.abacusdesk.instafeed.instafeed.Adpater.Feedadapter;
import com.abacusdesk.instafeed.instafeed.Adpater.Langadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.Models.languages;
import com.abacusdesk.instafeed.instafeed.R;

public class Language extends Fragment {
    // TextInputLayout inputfname
    LinearLayout social, emaillogin, l1, l2;
RecyclerView lang;
    Langadapter mLangadapter;
    ViewGroup header;
    ImageView menu;
    TextView Search;
   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
       View view;
       view = inflater.inflate(R.layout.language, container, false);
       lang=(RecyclerView)view.findViewById(R.id.lang);
       LinearLayoutManager manager = new LinearLayoutManager(getActivity());
       lang.setLayoutManager(manager);

       header=(ViewGroup)view.findViewById(R.id.header);
       menu=(ImageView) header.findViewById(R.id.menu);

       Search=(TextView) header.findViewById(R.id.Search);
       Search.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent in = new Intent(getActivity(), Seachactivity.class);
               startActivity(in);
           }
       });
       menu.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               MainActivity.Open();
           }
       });
       language();
       return view;
   }
    private void language() {
        RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        ErrorCallback.MyCall<languages> myCall;
        myCall = requestInterface.getlang();
        Log.e("response ", myCall.toString());
        myCall.enqueue(new ErrorCallback.MyCallback<languages>() {
            @Override
            public void success(final retrofit2.Response<languages> response) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body().getMessage().equals("success")) {
                            mLangadapter= new Langadapter(getActivity(), response.body().getData());
                            lang.setAdapter(mLangadapter);
                        } else {


                        }
                    }
                });
                // Showing Alert Message


            }

            @Override
            public void error(String errorMessage) {

            }
        }, getActivity(), Boolean.TRUE);
    }
}
