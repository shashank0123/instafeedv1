package com.abacusdesk.instafeed.instafeed.Responsemodel;

import com.google.gson.annotations.SerializedName;

public class signupresponse {
    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    /*   {
            "status": 200,
                "message": "success",
                "data": {
            "token": "893fe331b8c1f251391ef0f90bf8bc4e0355edda446f248c2c1e1c12e4f47630cf43b1ffee60d2e78dcb43b67c2c2e5094d89eff97405dce132aded5030286b2",
                    "token_updated": "2019-07-18 13:27:09",
                    "token_expires": "2019-07-19 01:27:09"
        }
        }*/
 private boolean error;

    @SerializedName("status")
    
    private Integer status;
    @SerializedName("message")
    
    private String message;
    @SerializedName("data")
    
    private DataSignup data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataSignup getData() {
        return data;
    }

    public void setData(DataSignup data) {
        this.data = data;
    }
}
