package com.abacusdesk.instafeed.instafeed.Adpater;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Models.DATACAT;
import com.abacusdesk.instafeed.instafeed.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CatlistingAdapter extends RecyclerView.Adapter<CatlistingAdapter.ViewHolder> {
    private ArrayList<DATACAT> arrayList;
    Context context;
    public CatlistingAdapter(Context context, ArrayList<DATACAT> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.catlistingrow, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Glide.with(context).load(arrayList.get(i).getImage()).
                into(viewHolder.cat);
        viewHolder.namecat.setText(arrayList.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView namecat;
        ImageView cat;
        public ViewHolder(View itemView) {
            super(itemView);
            cat=(ImageView)itemView.findViewById(R.id.cat);
            namecat=(TextView)itemView.findViewById(R.id.namecat);
        }
    }
}
