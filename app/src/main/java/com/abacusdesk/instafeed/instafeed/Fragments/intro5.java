package com.abacusdesk.instafeed.instafeed.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.abacusdesk.instafeed.instafeed.Activity.login;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
public class intro5 extends Fragment {
    Button SignInb,SignInb1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.letsgoscreen, container, false);
        ImageView im=(ImageView)view.findViewById(R.id.im);
        im.setImageResource(R.drawable.one);
        SignInb=(Button)view.findViewById(R.id.SignInb);
        SignInb1=(Button)view.findViewById(R.id.SignInb1);
        SignInb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveSharedPreference.setIsSplash(getActivity(),"true");
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });
        SignInb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveSharedPreference.setIsSplash(getActivity(),"true");
                startActivity(new Intent(getActivity(), login.class));
            }
        });

        return view;
    }
}