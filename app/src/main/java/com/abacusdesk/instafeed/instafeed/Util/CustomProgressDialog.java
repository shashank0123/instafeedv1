package com.abacusdesk.instafeed.instafeed.Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.abacusdesk.instafeed.instafeed.R;

/**
 * Created by Instafeed2 on 8/3/2018.
 */

public class CustomProgressDialog extends ProgressDialog {

    private AnimationDrawable animation;
    private ImageView animate;
    private Context mContext;

    public static ProgressDialog ctor(Context context) {

        CustomProgressDialog dialog = new CustomProgressDialog(context);

        dialog.setIndeterminate(true);

        dialog.setCancelable(false);

        return dialog;
    }

    public CustomProgressDialog(Context context) {

        super(context);
        this.mContext = context;
    }

    public CustomProgressDialog(Context context, int theme) {

        super(context, theme);
        this.mContext = context;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        /*animate = (ImageView) findViewById(R.id.animation);
        animate.setBackgroundResource(R.drawable.loader_animation);
		animation = (AnimationDrawable) animate.getBackground();
		animation.start();*/

    }

    @Override
    protected void onCreate(Bundle savedinstancestate) {

        super.onCreate(savedinstancestate);

        setContentView(R.layout.progress_layout);
        ProgressBar progBar = (ProgressBar) findViewById(R.id.animation);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        if (progBar != null) {
            progBar.setIndeterminate(true);


            progBar.getIndeterminateDrawable().setColorFilter(mContext.getResources().getColor(R.color.orange), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

    }

    @Override
    public void show() {

        super.show();

        // animation.start();
    }

    @Override
    public void dismiss() {

        super.dismiss();

        if (animation != null && animation.isRunning())
            animation.stop();

    }
}