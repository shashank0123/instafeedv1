package com.abacusdesk.instafeed.instafeed.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ApiURL;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;

import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.Models.profilemodel;
import com.abacusdesk.instafeed.instafeed.Mycode.CommonMethod;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.AndroidMultiPartEntity;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Instafeed2 on 8/21/2019.
 */

public class ProfileActivity extends Activity {
    private Uri mCropImageUri;
    Bitmap bitmap;
    ImageView imgprofile;
    int statusCode;
    RadioButton maleRadio, FemaleRadio;
    Button buttonprofile;
    Boolean flag = true;
    TextInputEditText fullname, dob, add1, add2, country, pincode, state, accName, accNo, ifsccode;
    private  String sex="",type="";
    private RadioGroup radioGroupSex,radioAccountType;
    RadioButton radioSexButton,radioButtonAccountType;
    private String birthday;
String token;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_page);
        buttonprofile = (Button) findViewById(R.id.btn_profile);
        token= SaveSharedPreference.getToken(ProfileActivity.this);
        ImageView imgpick = (ImageView) findViewById(R.id.img_selectphoto);
        imgprofile = (ImageView) findViewById(R.id.img_profile);
        fullname = (TextInputEditText) findViewById(R.id.fullname_text);
        dob = (TextInputEditText) findViewById(R.id.dob_text);
        add1 = (TextInputEditText) findViewById(R.id.address1_text);
        add2 = (TextInputEditText) findViewById(R.id.address2_text);
        state = (TextInputEditText) findViewById(R.id.country_text);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.Close();
                finish();
            }
        });
        country = (TextInputEditText) findViewById(R.id.city_text);
        pincode = (TextInputEditText) findViewById(R.id.pincode_text);
        accName = (TextInputEditText) findViewById(R.id.accname_text);
        ifsccode = (TextInputEditText) findViewById(R.id.ifsc_text);
        accNo = (TextInputEditText) findViewById(R.id.accno_text);
        maleRadio = (RadioButton) findViewById(R.id.male_radio);
        FemaleRadio = (RadioButton) findViewById(R.id.female_radio);
        radioGroupSex = (RadioGroup)findViewById(R.id.radioSex) ;
        radioAccountType = (RadioGroup)findViewById(R.id.radioAccountType) ;

        fullname.setEnabled(false);
        dob.setEnabled(false);
        add1.setEnabled(false);
        country.setEnabled(false);
        for(int i = 0; i < radioGroupSex.getChildCount(); i++){
            ((RadioButton)radioGroupSex.getChildAt(i)).setEnabled(false);
        }
        for(int i = 0; i < radioAccountType.getChildCount(); i++){
            ((RadioButton)radioAccountType.getChildAt(i)).setEnabled(false);
        }

        add2.setEnabled(false);
        state.setEnabled(false);
        pincode.setEnabled(false);
        accName.setEnabled(false);
        ifsccode.setEnabled(false);
        accNo.setEnabled(false);
        buttonprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag=!flag;
                if (flag == false) {
                    buttonprofile.setText("Save");
                    buttonprofile.setBackgroundResource(R.drawable.btn_bglight);
                    fullname.setEnabled(true);
                    dob.setEnabled(true);
                    add1.setEnabled(true);
                    country.setEnabled(true);
                    for(int i = 0; i < radioAccountType.getChildCount(); i++){
                        ((RadioButton)radioAccountType.getChildAt(i)).setEnabled(true);
                    }
                    for(int i = 0; i < radioGroupSex.getChildCount(); i++){
                        ((RadioButton)radioGroupSex.getChildAt(i)).setEnabled(true);
                    }
                    add2.setEnabled(true);
                    state.setEnabled(true);
                    pincode.setEnabled(true);
                    accName.setEnabled(true);
                    ifsccode.setEnabled(true);
                    accNo.setEnabled(true);


                }else
                {
                    buttonprofile.setText("Edit");
                    if(!fullname.getText().toString().isEmpty())
                    {
                        int selectedId = radioGroupSex.getCheckedRadioButtonId();

                        // find the radiobutton by returned id
                        radioSexButton = (RadioButton) findViewById(selectedId);
                        Log.e("radioSexButton",""+radioSexButton.getText().toString());
                        if(radioSexButton.getText().toString().equalsIgnoreCase("male"))
                        {
                            sex ="M";
                        }else
                        {
                            sex ="F";

                        }
                        int selectedId2 = radioAccountType.getCheckedRadioButtonId();

                        // find the radiobutton by returned id
                        radioButtonAccountType = (RadioButton) findViewById(selectedId2);
                        Log.e("radioSexButton",""+radioButtonAccountType.getText().toString());
                        if(radioButtonAccountType.getText().toString().equalsIgnoreCase("savings"))
                        {
                            type ="s";
                        }else
                        {
                            type ="a";

                        }
                        if (isConnected(ProfileActivity.this)) {
                            try{
                                Log.e("actual",""+dob.getText().toString());
                                birthday= sendDateNTime(dob.getText().toString());
                                Log.e("birthday",""+birthday);
                            }catch (Exception e){

                            }
                            Log.e("locationid", " " );
                            sendUserdetails();

                        }

                    }

                }
            }
        });


        imgpick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectImageClick(v);
                //   pickImage();
            }
        });
        USERDEtail();
    }
    public static String sendDateNTime(String dateRaw) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date da = null;
        Log.e("dateraw",""+formatter);
        try {
            da = formatter.parse(dateRaw);
            Log.e("da",""+da);
        } catch (ParseException e) {

        }
        return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(da);
    }
    public void sendUserdetails() {
        // Dialogs.showProDialog(NavDrawerActivity.this, "Loading");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiURL.update_profile,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("log", "" + response);
                        fullname.setEnabled(false);
                        dob.setEnabled(false);
                        add1.setEnabled(false);
                        country.setEnabled(false);
                        for(int i = 0; i < radioAccountType.getChildCount(); i++){
                            ((RadioButton)radioAccountType.getChildAt(i)).setEnabled(false);
                        }
                        for(int i = 0; i < radioGroupSex.getChildCount(); i++){
                            ((RadioButton)radioGroupSex.getChildAt(i)).setEnabled(false);
                        }
                        add2.setEnabled(false);
                        state.setEnabled(false);
                        pincode.setEnabled(false);
                        accName.setEnabled(false);
                        ifsccode.setEnabled(false);
                        accNo.setEnabled(false);
                       /* try {
                     //       responseUpdate(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendUserdetails();
                            }
                        };
                        if (error instanceof TimeoutError) {
                        } else if (error instanceof NoConnectionError) {
                        }
                    }
                })
        {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("user", SaveSharedPreference.getUserID(ProfileActivity.this));
                map.put("token", token);
                map.put("first_name",fullname.getText().toString());
                map.put("last_name",fullname.getText().toString());
                map.put("username",fullname.getText().toString());
                map.put("birth_date ","22-11-1996");
                map.put("location_id","id");
                map.put("sex", sex);
                map.put("pincode",pincode.getText().toString());
                map.put("address",add1.getText().toString());
                map.put("bank_name", accName.getText().toString().trim());
                map.put("bank_ifsc_code", ifsccode.getText().toString().trim());
                map.put("bank_acc_no", accNo.getText().toString().trim());
                map.put("bank_acc_type", type);
                map.put("city","abc");

                Log.e("updated details",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        requestQueue.add(stringRequest);
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null)
            return false;
        return networkInfo.isConnectedOrConnecting();
    }

    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {


                //   fileUri = data.getData();
                try {
                    if (result.getUri() != null) {
                        bitmap = MediaStore.Images.Media.getBitmap(ProfileActivity.this.getContentResolver(), result.getUri());
                        //bitmap=changeorientation(fileUri.getPath());

                        Log.e("bitmap", "" + bitmap);
                        imgprofile.setImageBitmap(bitmap);
                        //imgdemo.setVisibility(View.GONE);
                        new UploadFileToServer().execute();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private long totalSize;

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            // setting progress bar to zero

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
            byte[] data = bos.toByteArray();

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(ApiURL.update_pic);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                ByteArrayBody bab = new ByteArrayBody(data, "bt.jpg");
                entity.addPart("image", bab);
                entity.addPart("token", new StringBody(token));
                Log.e("token", "" + new StringBody(SaveSharedPreference.getPrefToken(ProfileActivity.this)));
                Log.e("image", "" + bab.getFilename() + " size :" + bab.getContentLength());
                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(r_entity);
                JSONObject jsonObject = new JSONObject(responseString);
                Log.e("respon", "" + responseString);

                if (statusCode == 200) {
                    // Server response
                    try {
                        SaveSharedPreference.setUserIMAGE(ProfileActivity.this, jsonObject.getJSONObject("data")
                                .getJSONObject("avatar").getString("200x200"));
                    } catch (Exception e) {

                    }
                } else if (statusCode == 201) {
                    // Server response
                    try {
                        SaveSharedPreference.setUserIMAGE(ProfileActivity.this, jsonObject.getJSONObject("data")
                                .getJSONObject("avatar").getString("200x200"));
                    } catch (Exception e) {

                    }

                } else {
                    responseString = "Error occurred! Http Status Code: " +
                            EntityUtils.toString(r_entity) + statusCode;
                }
            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (Exception e) {
                responseString = e.toString();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (statusCode == 200) {
                    Toast.makeText(ProfileActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                    //if (bitmap==null)
                    if (bitmap != null) {
                        if (!SaveSharedPreference.getUserIMAGE(ProfileActivity.this).isEmpty()) {
                            Glide.with(ProfileActivity.this).load(SaveSharedPreference.getUserIMAGE(ProfileActivity.this)).error(R.drawable.user).into(imgprofile);
                        }
                    }
                    Log.e("bitmap2", "" + bitmap);
                    //  NavDrawerActivity.updateProfile(ProfileActivity.this);
                } else if (statusCode == 201) {
                    Toast.makeText(ProfileActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                } else {

                }
            } catch (Exception e) {

            }
        }

    }


    private void USERDEtail() {
        Log.e("token", "" + SaveSharedPreference.getToken(ProfileActivity.this));
        //   RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(ProfileActivity.this, RequestInterface.class);

        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);

        Call<profilemodel> call = requestInterface.userprofile(Token);
        call.enqueue(new Callback<profilemodel>() {
            @Override
            public void onResponse(Call<profilemodel> call,
                                   Response<profilemodel> response) {
                Log.e("response", "response :" + response.raw());
                if (response.isSuccessful()) {
                    Log.e("response", "response :" + response.body().getMessage());

                    if (response.body().getMessage().equals("success")) {
                        Log.e("response", "response :" + response.body().getData().toString());
                        fullname.setText(response.body().getData().getFirstName());
                        dob.setText(response.body().getData().getBirthDate());
                        add1.setText(response.body().getData().getAddress());
                        country.setText(response.body().getData().getCity());
                        pincode.setText(response.body().getData().getPincode());
                        accNo.setText(response.body().getData().getBankAccNo());
                        accName.setText(response.body().getData().getBankName());
                        ifsccode.setText(response.body().getData().getBankIfscCode());
                        sex = response.body().getData().getSex();
                        if(sex.equalsIgnoreCase("M"))
                        {
                            radioGroupSex.check(R.id.male_radio);
                        }else
                        {
                            radioGroupSex.check(R.id.female_radio);

                        }
                        type = response.body().getData().getBankAccType();
                        if(type.equalsIgnoreCase("s"))
                        {
                            radioAccountType.check(R.id.saving_radio);
                        }else
                        {
                            radioAccountType.check(R.id.current_radio);

                        }
                        //    if(response.body().getData().getSex())

                        Picasso.with(ProfileActivity.this).load(response.body().getData().getAvatar()).into(imgprofile);

                        SaveSharedPreference.setUserName(ProfileActivity.this, response.body().getData().getUsername());
                        SaveSharedPreference.setFirstName(ProfileActivity.this, response.body().getData().getFirstName());
                        SaveSharedPreference.setLastName(ProfileActivity.this, response.body().getData().getLastName());
                        SaveSharedPreference.setUserEMAIL(ProfileActivity.this, response.body().getData().getEmail());
                        SaveSharedPreference.setUserID(ProfileActivity.this, response.body().getData().getUserId());
                        SaveSharedPreference.setUserIMAGE(ProfileActivity.this, response.body().getData().getAvatar());
                        SaveSharedPreference.setFollowers(ProfileActivity.this, response.body().getData().getTotals().getTotalFollowers());
                        SaveSharedPreference.setFollowing(ProfileActivity.this, response.body().getData().getTotals().getTotalFollowing());


                    } else {


                    }
                } else {
                    CommonMethod.showAlert2("Invalid Token", ProfileActivity.this);

                }
            }

            @Override
            public void onFailure(Call<profilemodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

}
