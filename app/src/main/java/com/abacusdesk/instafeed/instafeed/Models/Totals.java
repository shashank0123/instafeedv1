package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class Totals {
    @SerializedName("total_news")
       
    private String totalNews;
    @SerializedName("total_issues")
       
    private String totalIssues;
    @SerializedName("total_polls")
       
    private String totalPolls;
    @SerializedName("total_blogs")
       
    private String totalBlogs;
    @SerializedName("total_petitions")
       
    private String totalPetitions;
    @SerializedName("total_followers")
       
    private String totalFollowers;
    @SerializedName("total_following")
       
    private String totalFollowing;

    public String getTotalNews() {
        return totalNews;
    }

    public void setTotalNews(String totalNews) {
        this.totalNews = totalNews;
    }

    public String getTotalIssues() {
        return totalIssues;
    }

    public void setTotalIssues(String totalIssues) {
        this.totalIssues = totalIssues;
    }

    public String getTotalPolls() {
        return totalPolls;
    }

    public void setTotalPolls(String totalPolls) {
        this.totalPolls = totalPolls;
    }

    public String getTotalBlogs() {
        return totalBlogs;
    }

    public void setTotalBlogs(String totalBlogs) {
        this.totalBlogs = totalBlogs;
    }

    public String getTotalPetitions() {
        return totalPetitions;
    }

    public void setTotalPetitions(String totalPetitions) {
        this.totalPetitions = totalPetitions;
    }

    public String getTotalFollowers() {
        return totalFollowers;
    }

    public void setTotalFollowers(String totalFollowers) {
        this.totalFollowers = totalFollowers;
    }

    public String getTotalFollowing() {
        return totalFollowing;
    }

    public void setTotalFollowing(String totalFollowing) {
        this.totalFollowing = totalFollowing;
    }

}
