package com.abacusdesk.instafeed.instafeed.Activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.DatuDeta;
import com.abacusdesk.instafeed.instafeed.Models.ImageS;
import com.abacusdesk.instafeed.instafeed.Models.VideoS;
import com.abacusdesk.instafeed.instafeed.Models.citizendetailmodel;
import com.abacusdesk.instafeed.instafeed.Models.followunfollowmodel;
import com.abacusdesk.instafeed.instafeed.Models.isfollowmodel;
import com.abacusdesk.instafeed.instafeed.Models.likemodel;
import com.abacusdesk.instafeed.instafeed.Models.votestatus;
import com.abacusdesk.instafeed.instafeed.Mycode.WrappingViewPager;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.gaurav.GauravNewViewpager;
import com.abacusdesk.instafeed.instafeed.gaurav.GauravViewpager;
import com.abacusdesk.instafeed.instafeed.webview.VideoEnabledWebChromeClient;
import com.abacusdesk.instafeed.instafeed.webview.VideoEnabledWebView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Citizendetailpage extends AppCompatActivity {
    ImageView image;
    HashMap<String, String> hashMap;
    TextView tof_textview1, tof_textview2, main_heading, date_text, main_text, comments_readpost, views_readpost, imagebuttotn2;
    ImageView imagebuttotn1, likes, back;
    String postid, Type;
    CardView comments;
    String username;
    ImageButton imageButton, shhh;
    String VoteST = "";
    DatuDeta datuDetas;
    ArrayList<String> vedios;
    String vote, id;
    Call<votestatus> call1;
    String cid, isLIkE, token;
    String user;
    private AdView adView;
    // LinearLayout vmm;
    String videopath = "";
    String myFormat = "yyyy-MM-ddHH:mm:ss";
    DateFormat sdformat = new SimpleDateFormat(myFormat);
    DateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
    String formattedDate;
    //  public static VideoView videoView;
    private WrappingViewPager mViewPager;
    private Toolbar toolbar;
    CoordinatorLayout.LayoutParams appBarLayoutParams;
    AppBarLayout appBarLayout;
    private ArrayList<HashMap<String, String>> arrayviewcontainer;
    GauravNewViewpager viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        arrayviewcontainer = new ArrayList<>();
        hashMap = new HashMap<>();
        if (!arrayviewcontainer.isEmpty()) {
            arrayviewcontainer.clear();
        }
        MobileAds.initialize(Citizendetailpage.this, "ca-app-pub-3940256099942544~3347511713");
        adView = (AdView) findViewById(R.id.adView);
        AdRequest request = new AdRequest.Builder().build();
        adView.loadAd(request);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            postid = bundle.getString("postid");
            Type = bundle.getString("type");
        }
        mViewPager = (WrappingViewPager) findViewById(R.id.view_pager);
        tof_textview1 = (TextView) findViewById(R.id.tof_textview1);
        tof_textview2 = (TextView) findViewById(R.id.tof_textview2);
        likes = (ImageView) findViewById(R.id.like);
        views_readpost = (TextView) findViewById(R.id.views_readpost);
        shhh = (ImageButton) findViewById(R.id.shhh);
        shhh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareTextUrl();
            }
        });
        token = SaveSharedPreference.getToken(Citizendetailpage.this);
        datuDetas = new DatuDeta();
        vedios = new ArrayList();
        imageButton = (ImageButton) findViewById(R.id.comment);
        comments = (CardView) findViewById(R.id.comments);
        main_heading = (TextView) findViewById(R.id.main_heading);
        date_text = (TextView) findViewById(R.id.date_text);
        main_text = (TextView) findViewById(R.id.main_text);
        imagebuttotn2 = (TextView) findViewById(R.id.imagebuttotn2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setVisibility(View.GONE);

        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();

        imagebuttotn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebuttotn2.getText().toString().trim().equalsIgnoreCase("Follow")) {
                    imagebuttotn2.setText("UnFollow");
                    Unfollow();
                } else {
                    imagebuttotn2.setText("Follow");
                    follow();
                }

            }
        });
        imagebuttotn1 = (ImageView) findViewById(R.id.imagebuttotn1);
        comments_readpost = (TextView) findViewById(R.id.comments_readpost);
        image = (ImageView) findViewById(R.id.image);
        comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    Log.e("Type", "" + Type);
                    if (Type.equalsIgnoreCase("brand")) {
                        Intent in = new Intent(Citizendetailpage.this, commentactivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("postid", cid);
                        bundle.putString("type", "Brand");
                        in.putExtras(bundle);
                        startActivity(in);
                    } else if (Type.equalsIgnoreCase("star")) {
                        Intent in = new Intent(Citizendetailpage.this, commentactivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("postid", cid);
                        bundle.putString("type", "star");
                        in.putExtras(bundle);
                        startActivity(in);
                    } else {
                        Intent in = new Intent(Citizendetailpage.this, commentactivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("postid", cid);
                        bundle.putString("type", "citi");
                        in.putExtras(bundle);
                        startActivity(in);
                    }
                } else {

                }
            }
        });
        likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    if (isLIkE.equals("0")) {
                        id = cid;
                        datuDetas.setIsLike("1");
                        vote = "u";
                        vote();
                    } else {
                        id = cid;
                        datuDetas.setIsLike("0");
                        vote = "d";
                        votedown();
                    }
                } else {

                }
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    Log.e("Type", "" + Type);
                    if (Type.equalsIgnoreCase("brand")) {
                        Intent in = new Intent(Citizendetailpage.this, commentactivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("postid", cid);
                        bundle.putString("type", "Brand");
                        in.putExtras(bundle);
                        startActivity(in);
                    } else if (Type.equalsIgnoreCase("star")) {
                        Intent in = new Intent(Citizendetailpage.this, commentactivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("postid", cid);
                        bundle.putString("type", "star");
                        in.putExtras(bundle);
                        startActivity(in);
                    } else {
                        Intent in = new Intent(Citizendetailpage.this, commentactivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("postid", cid);
                        bundle.putString("type", "citi");
                        in.putExtras(bundle);
                        startActivity(in);
                    }
                } else {

                }
            }
        });
        likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    if (VoteST.equals("0")) {
                        id = postid;
                        vote = "u";
                        VoteST = "1";
                        vote();
                    } else {
                        id = postid;
                        vote = "d";
                        VoteST = "0";
                        votedown();
                    }
                } else {

                }
            }
        });

        contryFF();
        if (!token.equals("")
                && token != null) {
            ISVOTE();

        }
    }


    @Override
    public void onBackPressed() {
        // Notify the VideoEnabledWebChromeClient, and handle it ourselves if it doesn't handle it
        if (viewPagerAdapter != null)
            if (viewPagerAdapter.webChromeClient != null)
                if (!viewPagerAdapter.webChromeClient.onBackPressed()) {
                    if (viewPagerAdapter.webView.canGoBack()) {
                        viewPagerAdapter.webView.goBack();
                    } else {
                        // Standard back button implementation (for example this could close the app)
                        super.onBackPressed();
                    }
                } else
                    super.onBackPressed();
    }


 /*   @Override
    protected void onPause() {
        super.onPause();
        if (viewPagerAdapter != null)
            if (viewPagerAdapter.webView != null)
                viewPagerAdapter.webView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (viewPagerAdapter != null)
            if (viewPagerAdapter.webView != null)
                viewPagerAdapter.webView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (viewPagerAdapter != null)
            if (viewPagerAdapter.webView != null)
                viewPagerAdapter.webView.stopLoading();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(viewPagerAdapter!=null)
            if (viewPagerAdapter.webView != null)
            viewPagerAdapter.webView.destroy();
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here


                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    int count = 0;


    ArrayList<ImageS> mediaDataImage = new ArrayList<>();
    ArrayList<VideoS> mediaDataVideo = new ArrayList<>();

    //  ArrayList<VideoView> mediaData = new ArrayList<>();
    private void contryFF() {
        final RequestInterface requestInterface = ApiFactory.createService(Citizendetailpage.this, RequestInterface.class);
        ErrorCallback.MyCall<citizendetailmodel> myCall;
        if (Type.equalsIgnoreCase("brand")) {
            myCall = requestInterface.detailssb(postid);
        } else if (Type.equalsIgnoreCase("star")) {
            myCall = requestInterface.detailss(postid);
        } else {
            myCall = requestInterface.details(postid);
        }
        myCall.enqueue(new ErrorCallback.MyCallback<citizendetailmodel>() {
            @Override
            public void success(final Response<citizendetailmodel> res) {
                Citizendetailpage.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (res.isSuccessful()) {
                            toolbar.setVisibility(View.VISIBLE);
                            Log.e("res size", "" + res.body().getData().toString());
                            if (res.body().getData().get(1).getImages().size() > 0) {
                                mediaDataImage.addAll(res.body().getData().get(1).getImages());

                                for (ImageS imageS : mediaDataImage) {
                                    try {
                                        hashMap = new HashMap<>();
                                        hashMap.put("image", imageS.getImage360x290());
                                        //  hashMap.put("imagezoom", imageS.getImageOriginal());
                                        arrayviewcontainer.add(hashMap);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            Picasso.with(Citizendetailpage.this).load(res.body().getData().get(0).getAvatar()).into(imagebuttotn1);
                            if (res.body().getData().get(2).getVideos().size() > 0) {
                                mediaDataVideo.addAll(res.body().getData().get(2).getVideos());
                                for (VideoS videoS : mediaDataVideo) {
                                    try {
                                        hashMap = new HashMap<>();
                                        hashMap.put("video", videoS.getVideo());
                                        hashMap.put("image", videoS.getVideoThumb());
                                        arrayviewcontainer.add(hashMap);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            count += mediaDataImage.size();
                            count += mediaDataVideo.size();
                            try {
                                Log.e("arrayviewcontainer", "" + arrayviewcontainer.size());
                                viewPagerAdapter = new GauravNewViewpager(Citizendetailpage.this, arrayviewcontainer, "news");
                                mViewPager.setAdapter(viewPagerAdapter);
                                mViewPager.setPageMargin(20);
                            } catch (Exception e) {

                            }
                            viewPagerAdapter.notifyDataSetChanged();
                            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                @Override
                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                    if (arrayviewcontainer.size() > 0) {
                                        //image_selection.setVisibility(View.VISIBLE);

                                        Log.e("ccccccccc", "" + position);
                                        // image_selection.setText(""+1 + "/" + ab.size());
                                        Log.e("YES MY VALUe", "" + (position));
                                    } else {
                                        //image_selection.setVisibility(View.GONE);
                                    }
                                    // image_selection.setText((position + 1) + "/" + ab.size());
                                    //image_selection.setText((position + 1) + "/" + ab.size());
                                    Log.e("onPageSelected MY VALUe", "" + (position + 1));
                                }

                                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                                @Override
                                public void onPageSelected(int position) {

                                    if (arrayviewcontainer.get(position).containsKey("video")) {

                                    }

                                    int currentPage = 1;
                                    int totalPages = arrayviewcontainer.size();

                                    int nextPage = currentPage + 1;
                                    int previousPage = currentPage - 1;

                                    if (nextPage >= totalPages) {
                                        //  image_selection.setVisibility(View.VISIBLE);

                                        Log.e("ccccccccc", "" + position);
                                        //  image_selection.setText(currentPage+1 + "/" + ab.size());
                                        //  img_forward.setVisibility(View.INVISIBLE);
                                        //nextPage = 0;
                                    } else {
                                        //  img_forward.setVisibility(View.VISIBLE);
                                    }
                                    if (previousPage < 0) {
                                        //   image_selection.setVisibility(View.VISIBLE);

                                        Log.e("ccccccccc", "" + position);
                                        // image_selection.setText(currentPage-1 + "/" + ab.size());
                                        //  img_back.setVisibility(View.INVISIBLE);
                                    } else {
                                        // img_back.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onPageScrollStateChanged(int state) {
                                }
                            });
                            imagebuttotn2.setVisibility(View.VISIBLE);
                            datuDetas = res.body().getData().get(0);
                            tof_textview1.setText(res.body().getData().get(0).getFirstName());
                            tof_textview2.setText(res.body().getData().get(0).getTitle());
                            try {
                                formattedDate = targetFormat.format(sdformat.parse(res.body().getData().get(0).getDtAdded()));
                                date_text.setText(formattedDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            cid = res.body().getData().get(0).getId();
                            username = res.body().getData().get(0).getUsername();
                            views_readpost.setText(res.body().getData().get(0).getTotalLikes());
                            main_heading.setText(datuDetas.getTitle());
                            main_text.setText(datuDetas.getDescription());
                            comments_readpost.setText(datuDetas.getTotalComments());
                            if (!token.equals("")
                                    && token != null) {
                                ISfollow();
                            }
                        } else {

                        }

                    }
                });
            }

            @Override
            public void error(String errorMessage) {
            }
        }, Citizendetailpage.this, Boolean.FALSE);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    private void shareTextUrl() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, "I AM GAURAV" + "\n\n" + "Download app for more updates" + "\n" + "https://bit.ly/2pDJWrB");
        //  Log.e("Link", Apis.talent_share + data.getSlug()/*+"\n\n"+"Download app for more updates"+"\n"+"https://bit.ly/2pDJWrB"*/);
        Citizendetailpage.this.startActivity(Intent.createChooser(share, "Share link!"));
    }


    private void ISVOTE() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Citizendetailpage.this, RequestInterface.class);
        RequestBody Token = RequestBody.create(MultipartBody.FORM, token);
        RequestBody Pid = RequestBody.create(MultipartBody.FORM, postid);
        //Call<votestatus> call = requestInterface.isvote(Token,Pid);
        if (Type.equalsIgnoreCase("brand")) {
            call1 = requestInterface.isvote(Token, Pid);
        } else if (Type.equalsIgnoreCase("star")) {
            call1 = requestInterface.isvote(Token, Pid);
        } else {
            call1 = requestInterface.isvote(Token, Pid);
        }
        call1.enqueue(new Callback<votestatus>() {
            @Override
            public void onResponse(Call<votestatus> call, final Response<votestatus> response) {
                if (response.isSuccessful()) {
                    likes.setImageResource(R.mipmap.heartcol);
                    VoteST = "1";
                } else {
                    likes.setImageResource(R.mipmap.heart);
                    VoteST = "0";
                }
            }

            @Override
            public void onFailure(Call<votestatus> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    private void vote() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Citizendetailpage.this, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        MultipartBody.FORM, id);
        RequestBody Vote =
                RequestBody.create(
                        MultipartBody.FORM, vote);
        Call<likemodel> call = requestInterface.vote(Token, Id, Vote);
        call.enqueue(new Callback<likemodel>() {
            @Override
            public void onResponse(Call<likemodel> call,
                                   Response<likemodel> response) {

                if (response.isSuccessful()) {
                    Log.e("Strring", "TEST" + response.body().getData().toString());
                    views_readpost.setText(response.body().getData().getTotalLikes());
                    likes.setImageResource(R.mipmap.heartcol);
                } else {
                    //Toast.makeText(Citizendetailpage.this,response.body().getData().getMessage(),Toast.LENGTH_SHORT).show();
                    Log.e("String", "TEST111");
                }
            }

            @Override
            public void onFailure(Call<likemodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    private void votedown() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Citizendetailpage.this, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        MultipartBody.FORM, id);
        RequestBody Vote =
                RequestBody.create(
                        MultipartBody.FORM, vote);
        Call<likemodel> call = requestInterface.voted(Token, Id, Vote);
        call.enqueue(new Callback<likemodel>() {
            @Override
            public void onResponse(Call<likemodel> call,
                                   Response<likemodel> response) {

                if (response.isSuccessful()) {
                    Log.e("Strring", "TEST" + response.body().getData().toString());
                    views_readpost.setText(response.body().getData().getTotalLikes());
                    likes.setImageResource(R.mipmap.heart);
                } else {
                    Toast.makeText(Citizendetailpage.this, response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("String", "TEST111");
                }
            }

            @Override
            public void onFailure(Call<likemodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    private void follow() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Citizendetailpage.this, RequestInterface.class);
        RequestBody Token = RequestBody.create(MultipartBody.FORM, token);
        RequestBody userid = RequestBody.create(MultipartBody.FORM, username);
        Call<followunfollowmodel> call = requestInterface.follow(Token, userid);
        call.enqueue(new Callback<followunfollowmodel>() {
            @Override
            public void onResponse(Call<followunfollowmodel> call, final Response<followunfollowmodel> response) {
                if (response.isSuccessful()) {
                    imagebuttotn2.setText("Follow");
                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                } else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }

            @Override
            public void onFailure(Call<followunfollowmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    private void Unfollow() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Citizendetailpage.this, RequestInterface.class);
        RequestBody Token = RequestBody.create(MultipartBody.FORM, token);
        RequestBody userid = RequestBody.create(MultipartBody.FORM, username);
        Call<followunfollowmodel> call = requestInterface.unfollow(Token, userid);
        call.enqueue(new Callback<followunfollowmodel>() {
            @Override
            public void onResponse(Call<followunfollowmodel> call, final Response<followunfollowmodel> response) {
                if (response.isSuccessful()) {
                    imagebuttotn2.setText("UnFollow");
                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                } else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }

            @Override
            public void onFailure(Call<followunfollowmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    private void ISfollow() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Citizendetailpage.this, RequestInterface.class);
        RequestBody Token = RequestBody.create(MultipartBody.FORM, token);
        RequestBody userid = RequestBody.create(MultipartBody.FORM, username);
        Call<isfollowmodel> call = requestInterface.isfollow(Token, userid);
        call.enqueue(new Callback<isfollowmodel>() {
            @Override
            public void onResponse(Call<isfollowmodel> call, final Response<isfollowmodel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData().getIsFollowing().booleanValue() == false) {
                        imagebuttotn2.setText("Follow");
                    } else {
                        imagebuttotn2.setText("UnFollow");
                    }


                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                } else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }

            @Override
            public void onFailure(Call<isfollowmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}