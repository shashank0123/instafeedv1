package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;

import com.abacusdesk.instafeed.instafeed.R;

public class Rewards extends Activity {
LinearLayout one;
CardView two,three;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rewardsandoffer);
        one=(LinearLayout)findViewById(R.id.one);
        two=(CardView)findViewById(R.id.two);
        three=(CardView)findViewById(R.id.three);
        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Rewards.this,Rewardpoint.class);
                startActivity(in);
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Rewards.this,Deals.class);
                startActivity(in);
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}