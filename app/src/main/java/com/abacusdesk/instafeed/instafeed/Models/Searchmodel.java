package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class Searchmodel {
    @SerializedName("status")
 
    private Integer status;
    @SerializedName("message")
 
    private String message;
    @SerializedName("data")
 
    private DataN data;
    @SerializedName("type")
 
    private String type;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataN getData() {
        return data;
    }

    public void setData(DataN data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
