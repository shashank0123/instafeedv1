package com.abacusdesk.instafeed.instafeed.Activity;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Adpater.TabAdapter;
import com.abacusdesk.instafeed.instafeed.Adpater.brandadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Fragments.EARN;
import com.abacusdesk.instafeed.instafeed.Fragments.Follow;
import com.abacusdesk.instafeed.instafeed.Fragments.Followers;
import com.abacusdesk.instafeed.instafeed.Fragments.HISTORY;
import com.abacusdesk.instafeed.instafeed.Fragments.REWARDS;
import com.abacusdesk.instafeed.instafeed.Models.favmodel;
import com.abacusdesk.instafeed.instafeed.Models.rewardsmodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Rewardpoint extends FragmentActivity {
//        viewHolder.buy.setText("\u20B9 " + data.get(i).getPrice());
TextView rs,pts;
    private TabAdapter adapter;
    private TabLayout tabLayoutF;
    private ViewPager viewPagerF;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rewadpoint);
        viewPagerF = (ViewPager)  findViewById(R.id.viewPager);
        tabLayoutF = (TabLayout)  findViewById(R.id.tabLayout);
        rs=(TextView)findViewById(R.id.rs);
        pts=(TextView)findViewById(R.id.pts);
        token= SaveSharedPreference.getToken(Rewardpoint.this);

        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new EARN(), "EARN POINTS");
        adapter.addFragment(new REWARDS(), "REWARDS");
        adapter.addFragment(new HISTORY(), "HISTORY");
        viewPagerF.setAdapter(adapter);
        tabLayoutF.setupWithViewPager(viewPagerF);
        Rewards();
    }

    private void Rewards() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Rewardpoint.this, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);

        Call<rewardsmodel> call = requestInterface.Rewards(Token);
        call.enqueue(new Callback<rewardsmodel>() {
            @Override
            public void onResponse(Call<rewardsmodel> call,
                                   Response<rewardsmodel> response) {
                if (response.isSuccessful()) {
                    rs.setText(response.body().getWalletBalance());
                    pts.setText(response.body().getUserRewardPoint()+" pts");
                }
            }
            @Override
            public void onFailure(Call<rewardsmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}