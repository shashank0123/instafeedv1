package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;

import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

public class Splashscreen extends Activity {
    public static int SPLASH_TIME_OUT = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);
        if (!SaveSharedPreference.getToken(Splashscreen.this).equals("")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(Splashscreen.this, MainActivity.class));
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (SaveSharedPreference.getIsSplash(Splashscreen.this).equals("true")) {
                        startActivity(new Intent(Splashscreen.this, MainActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(Splashscreen.this, Introscreeen.class));
                        finish();
                    }
                }

            }, SPLASH_TIME_OUT);
        }

    }
}