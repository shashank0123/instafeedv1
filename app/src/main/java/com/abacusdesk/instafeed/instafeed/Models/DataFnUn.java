package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class DataFnUn {
    @SerializedName("users_id")
   
    private Integer usersId;
    @SerializedName("follow_users_id")
   
    private String followUsersId;

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public String getFollowUsersId() {
        return followUsersId;
    }

    public void setFollowUsersId(String followUsersId) {
        this.followUsersId = followUsersId;
    }
}
