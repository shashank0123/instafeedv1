package com.abacusdesk.instafeed.instafeed.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.abacusdesk.instafeed.instafeed.Adpater.Catadappter;

import com.abacusdesk.instafeed.instafeed.Adpater.Feedadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.Citizendata;
import com.abacusdesk.instafeed.instafeed.Models.DATACAT;
import com.abacusdesk.instafeed.instafeed.Models.MasterModel;
import com.abacusdesk.instafeed.instafeed.Models.category;

import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import java.util.ArrayList;


public class Citizen extends Fragment {
    RecyclerView recyclerView, recy;
    int index = 0;
    Feedadapter mFeedadapter;
  //  Catadappter mCatadappter;
    String token;
    int currentItems, totalItems, scrollOutItems;
    boolean isScrolling = false;
    ArrayList ab;
    Handler handler;
    //  LinearLayoutManager manager;
    LinearLayoutManager manager;
    private ArrayList<MasterModel> citizenData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.citizen, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recy);
        citizenData = new ArrayList<>();

        token = SaveSharedPreference.getToken(getActivity());
        Cat();
      //  contryFF(false, 47);
        handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (citizenData.size() > 0) {
                    initAdapter();
                    listener = new EndlessRecyclerViewScrollListener(manager) {
                        @Override
                        public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        //    Log.e("index", "index" + index);
                            contryFF(false, 52);
                        }
                    };
                    recyclerView.addOnScrollListener(listener);
                  //  Log.e("handler", "if");
                } else {
                    handler.postDelayed(this, 1000);
                  //  Log.e("handler", "else");
                }
            }
        });
        // initScrollListener();
        return view;
    }

    private ArrayList<MasterModel> masterModels = new ArrayList<>();

    private void initAdapter() {
        manager = new LinearLayoutManager(getActivity());
        mFeedadapter = new Feedadapter(getActivity(), citizenData);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(mFeedadapter);
        mFeedadapter.notifyDataSetChanged();
    }

    private EndlessRecyclerViewScrollListener listener;
    ArrayList<Citizendata> citizendata;
    private void contryFF(final boolean shouldRefresh, int positiomn) {
        citizendata =new ArrayList<>();
        citizendata.clear();
     //   masterModels.clear();
        Log.e("contryFF", "index" + index);
        final RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        ErrorCallback.MyCall<com.abacusdesk.instafeed.instafeed.Models.Citizen> myCall;
        if (!token.equals("")) {
          //  Log.e("indexCiti",""+index);
            myCall = requestInterface.getvf("1", token, index);
        } else {
            myCall = requestInterface.getvfeed(index);
        }
        myCall.enqueue(new ErrorCallback.MyCallback<com.abacusdesk.instafeed.instafeed.Models.Citizen>() {
            @Override
            public void success(final retrofit2.Response<com.abacusdesk.instafeed.instafeed.Models.Citizen> res) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (shouldRefresh) {
                            Log.e("IF", "citizendata size :" + citizendata.size()+" data size:"+res.body().getData().size());
                            ArrayList<MasterModel> masterModels= new ArrayList<>();
                             citizendata.addAll(res.body().getData());
                            for (int k = 0; k < citizendata.size(); k++) {
                                masterModels.add(new MasterModel(MasterModel.Category.V, null, citizendata.get(k)));
                            }
                           // mFeedadapter.refreshData(masterModels);
                            refreshData(masterModels);
                        } else {
                          //  citizendata.addAll(res.body().getData());
                            Log.e("Else", "citizendata size :" + citizendata.size()+" data size:"+res.body().getData().size());
                          //  Log.e("Else", "shouldRefresh" + shouldRefresh+" size:"+citizendata.size());
                            ArrayList<MasterModel> masterModels= new ArrayList<>();
                            if(res.body().getData().size()>0) {
                                citizendata.addAll(res.body().getData());
                                for (int k = 0; k < citizendata.size(); k++) {
                                    masterModels.add(new MasterModel(MasterModel.Category.V, null, citizendata.get(k)));
                                }
                            }
                            Log.e("Else1", "citizendata size :" + citizendata.size()+" data size:"+res.body().getData().size());

                            //mFeedadapter.setData(masterModels);
                            setData(masterModels);
                        }
                        index+=10;
                    }
                });
            }

            @Override
            public void error(String errorMessage) {
            }
        }, getActivity(), Boolean.FALSE);
    }

    public void refreshData(ArrayList<MasterModel> arrayList) {
        this.citizenData.clear();
        this.citizenData.addAll(arrayList);
        if (mFeedadapter != null)
            mFeedadapter.notifyDataSetChanged();


    }

    public void setData(ArrayList<MasterModel> arrayList) {
        Log.e("setData0", "citizendata size :" + citizenData.size());
        this.citizenData.addAll(arrayList);
        Log.e("setData1", "citizendata size :" + citizenData.size());
        if (mFeedadapter != null)
            mFeedadapter.notifyDataSetChanged();

    }

    private void Cat() {
        RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        ErrorCallback.MyCall<category> myCall;
        myCall = requestInterface.category();
        Log.e("response ", myCall.toString());
        myCall.enqueue(new ErrorCallback.MyCallback<category>() {
            @Override
            public void success(final retrofit2.Response<category> response) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body().getMessage().equals("success")) {
                            ArrayList<DATACAT> citizendata = response.body().getData();

                            masterModels.add(new MasterModel(MasterModel.Category.H, citizendata, null));
                            setData(masterModels);
                            contryFF(false, 47);
                         /*   mCatadappter = new Catadappter(getActivity(), response.body().getData());
                            recy.setAdapter(mCatadappter);*/
                        } else {


                        }
                    }
                });
            }

            @Override
            public void error(String errorMessage) {

            }
        }, getActivity(), Boolean.TRUE);
    }
    @Override
    public void onResume() {
        super.onResume();
     //   index=0;
        Log.e("indexCiti",""+"onResume");
    }
}