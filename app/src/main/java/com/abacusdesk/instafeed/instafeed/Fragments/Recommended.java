package com.abacusdesk.instafeed.instafeed.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.Adpater.FavoritesAdapter;
import com.abacusdesk.instafeed.instafeed.Adpater.staradapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.Models.favoritesModel;
import com.abacusdesk.instafeed.instafeed.Models.favoritesModel;
import com.abacusdesk.instafeed.instafeed.Mycode.CommonMethod;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Recommended extends Fragment {
        FavoritesAdapter  favoritesAdapter;
        ViewGroup header;
        ImageView menu;
        RecyclerView recyclerView;
        String token;
        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            token = SaveSharedPreference.getToken(getActivity());
        }
    
        private void favoritesdetail() {
            Log.e("token", "" + SaveSharedPreference.getToken(getActivity()));
            //   RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
            RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
    
            RequestBody Token =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, token);
    
            Call<favoritesModel> call = requestInterface.starSuggest(Token);
            call.enqueue(new Callback<favoritesModel>() {
                @Override
                public void onResponse(Call<favoritesModel> call,
                                       Response<favoritesModel> response) {
                    if (response.isSuccessful()) {
    
                        if (response.body().getMessage().equals("success")) {
                            Log.e("favoritesdetail ",""+response.body().toString());
                            favoritesAdapter = new FavoritesAdapter(response.body().getDataFavorites(), getActivity());
                            recyclerView.setAdapter(favoritesAdapter);  }
                    } else {
    
                      //  CommonMethod.showAlert2("Invalid Token", getActivity());
                    }
                }
    
                @Override
                public void onFailure(Call<favoritesModel> call, Throwable t) {
                    Log.e("Upload error:", t.getMessage());
                }
            });
        }
    
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.favorites_new, container, false);
    
            header = (ViewGroup) view.findViewById(R.id.header_favorites);
            menu = (ImageView) header.findViewById(R.id.menu);
            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity.Open();
                }
            });
            recyclerView = (RecyclerView) view.findViewById(R.id.favorites_recycler_view);
            GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);
            favoritesdetail();

            return view;
        }
}
