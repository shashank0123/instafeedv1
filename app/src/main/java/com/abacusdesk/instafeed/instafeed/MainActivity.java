package com.abacusdesk.instafeed.instafeed;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.Followandfollowers;
import com.abacusdesk.instafeed.instafeed.Activity.ProfileActivity;
import com.abacusdesk.instafeed.instafeed.Activity.Rewards;
import com.abacusdesk.instafeed.instafeed.Activity.login;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Fragments.Category;
import com.abacusdesk.instafeed.instafeed.Fragments.Language;
import com.abacusdesk.instafeed.instafeed.Activity.Setting;
import com.abacusdesk.instafeed.instafeed.Fragments.Advertising;
import com.abacusdesk.instafeed.instafeed.Fragments.HomeFragment;
import com.abacusdesk.instafeed.instafeed.Activity.MYFav;
import com.abacusdesk.instafeed.instafeed.Fragments.Myreards;
import com.abacusdesk.instafeed.instafeed.Fragments.Myrefral;
import com.abacusdesk.instafeed.instafeed.Fragments.Recommended;
import com.abacusdesk.instafeed.instafeed.Models.profilemodel;
import com.abacusdesk.instafeed.instafeed.Mycode.CommonMethod;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.abacusdesk.instafeed.instafeed.kyc.BrandVerificationScreen;
import com.abacusdesk.instafeed.instafeed.upload.upload;
import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.volcaniccoder.bottomify.BottomifyNavigationView;
import com.volcaniccoder.bottomify.OnNavigationItemChangeListener;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private NavigationView navigationView;
    public static DrawerLayout drawer;
    private View navHeader;
    String token;
    private ImageView imgProfile, profileuser;
    private TextView txtName, txtWebsite;
    private Toolbar toolbar;
    public static BottomifyNavigationView navigation;
    private static final String urlProfileImg = "http://instafeed.org/storage/avatar/80x80-fb6a61ff1bf47faf20c4d34bf17100fa.jpg";
    // index to identify current nav menu item
    public static MainActivity ctx;
    public static int navItemIndex = 0;
    Runnable mPendingRunnable;
    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_Follow = "follow";
    private static final String TAG_FAV = "fav";
    private static final String TAG_Payment = "payment";
    private static final String TAG_rewadrs = "rewards";
    private static final String TAG_refral = "refral";
    private static final String TAG_advertise = "advertise";
    private static final String TAG_setting = "setting";
    public static String CURRENT_TAG = TAG_HOME;
    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;
    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigation = (BottomifyNavigationView) findViewById(R.id.navigation);
        mHandler = new Handler();
        toolbar.setVisibility(View.GONE);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        token = SaveSharedPreference.getToken(MainActivity.this);
        ctx = this;
        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtName.getText().toString().trim().equals("Login")) {
                    Intent in = new Intent(MainActivity.this, login.class);
                    startActivity(in);
                } else {

                }

            }
        });
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);
        profileuser = (ImageView) navHeader.findViewById(R.id.profileuser);
        profileuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(in);
            }
        });
        if (!token.equals("")) {
            USERDEtail();
        } else {
            imgProfile.setImageResource(R.drawable.user);
        }
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        // load nav menu header data
        //   txtName.setText("Gaurav Pandey");
        try {
            if (!SaveSharedPreference.getUserID(MainActivity.this).equals("")) {
                Glide.with(MainActivity.this).load(SaveSharedPreference.getUserIMAGE(MainActivity.this)).centerInside().error(R.drawable.user).into(imgProfile);
                txtName.setText(SaveSharedPreference.getUserName(MainActivity.this));
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        // initializing navigation menu
        setUpNavigationView();
        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }
        //  navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        // attaching bottom sheet behaviour - hide / show on scroll
        //  CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        //  layoutParams.setBehavior(new BottomNavigationBehavior());

        navigation.setOnNavigationItemChangedListener(new OnNavigationItemChangeListener() {
            @Override
            public void onNavigationItemChanged(BottomifyNavigationView.NavigationItem navigationItem) {
                Log.e("getPosition :", "" + navigationItem.getPosition());
                switch (navigationItem.getPosition()) {
                    case 0:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                // update the main content by replacing fragments
                                // CURRENT_TAG = TAG_Post;
                                Log.e("Fragment", CURRENT_TAG);
                                HomeFragment fragment = new HomeFragment();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                        android.R.anim.fade_out);
                                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                                fragmentTransaction.commitAllowingStateLoss();
                                //toggleFab();
                                // }
                            }
                        };
                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        break;
                    case 1:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                Language fragment = new Language();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                        android.R.anim.fade_out);
                                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                                fragmentTransaction.commitAllowingStateLoss();
                            }
                        };

                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        break;
                    case 2:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                // update the main content by replacing fragments
                                //  CURRENT_TAG = TAG_Post;
                                upload fragment = new upload();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                        android.R.anim.fade_out);
                                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                                fragmentTransaction.commitAllowingStateLoss();
                                Log.e("Fragment", CURRENT_TAG);
                            }
                        };

                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        break;
                    case 3:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                // update the main content by replacing fragments
                                // CURRENT_TAG = TAG_Post;
                                Recommended fragment = new Recommended();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                        android.R.anim.fade_out);
                                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                                fragmentTransaction.commitAllowingStateLoss();
                                Log.e("Fragment", CURRENT_TAG);
                                Log.e("Fragment", CURRENT_TAG);
                                //toggleFab();

                                // }
                            }
                        };

                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        break;
                    case 4:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                // update the main content by replacing fragments
                                //  CURRENT_TAG = TAG_Post;
                                Category fragment = new Category();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                        android.R.anim.fade_out);
                                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                                fragmentTransaction.commitAllowingStateLoss();
                                Log.e("Fragment", CURRENT_TAG);
                                //toggleFab();
                                // }
                            }
                        };
                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        break;

                }
/*
                switch (navigationItem.getPosition()) {
                    case R.id.action_favorites:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                // update the main content by replacing fragments
                                // CURRENT_TAG = TAG_Post;
                                Log.e("Fragment", CURRENT_TAG);
                                HomeFragment fragment = new HomeFragment();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                        android.R.anim.fade_out);
                                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                                fragmentTransaction.commitAllowingStateLoss();
                                //toggleFab();
                                // }
                            }
                        };
                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        return true;
                    case R.id.action_schedules:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                Language fragment = new Language();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                        android.R.anim.fade_out);
                                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                                fragmentTransaction.commitAllowingStateLoss();
                            }
                        };

                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        return true;
                    case R.id.action_music:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                // update the main content by replacing fragments
                                //  CURRENT_TAG = TAG_Post;

                            }
                        };

                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        return true;
                    case R.id.post:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                // update the main content by replacing fragments
                                // CURRENT_TAG = TAG_Post;
                                Recommended fragment = new Recommended();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                        android.R.anim.fade_out);
                                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                                fragmentTransaction.commitAllowingStateLoss();
                                Log.e("Fragment", CURRENT_TAG);
                                Log.e("Fragment", CURRENT_TAG);
                                //toggleFab();

                                // }
                            }
                        };

                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        return true;
                    case R.id.lastc:
                        mPendingRunnable = new Runnable() {
                            @Override
                            public void run() {
                                // update the main content by replacing fragments
                                //  CURRENT_TAG = TAG_Post;
                                Category fragment = new Category();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                        android.R.anim.fade_out);
                                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                                fragmentTransaction.commitAllowingStateLoss();
                                Log.e("Fragment", CURRENT_TAG);
                                //toggleFab();
                                // }
                            }
                        };
                        // If mPendingRunnable is not null, then add to the message queue
                        if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                        return true;
                }
*/

            }
        });
        navigation.setActiveNavigationIndex(0);
    }
   /* private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_favorites:
                    mPendingRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // update the main content by replacing fragments
                            // CURRENT_TAG = TAG_Post;
                            Log.e("Fragment", CURRENT_TAG);
                            HomeFragment fragment = new HomeFragment();
                            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                    android.R.anim.fade_out);
                            fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                            fragmentTransaction.commitAllowingStateLoss();
                            //toggleFab();
                            // }
                        }
                    };
                    // If mPendingRunnable is not null, then add to the message queue
                    if (mPendingRunnable != null) {
                        mHandler.post(mPendingRunnable);
                    }
                    return true;
                case R.id.action_schedules:
                    mPendingRunnable = new Runnable() {
                        @Override
                        public void run() {
                            Language fragment = new Language();
                            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                    android.R.anim.fade_out);
                            fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                            fragmentTransaction.commitAllowingStateLoss();
                        }
                        };

                        // If mPendingRunnable is not null, then add to the message queue
                    if (mPendingRunnable != null) {
                            mHandler.post(mPendingRunnable);
                        }
                    return true;
                case R.id.action_music:
                            mPendingRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    // update the main content by replacing fragments
                                    //  CURRENT_TAG = TAG_Post;

                                }
                            };

                            // If mPendingRunnable is not null, then add to the message queue
                            if (mPendingRunnable != null) {
                                mHandler.post(mPendingRunnable);
                            }
                            return true;
                case R.id.post:
                    mPendingRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // update the main content by replacing fragments
                            // CURRENT_TAG = TAG_Post;
                            Recommended fragment = new Recommended();
                            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                    android.R.anim.fade_out);
                            fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                            fragmentTransaction.commitAllowingStateLoss();
                            Log.e("Fragment", CURRENT_TAG);
                            Log.e("Fragment", CURRENT_TAG);
                            //toggleFab();

                            // }
                        }
                    };

                    // If mPendingRunnable is not null, then add to the message queue
                    if (mPendingRunnable != null) {
                        mHandler.post(mPendingRunnable);
                    }
                    return true;
                case R.id.lastc:
                    mPendingRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // update the main content by replacing fragments
                            //  CURRENT_TAG = TAG_Post;
                            Category fragment = new Category();
                            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                    android.R.anim.fade_out);
                            fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                            fragmentTransaction.commitAllowingStateLoss();
                            Log.e("Fragment", CURRENT_TAG);
                            //toggleFab();
                            // }
                        }
                    };
                    // If mPendingRunnable is not null, then add to the message queue
                    if (mPendingRunnable != null) {
                        mHandler.post(mPendingRunnable);
                    }
                    return true;
            }

            return false;
        }
    };*/
    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button

            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };
        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
        // show or hide the fab button
        //Closing drawer on item click
        drawer.closeDrawers();
        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                Log.e("navItemIndex", "" + navItemIndex);
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                // photos
            /*    Intent info = new Intent(upload.this, Follow.class);
                startActivity(info);*/
            case 2:
                // movies fragment
               /* Intent inf = new Intent(upload.this, MYFav.class);
                startActivity(inf);*/
            case 3:
                // notifications fragment
               /* Payment payment = new Payment();
                return payment;*/
              /*  Intent in = new Intent(upload.this, com.abacusdesk.instafeed.instafeed.Payment.upload.class);
                startActivity(in);*/
            case 4:
                // settings fragment
                Myreards myreards = new Myreards();
                return myreards;
            case 5:
                // settings fragment
                Myrefral myrefral = new Myrefral();
                return myrefral;
            case 6:
                Advertising advertising = new Advertising();
                return advertising;
            case 7:
                Intent in1 = new Intent(MainActivity.this, Setting.class);
                startActivity(in1);
            default:
                return new HomeFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                Log.e("navItemIndex1", "" + navItemIndex);
                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {

                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        Log.e("CURRENT_TAG0", "" + CURRENT_TAG);

                        break;
                    case R.id.nav_follow:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_Follow;
                        Intent info = new Intent(MainActivity.this, Followandfollowers.class);
                        startActivity(info);
                        Log.e("CURRENT_TAG1", "" + CURRENT_TAG + navItemIndex);
                        return true;
                    case R.id.nav_Favourites:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_FAV;
                        startActivity(new Intent(MainActivity.this, MYFav.class));
                        drawer.closeDrawers();
                        return true;


                    case R.id.nav_Payments:
                        navItemIndex = 3;
                        startActivity(new Intent(MainActivity.this, com.abacusdesk.instafeed.instafeed.Payment.MainActivity.class));
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_kyc:
                        navItemIndex = 4;
                        startActivity(new Intent(MainActivity.this, BrandVerificationScreen.class));
                        drawer.closeDrawers();
                        return true;

                    case R.id.nav:
                        navItemIndex = 5;
                        startActivity(new Intent(MainActivity.this, Rewards.class));
                        drawer.closeDrawers();
                        CURRENT_TAG = TAG_rewadrs;
                        Log.e("CURRENT_TAG", "" + CURRENT_TAG);
                        return true;
                    case R.id.nav_Referral:
                        navItemIndex = 6;
                        CURRENT_TAG = TAG_refral;
                        Log.e("CURRENT_TAG", "" + CURRENT_TAG);
                        break;
                    case R.id.nav_Advertising:
                        navItemIndex = 7;
                        CURRENT_TAG = TAG_advertise;
                        Log.e("CURRENT_TAG", "" + CURRENT_TAG);
                        break;
                    case R.id.nav_sett1:
                        navItemIndex = 8;
                        CURRENT_TAG = TAG_setting;
                        startActivity(new Intent(MainActivity.this, Setting.class));
                        drawer.closeDrawers();
                        return true;
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    public static void Open() {
        drawer.openDrawer(Gravity.LEFT);
    }

    public static void Close() {
        drawer.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

       /* // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.main, menu);
        }*/

        // when fragment is notifications, load the menu created for notifications

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify add parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    private void USERDEtail() {
        Log.e("token", "" + SaveSharedPreference.getToken(MainActivity.this));
        //   RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(MainActivity.this, RequestInterface.class);

        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);

        Call<profilemodel> call = requestInterface.userprofile(Token);
        call.enqueue(new Callback<profilemodel>() {
            @Override
            public void onResponse(Call<profilemodel> call,
                                   Response<profilemodel> response) {
                if (response.isSuccessful()) {

                    if (response.body().getMessage().equals("success")) {
                        SaveSharedPreference.setUserName(MainActivity.this, response.body().getData().getUsername());
                        SaveSharedPreference.setFirstName(MainActivity.this, response.body().getData().getFirstName());
                        SaveSharedPreference.setLastName(MainActivity.this, response.body().getData().getLastName());
                        SaveSharedPreference.setUserEMAIL(MainActivity.this, response.body().getData().getEmail());
                        SaveSharedPreference.setUserID(MainActivity.this, response.body().getData().getUserId());
                        SaveSharedPreference.setUserIMAGE(MainActivity.this, response.body().getData().getAvatar());
                        SaveSharedPreference.setFollowers(MainActivity.this, response.body().getData().getTotals().getTotalFollowers());
                        SaveSharedPreference.setFollowing(MainActivity.this, response.body().getData().getTotals().getTotalFollowing());

                        try {
                            if (!SaveSharedPreference.getUserID(MainActivity.this).equals("")) {

                                Glide.with(MainActivity.this).load(SaveSharedPreference.getUserIMAGE(MainActivity.this)).error(R.drawable.user).into(imgProfile);
                                txtName.setText(SaveSharedPreference.getUserName(MainActivity.this));

                            }
                        } catch (NullPointerException npe) {
                            npe.printStackTrace();
                        }
                    } else {


                    }
                } else {

                    CommonMethod.showAlert2("Invalid Token", MainActivity.this);
                }
            }

            @Override
            public void onFailure(Call<profilemodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

}
