package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DatuDeta {


    public ArrayList<VideoS> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<VideoS> videos) {
        this.videos = videos;
    }

    @SerializedName("videos")


    private ArrayList<VideoS> videos = null;

    public String getIsLike() {
        return isLike;
    }

    public void setIsLike(String isLike) {
        this.isLike = isLike;
    }

    @SerializedName("is_like")

    private String isLike;



    @SerializedName("id")
   
    private String id;
    @SerializedName("news_category_id")
   
    private String newsCategoryId;
    @SerializedName("name")
   
    private String name;
    @SerializedName("title")
   
    private String title;
    @SerializedName("slug")
   
    private String slug;
    @SerializedName("description")
   
    private String description;
    @SerializedName("total_likes")
   
    private String totalLikes;
    @SerializedName("total_dislikes")
   
    private String totalDislikes;
    @SerializedName("total_comments")
   
    private String totalComments;
    @SerializedName("total_views")
   
    private String totalViews;
    @SerializedName("total_flags")
   
    private String totalFlags;
    @SerializedName("dt_added")
   
    private String dtAdded;
    @SerializedName("status")
   
    private String status;
    @SerializedName("user_id")
   
    private String userId;
    @SerializedName("first_name")
   
    private String firstName;
    @SerializedName("last_name")
   
    private String lastName;
    @SerializedName("nickname")
   
    private String nickname;
    @SerializedName("avatar")
   
    private String avatar;
    @SerializedName("username")
   
    private String username;
    @SerializedName("location_id")
   
    private String locationId;
    @SerializedName("location")
   
    private Object location;
    @SerializedName("source")
   
    private String source;
    @SerializedName("latitude")
   
    private String latitude;
    @SerializedName("longitude")
   
    private String longitude;
    @SerializedName("is_anonymous")
   
    private String isAnonymous;
    @SerializedName("images")
   
    private ArrayList<ImageS> images = new ArrayList<>();

    @SerializedName("audio")
   
    private ArrayList<String> audio = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNewsCategoryId() {
        return newsCategoryId;
    }

    public void setNewsCategoryId(String newsCategoryId) {
        this.newsCategoryId = newsCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalDislikes() {
        return totalDislikes;
    }

    public void setTotalDislikes(String totalDislikes) {
        this.totalDislikes = totalDislikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(String totalViews) {
        this.totalViews = totalViews;
    }

    public String getTotalFlags() {
        return totalFlags;
    }

    public void setTotalFlags(String totalFlags) {
        this.totalFlags = totalFlags;
    }

    public String getDtAdded() {
        return dtAdded;
    }

    public void setDtAdded(String dtAdded) {
        this.dtAdded = dtAdded;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public Object getLocation() {
        return location;
    }

    public void setLocation(Object location) {
        this.location = location;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(String isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public ArrayList<ImageS> getImages() {
        return images;
    }

    public void setImages(ArrayList<ImageS> images) {
        this.images = images;
    }



    public ArrayList<String> getAudio() {
        return audio;
    }

    public void setAudio(ArrayList<String> audio) {
        this.audio = audio;
    }
}
