package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class DATAUSER {

    @SerializedName("follow_status")
    private String followStatus;
    @SerializedName("avatar_userid")
    private String avatarUserid;
    @SerializedName("avatar")
    private Object avatar;
    @SerializedName("username")
    private String username;
    @SerializedName("like_type")
    private String likeType;
    public String getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(String followStatus) {
        this.followStatus = followStatus;
    }

    public String getAvatarUserid() {
        return avatarUserid;
    }

    public void setAvatarUserid(String avatarUserid) {
        this.avatarUserid = avatarUserid;
    }

    public Object getAvatar() {
        return avatar;
    }

    public void setAvatar(Object avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLikeType() {
        return likeType;
    }

    public void setLikeType(String likeType) {
        this.likeType = likeType;
    }
}
