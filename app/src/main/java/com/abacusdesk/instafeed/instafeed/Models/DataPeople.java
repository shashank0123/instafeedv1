package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Instafeed2 on 9/7/2019.
 */

public class DataPeople {
    @SerializedName("last_name")
    private String last_name;

    @SerializedName("id")
    private String id;

    @SerializedName("avatar")
    private String avatar;

    @SerializedName("middle_name")
    private String middle_name;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("username")
    private String username;

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAvatar ()
    {
        return avatar;
    }

    public void setAvatar (String avatar)
    {
        this.avatar = avatar;
    }

    public String getMiddle_name ()
    {
        return middle_name;
    }

    public void setMiddle_name (String middle_name)
    {
        this.middle_name = middle_name;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [last_name = "+last_name+", id = "+id+", avatar = "+avatar+", middle_name = "+middle_name+", first_name = "+first_name+", username = "+username+"]";
    }
}
