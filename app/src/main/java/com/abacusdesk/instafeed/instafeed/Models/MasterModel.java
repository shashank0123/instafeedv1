package com.abacusdesk.instafeed.instafeed.Models;

import com.abacusdesk.instafeed.instafeed.Models.Citizendata;
import com.abacusdesk.instafeed.instafeed.Models.DATACAT;

import java.util.ArrayList;

public class MasterModel {
    public enum Category {
        H, V
    }

    private ArrayList<DATACAT> horizobtalObjs;
    private Citizendata verticalObj;
    private Category category;

    public MasterModel(Category category, ArrayList<DATACAT> horizobtalObjs, Citizendata verticalObj) {
        this.category = category;
        this.verticalObj = verticalObj;
        if (horizobtalObjs != null) {
            this.horizobtalObjs = new ArrayList<>(horizobtalObjs);
        }
    }

    public ArrayList<DATACAT> getHorizobtalObjs() {
        return horizobtalObjs;
    }

    public Citizendata getVerticalObj() {
        return verticalObj;
    }

    public Category getCategory() {
        return category;
    }
}
