package com.abacusdesk.instafeed.instafeed.kyc;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.R;

/**
 * Created by Instafeed2 on 8/23/2019.
 */

public class SignedAgreementActivity extends AppCompatActivity {
    CardView downloadAgreement;
    CardView uploadAgreement;
    CheckBox iAgreeCheckbox;
    ImageView back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signed_agreement);
        downloadAgreement = (CardView)findViewById(R.id.download_agreement_layout);
        uploadAgreement = (CardView)findViewById(R.id.upload_agreement_layout);
        iAgreeCheckbox = (CheckBox)findViewById(R.id.i_agree_checkbox) ;
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        }); downloadAgreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        uploadAgreement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        iAgreeCheckbox.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });

    }
}
