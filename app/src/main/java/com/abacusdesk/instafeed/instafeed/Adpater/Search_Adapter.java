package com.abacusdesk.instafeed.instafeed.Adpater;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.Citizendetailpage;
import com.abacusdesk.instafeed.instafeed.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class Search_Adapter extends RecyclerView.Adapter<Search_Adapter.MyViewHolder> {

    Context context;
    //ArrayList<Search_model.DataBean> arrayList;
    ArrayList<HashMap<String,String>> arrayList;
    String formattedDate;
    public Search_Adapter(Context context,ArrayList<HashMap<String,String>> arrayList){
        this.context=context;
        this.arrayList=arrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_title,txt_descr,user_people;
        public ImageView imgpro;
        public ImageView avatar_people;
        public TextView date;
        public LinearLayout lnitem;
        public CardView carditem,search_people;

        public MyViewHolder(View view) {
            super(view);
            this.txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            this.txt_descr = (TextView) itemView.findViewById(R.id.txt_descr);
            this.user_people = (TextView) itemView.findViewById(R.id.user_people);
            this.lnitem=(LinearLayout)itemView.findViewById(R.id.ln_desc);
            this.date = (TextView) itemView.findViewById(R.id.txt_date);
            this.carditem = (CardView) itemView.findViewById(R.id.card_view);
            this.search_people = (CardView) itemView.findViewById(R.id.search_people);
            this.avatar_people = (ImageView) itemView.findViewById(R.id.avatar_people);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.searchlist, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String myFormat = "yyyy-MM-ddHH:mm:ss";
        Log.e("contryFF", "success..."+arrayList.size());
        Log.e("contryFF", "first name..."+arrayList.get(position).get("first_name")+" position :"+position);
       // Log.e("contryFF", "success..."+arrayList.size());

        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy hh mm");
        if(arrayList.get(position).get("Type").contains("users"))
        {
            holder.lnitem.setVisibility(View.GONE);
            holder.search_people.setVisibility(View.VISIBLE);

            if(arrayList.get(position).get("first_name")!=null ) {
                holder.user_people.setText(arrayList.get(position).get("first_name"));

                Picasso.with(context).load(arrayList.get(position).get("avatar")).placeholder(R.mipmap.proo).into(holder.avatar_people);
            }else
                holder.user_people.setText(arrayList.get(position).get("username"));

            Picasso.with(context).load(arrayList.get(position).get("avatar")).placeholder(R.mipmap.proo).into(holder.avatar_people);

        }
        else {
            holder.lnitem.setVisibility(View.VISIBLE);
            holder.search_people.setVisibility(View.GONE);
            holder.txt_title.setText(arrayList.get(position).get("title"));
            try {
                formattedDate = targetFormat.format(sdformat.parse(arrayList.get(position).get("description")));
                holder.date.setText(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.carditem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(context, Citizendetailpage.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("postid", arrayList.get(position).get("id"));
                    bundle.putString("type", arrayList.get(position).get("Type"));
                    in.putExtras(bundle);
                    context.startActivity(in);
                }
            });
            holder.lnitem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("clicked", arrayList.get(position).get("title"));
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
