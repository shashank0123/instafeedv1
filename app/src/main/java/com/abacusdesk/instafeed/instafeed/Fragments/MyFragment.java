package com.abacusdesk.instafeed.instafeed.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.commentactivity;
import com.abacusdesk.instafeed.instafeed.Activity.sample;
import com.abacusdesk.instafeed.instafeed.Activity.who_like;
import com.abacusdesk.instafeed.instafeed.Adpater.Myadapter;
import com.abacusdesk.instafeed.instafeed.Adpater.likeadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.brand;
import com.abacusdesk.instafeed.instafeed.Models.brandpromodel;
import com.abacusdesk.instafeed.instafeed.R;

public class MyFragment extends Fragment {

  String label;
    Myadapter mMyadapter;

    RecyclerView rec;

  public static MyFragment newInstance(String fragmentLabel) {
    MyFragment fragment = new MyFragment();
    Bundle args = new Bundle();
    args.putString("label", fragmentLabel);
    fragment.setArguments(args);
    return fragment;
  }
//fragment_item_list
  public MyFragment() {
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_item, container, false);

      rec=(RecyclerView)view.findViewById(R.id.rec);
      rec.setLayoutManager(new LinearLayoutManager(getActivity()));
      rec.setNestedScrollingEnabled(false);
      rec.setHasFixedSize(false);


    if(getArguments() == null) return view;
    label = getArguments().getString("label", "");
      CATdetail();
    return view;
  }
    private void CATdetail() {
        final RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        ErrorCallback.MyCall<brand> myCall;
        myCall = requestInterface.branpidlis(label);
        myCall.enqueue(new ErrorCallback.MyCallback<brand>() {
            @Override
            public void success(final retrofit2.Response<brand> res) {
                getActivity().runOnUiThread(new  Runnable() {
                    @Override
                    public void run() {
                        mMyadapter = new Myadapter(getActivity(), res.body().getData());
      rec.setAdapter(mMyadapter);
                    }
                });
            }
            @Override
            public void error(String errorMessage) {
            }
        }, getActivity(), Boolean.FALSE);

    }
}