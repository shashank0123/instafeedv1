package com.abacusdesk.instafeed.instafeed.Adpater;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.Citizendetailpage;
import com.abacusdesk.instafeed.instafeed.Models.Databrand;
import com.abacusdesk.instafeed.instafeed.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Myadapter extends RecyclerView.Adapter<Myadapter.ViewHolder> {
    private ArrayList<Databrand> arrayList;
    Context context;
    String formattedDate;
    public Myadapter(Context context, ArrayList<Databrand> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @NonNull
    @Override
    public  ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item_list, parent, false);
         ViewHolder viewHolder = new  ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull  ViewHolder viewHolder, final int i) {
        String myFormat = "yyyy-MM-ddHH:mm:ss";
        Log.e("contryFF", "success..."+arrayList.size());
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
        viewHolder.txt_name.setText(arrayList.get(i).getTitle());
        try {
            formattedDate = targetFormat.format(sdformat.parse(arrayList.get(i).getDtAdded()));
            viewHolder.imgtym.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Picasso.with(context).load(arrayList.get(i).getAvatar()).error(R.drawable.user).
                into(viewHolder.img_profile);
        viewHolder.head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, Citizendetailpage.class);
                Bundle bundle = new Bundle();
                bundle.putString("postid", arrayList.get(i).getId());
                bundle.putString("type", "Brand");
                in.putExtras(bundle);
                context.startActivity(in);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView lang;
        ImageView img_profile;
        TextView txt_name,imgtym;
        LinearLayout head;
        public ViewHolder(View itemView) {
            super(itemView);
            txt_name = (TextView) itemView.findViewById(R.id.txt_name);
            imgtym = (TextView) itemView.findViewById(R.id.imgtym);
            img_profile=(ImageView)itemView.findViewById(R.id.img_profile);
            head=(LinearLayout)itemView.findViewById(R.id.head);
        }
    }
}
