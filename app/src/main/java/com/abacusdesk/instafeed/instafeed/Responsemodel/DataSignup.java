package com.abacusdesk.instafeed.instafeed.Responsemodel;

import com.google.gson.annotations.SerializedName;

public class DataSignup {
    @SerializedName("token")
      
    private String token;
    @SerializedName("token_updated")
      
    private String tokenUpdated;
    @SerializedName("token_expires")
      
    private String tokenExpires;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenUpdated() {
        return tokenUpdated;
    }

    public void setTokenUpdated(String tokenUpdated) {
        this.tokenUpdated = tokenUpdated;
    }

    public String getTokenExpires() {
        return tokenExpires;
    }

    public void setTokenExpires(String tokenExpires) {
        this.tokenExpires = tokenExpires;
    }
}
