package com.abacusdesk.instafeed.instafeed.Adpater;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.Datalistfollow;
import com.abacusdesk.instafeed.instafeed.Models.followunfollowmodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.squareup.picasso.Picasso;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class followadapter extends RecyclerView.Adapter<followadapter.MyViewHolder> {

    Context context;
    ArrayList<Datalistfollow> data;
    public static String comment = "", comment_id;
    Dialog dialog;
    String upperString;
    String token, username;
    public followadapter(ArrayList<Datalistfollow> data, Context context) {
        this.context = context;
        this.data = data;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtname, txt_userhashname;
        public ImageView imgprofile, imgedit;
        TextView btn_follow;
        public MyViewHolder(View view) {
            super(view);
            this.imgprofile = (ImageView) itemView.findViewById(R.id.user_img);
            this.txtname = (TextView) itemView.findViewById(R.id.txt_username);
            this.txt_userhashname = (TextView) itemView.findViewById(R.id.txt_userhashname);
            this.btn_follow = (TextView) itemView.findViewById(R.id.btn_follow);
        }
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rowlistf, parent, false);
        token = SaveSharedPreference.getToken(context);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        String myFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");
        if (data.get(position).getFirstName() == null) {
            upperString = data.get(position).getUsername().substring(0, 1).toUpperCase() + data.get(position).getUsername().substring(1);
            holder.txtname.setText(upperString);
        } else {
            upperString = data.get(position).getFirstName().substring(0, 1).toUpperCase() + data.get(position).getFirstName().substring(1);
            holder.txt_userhashname.setText(data.get(position).getUsername());
            holder.txtname.setText(upperString);

        }
        Picasso.with(context).load(data.get(position).getAvatar()).error(R.drawable.user).into(holder.imgprofile);
        holder.btn_follow.setText("FOLLOW");
        holder.btn_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username=data.get(position).getUsername();
                follow();
            }
        });


    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    private void follow() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token = RequestBody.create(MultipartBody.FORM, token);
        RequestBody userid = RequestBody.create(okhttp3.MultipartBody.FORM, username);
        Call<followunfollowmodel> call = requestInterface.follow(Token, userid);
        call.enqueue(new Callback<followunfollowmodel>() {
            @Override
            public void onResponse(Call<followunfollowmodel> call, final Response<followunfollowmodel> response) {
                if (response.isSuccessful()) {

                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                } else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }

            @Override
            public void onFailure(Call<followunfollowmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}
