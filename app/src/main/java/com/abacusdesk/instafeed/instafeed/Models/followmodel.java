package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class followmodel {
    @SerializedName("status")
 
    private Integer status;
    @SerializedName("message")
 
    private String message;
    @SerializedName("data")
 
    private DataF data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataF getData() {
        return data;
    }

    public void setData(DataF data) {
        this.data = data;
    }
}
