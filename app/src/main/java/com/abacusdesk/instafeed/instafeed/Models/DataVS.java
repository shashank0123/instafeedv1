package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class DataVS {
    @SerializedName("news_id")
  
    private String newsId;
    @SerializedName("user_id")
  
    private String userId;
    @SerializedName("message")
  
    private String message;
    @SerializedName("vote_status")
  
    private String voteStatus;

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVoteStatus() {
        return voteStatus;
    }

    public void setVoteStatus(String voteStatus) {
        this.voteStatus = voteStatus;
    }
}
