package com.abacusdesk.instafeed.instafeed.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.abacusdesk.instafeed.instafeed.Adpater.brandadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.brand;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;

public class Brand extends Fragment {
    RecyclerView recyclerView;
    brandadapter mbrandadapter;
    String Token="";
    ArrayList ab;
    int index=0;
    private AdView adView;
    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.brand, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView);
        MobileAds.initialize(getActivity(), "ca-app-pub-3940256099942544~3347511713");
        adView = (AdView)view.findViewById(R.id.adView);
        AdRequest request = new AdRequest.Builder().build();
        adView.loadAd(request);
        ab= new ArrayList();
        final LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        mbrandadapter= new brandadapter(getActivity(),ab);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(mbrandadapter);
        Token= SaveSharedPreference.getToken(getActivity());
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.simpleSwipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                    index = 0;
                    ab.clear();
                    listener.resetState();
                    BRand(true);
                    // swipeRefreshLayout.setRefreshing(false);  // This hides the spinner
                }
            }
        });
        BRand(false);
        listener = new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.e("index", "index" +index);
                BRand(false);
            }
        };
        recyclerView.addOnScrollListener(listener);

        return view;
    }
    private EndlessRecyclerViewScrollListener listener;
    private void BRand(final boolean shouldRefresh) {
        Log.e("contryFF", "started...");
        final RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        ErrorCallback.MyCall<brand> myCall;
        if (!Token.equals("")) {
       //     Log.e("indexBRand",""+index);
            myCall = requestInterface.getbrand( "1",Token ,String.valueOf(index));
        } else {
            myCall = requestInterface.getbrand1("1", String.valueOf(index));
        }
        myCall.enqueue(new ErrorCallback.MyCallback<brand>() {
            @Override
            public void success(final retrofit2.Response<brand> res) {
                getActivity().runOnUiThread(new  Runnable() {
                    @Override
                    public void run() {
                       /* mbrandadapter= new brandadapter(getActivity(),res.body().getData());
                        recyclerView.setAdapter(mbrandadapter);
                        Log.e("setData", "shouldRefresh"+shouldRefresh);*/
                        if (mbrandadapter != null) {
                            swipeRefreshLayout.setRefreshing(false);
                            if (shouldRefresh)
                                mbrandadapter.refreshData(res.body().getData());
                            else

                                mbrandadapter.setData(res.body().getData());

                        }
                        ++index;
                    }
                });
            }
            @Override
            public void error(String errorMessage) {
            }
        }, getActivity(), Boolean.FALSE);
    }

    @Override
    public void onResume() {
        super.onResume();
        index=0;
        Log.e("indexBRand",""+"onResume");
    }
}