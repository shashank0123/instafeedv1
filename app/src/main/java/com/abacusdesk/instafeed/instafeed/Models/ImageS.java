package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class ImageS {
    @SerializedName("id")
     
    private String id;
    @SerializedName("image")
     
    private String image;
    @SerializedName("image_zoom")
     
    private String imageZoom;
    @SerializedName("image_original")
     
    private String imageOriginal;
    @SerializedName("image_100x100")
     
    private String image100x100;
    @SerializedName("image_256x170")
     
    private String image256x170;
    @SerializedName("image_264x200")
     
    private String image264x200;
    @SerializedName("image_360x290")
     
    private String image360x290;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageZoom() {
        return imageZoom;
    }

    public void setImageZoom(String imageZoom) {
        this.imageZoom = imageZoom;
    }

    public String getImageOriginal() {
        return imageOriginal;
    }

    public void setImageOriginal(String imageOriginal) {
        this.imageOriginal = imageOriginal;
    }

    public String getImage100x100() {
        return image100x100;
    }

    public void setImage100x100(String image100x100) {
        this.image100x100 = image100x100;
    }

    public String getImage256x170() {
        return image256x170;
    }

    public void setImage256x170(String image256x170) {
        this.image256x170 = image256x170;
    }

    public String getImage264x200() {
        return image264x200;
    }

    public void setImage264x200(String image264x200) {
        this.image264x200 = image264x200;
    }

    public String getImage360x290() {
        return image360x290;
    }

    public void setImage360x290(String image360x290) {
        this.image360x290 = image360x290;
    }
}
