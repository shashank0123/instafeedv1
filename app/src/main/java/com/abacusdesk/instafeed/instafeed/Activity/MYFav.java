package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.abacusdesk.instafeed.instafeed.Adpater.bookmarkadapter;
import com.abacusdesk.instafeed.instafeed.Adpater.likeadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Mycode.CommonMethod;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Responsemodel.forgotmodel;
import com.abacusdesk.instafeed.instafeed.Responsemodel.getbookmarkmodel;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MYFav   extends Activity {
   RecyclerView bookmark;
   String Token;
    bookmarkadapter mbookmarkadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myfav);
        Token= SaveSharedPreference.getToken(MYFav.this);
        bookmark=(RecyclerView)findViewById(R.id.bookmark);
        bookmark.setLayoutManager(new LinearLayoutManager(MYFav.this));
        bookmark.setNestedScrollingEnabled(false);
        bookmark.setHasFixedSize(false);
      findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              finish();
          }
      });
        Bookmarks();
    }
    private void Bookmarks() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(MYFav.this, RequestInterface.class);
        RequestBody Ttoken = RequestBody.create(okhttp3.MultipartBody.FORM, Token);

        Call<getbookmarkmodel> call = requestInterface.getbookmarks(Ttoken);
        call.enqueue(new Callback<getbookmarkmodel>() {
            @Override
            public void onResponse(Call<getbookmarkmodel> call, final Response<getbookmarkmodel> response) {
                Log.e("Bookmarks :", response.body().toString());

                if (response.isSuccessful()) {
                    mbookmarkadapter = new bookmarkadapter(MYFav.this,response.body().getData());
                    bookmark.setAdapter(mbookmarkadapter);
                } else {

                }
            }
            @Override
            public void onFailure(Call<getbookmarkmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}