package com.abacusdesk.instafeed.instafeed.Responsemodel;

import com.google.gson.annotations.SerializedName;

public class forgotmodel {
/*    {
        "status": 200,
            "message": "success",
            "data": {
        "user_id": "9818499737",
                "message": "Reset password link sent to your registered email address"
    }
    }*/

    @SerializedName("status")
   
    private Integer status;
    @SerializedName("message")
   
    private String message;
    @SerializedName("data")
   
    private DataForgot data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataForgot getData() {
        return data;
    }

    public void setData(DataForgot data) {
        this.data = data;
    }
}
