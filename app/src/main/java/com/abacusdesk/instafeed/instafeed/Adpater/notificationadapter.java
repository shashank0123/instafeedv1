package com.abacusdesk.instafeed.instafeed.Adpater;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.Citizendetailpage;
import com.abacusdesk.instafeed.instafeed.Models.DataNoti;
import com.abacusdesk.instafeed.instafeed.R;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class notificationadapter extends RecyclerView.Adapter<notificationadapter.ViewHolder> {
    private ArrayList<DataNoti> arrayList;
    Context context;
    String TYPE;
    String myFormat = "yyyy-MM-ddHH:mm:ss";
    DateFormat sdformat = new SimpleDateFormat(myFormat);
    DateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
    String formattedDate;
    public notificationadapter(ArrayList<DataNoti> arrayList, Context context) {
        this.context = context;
        this.arrayList = arrayList;
    }
    @NonNull
    @Override
    public  ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notificationrow, parent, false);
    ViewHolder viewHolder = new  ViewHolder(v);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull  ViewHolder viewHolder, final int position) {
        viewHolder.txt_name.setText(arrayList.get(position).getNotification());
        viewHolder.txt_comment.setText(arrayList.get(position).getDtAdded());
        Glide.with(context).load(arrayList.get(position).getAvatar()).error(R.drawable.user).into(viewHolder.imageView);
        viewHolder.cardview_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrayList.get(position).getModuleId().equals("1")) {
                    TYPE="Citi";
                }else if(arrayList.get(position).getModuleId().equals("3")){
                    TYPE="Brand";
                }else if(arrayList.get(position).getModuleId().equals("2")){
                    TYPE="Star";
                }
                Intent in = new Intent(context, Citizendetailpage.class);
                Bundle bundle = new Bundle();
                bundle.putString("postid", arrayList.get(position).getPostId());
                bundle.putString("type", TYPE);
                in.putExtras(bundle);
                context.startActivity(in);
            }
        });
    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_name, txt_comment, genre;
        public ImageView imageView;
        CardView cardview_id;
        LinearLayout hhhhc;
        public ViewHolder(View view) {
            super(view);
            imageView =(ImageView)view.findViewById(R.id.img_profilepic);
            txt_name = (TextView) view.findViewById(R.id.txt_name);
            txt_comment = (TextView) view.findViewById(R.id.txt_comment);
            hhhhc= (LinearLayout) view.findViewById(R.id.hhhhc);
            cardview_id=(CardView)view.findViewById(R.id.cardview_id);
        }
    }

}

