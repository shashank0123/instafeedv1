package com.abacusdesk.instafeed.instafeed.Util;

public interface OnLoadMoreListener {
    void onLoadMore();
}
