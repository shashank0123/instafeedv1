package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Instafeed2 on 9/4/2019.
 */


public class favoritesModel
{
    @SerializedName("data")

    private ArrayList<DataFavorites> DataFavorites =null;
    @SerializedName("message")
    private String message;
    @SerializedName("status")

    private String status;

    public ArrayList<DataFavorites> getDataFavorites ()
    {
        return DataFavorites;
    }

    public void setDataFavorites (ArrayList<DataFavorites> DataFavorites)
    {
        this.DataFavorites = DataFavorites;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DataFavorites = "+DataFavorites+", message = "+message+", status = "+status+"]";
    }
}