package com.abacusdesk.instafeed.instafeed.upload.helpers;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.util.Log;
import android.widget.Toast;

import com.iceteck.silicompressorr.SiliCompressor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import id.zelory.compressor.Compressor;

public class CoreHelper {

    private Context context;

    public CoreHelper(Context context) {
        this.context = context;
    }

    public String getFileNameFromUri(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public int generateUniqueID(){
        final int min = 1000000;
        final int max = 10000000;
        return new Random().nextInt((max - min) + 1) + min;
    }

    public File compressImage(int quality, File actualImage, File destinationDirectory) throws IOException {
        return new Compressor(context)
                .setQuality(quality)
                .setDestinationDirectoryPath(destinationDirectory.getAbsolutePath())
                .compressToFile(actualImage);
    }

    public File compressVideo(String videoPath, String destinationDirectory) throws URISyntaxException {
        String filePath = SiliCompressor.with(context).compressVideo(videoPath, destinationDirectory);
        return new File(filePath);
    }

    public File saveCapturedBitmap(Bitmap finalBitmap) {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/Instafeed/.tempIMG/.store");
        if (!myDir.exists()){
            if (!myDir.mkdirs()){
                Toast.makeText(context, "Couldn't save image!", Toast.LENGTH_SHORT).show();
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = "IMG_"+ timeStamp +".jpg";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public void createCoreDirectories(){
        File tempVids = new File(Environment.getExternalStorageDirectory(), "Instafeed" + File.separator + ".tempVids");
        File tempIMG = new File(Environment.getExternalStorageDirectory(), "Instafeed" + File.separator + ".tempIMG");
        File tempAudio = new File(Environment.getExternalStorageDirectory(), "Instafeed" + File.separator + ".tempAudio");
        File tempVideo = new File(Environment.getExternalStorageDirectory(), "Instafeed" + File.separator + ".tempVideo");
        File tempIMG_Store = new File(Environment.getExternalStorageDirectory(), "Instafeed"+File.separator+".tempIMG"+File.separator+".store");
        if (!tempVids.exists()){
            if (!tempVids.mkdirs()){
                Log.d("APP_LOG:CoreDir", "Unable to create directory (tempVids)");
            }
        }
        if (!tempIMG.exists()){
            if (!tempIMG.mkdirs()){
                Log.d("APP_LOG:CoreDir", "Unable to create directory (tempIMG)");
            }
        }
        if (!tempAudio.exists()){
            if (!tempAudio.mkdirs()){
                Log.d("APP_LOG:CoreDir", "Unable to create directory (tempAudio)");
            }
        }
        if (!tempVideo.exists()){
            if (!tempVideo.mkdirs()){
                Log.d("APP_LOG:CoreDir", "Unable to create directory (tempVideo)");
            }
        }
        if (!tempIMG_Store.exists()){
            if (!tempIMG_Store.mkdirs()){
                Log.d("APP_LOG:CoreDir", "Unable to create directory (tempIMG_Store)");
            }
        }
    }
}
