package com.abacusdesk.instafeed.instafeed.Adpater;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Models.DataRewards;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class RewardsAdapterH extends RecyclerView.Adapter<RewardsAdapterH.MyViewHolder> {
    String id="";
    String point="";
    Context context;
    ArrayList<DataRewards> data;
    public static String comment = "", comment_id;
    Dialog dialog;
    String upperString;
    String token, username;
    public RewardsAdapterH(ArrayList<DataRewards> data, Context context) {
        this.context = context;
        this.data = data;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, dis;
        public ImageView imge;
        CardView buy;

        public MyViewHolder(View view) {
            super(view);
            this.imge = (ImageView) itemView.findViewById(R.id.imge);
            this.name = (TextView) itemView.findViewById(R.id.name);
            this.dis = (TextView) itemView.findViewById(R.id.dis);
            this.buy=(CardView) itemView.findViewById(R.id.buy);
        }
    }
    @Override
    public  MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rewardrow, parent, false);
        token = SaveSharedPreference.getToken(context);
        return new  MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Picasso.with(context).load(data.get(position).getImage()).into(holder.imge);
        holder.name.setText(data.get(position).getDescription());
        holder.dis.setText("Already REDEEM");
        holder.buy.setCardBackgroundColor(Color.parseColor("#FF88E972"));

    }


    @Override
    public int getItemCount() {
        return data.size();
    }



}
