package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class datah {
    @SerializedName("id")
         
    private String id;
    @SerializedName("name")
         
    private String name;
    @SerializedName("total_points")
         
    private String totalPoints;
    @SerializedName("dt_added")
         
    private String dtAdded;
    @SerializedName("status")
         
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getDtAdded() {
        return dtAdded;
    }

    public void setDtAdded(String dtAdded) {
        this.dtAdded = dtAdded;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
