package com.abacusdesk.instafeed.instafeed.upload;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.upload.helpers.CoreHelper;
import com.abacusdesk.instafeed.instafeed.upload.helpers.MK23_ImagesAdapter;
import com.abacusdesk.instafeed.instafeed.upload.helpers.MK23_ImagesModel;
import com.abacusdesk.instafeed.instafeed.upload.helpers.MK23_MusicAdapter;
import com.abacusdesk.instafeed.instafeed.upload.helpers.MK23_MusicModel;
import com.abacusdesk.instafeed.instafeed.upload.helpers.MK23_VideoAdapter;
import com.abacusdesk.instafeed.instafeed.upload.helpers.MK23_VideoModel;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.iceteck.silicompressorr.FileUtils;
import com.iceteck.silicompressorr.SiliCompressor;
import com.mindorks.paracamera.Camera;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static android.app.Activity.RESULT_OK;
import static com.abacusdesk.instafeed.instafeed.upload.helpers.App.CHANNEL_ID;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.google.android.gms.internal.zzagz.runOnUiThread;
public class upload extends Fragment implements View.OnClickListener {
    CoreHelper coreHelper;
    private static final int STORAGE_PERMISSION_CODE = 1001;
    private static final int GET_IMAGE_REQUEST_CODE = 1002;
    private static final int GET_AUDIO_REQUEST_CODE = 1003;
    private static final int GET_VIDEO_REQUEST_CODE = 1004;
    private static final int VIDEO_CAPTURE_REQUEST_CODE = 1005;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 1006;
    private static final int RECORD_PERMISSION_CODE = 1007;
    private static final int AUDIO_INTENT_REQUEST_CODE = 1008;
    private static final int CAPTURE_IMAGE_REQUEST_CODE = 1009;
    private static final int ALL_PERMISSION_REQUEST = 2000;
    public static final String MIME_IMAGES = "images";
    public static final String MIME_VIDEOS = "videos";
    public static final String MIME_AUDIOS = "audios";
    public static final String MIME_TEXT = "text";
    String currentAccessToken = null;
    String recordedAudioFilePath;
    EditText mainInput;
    RecyclerView recyclerView;
    Spinner modeSpinner, locationSpinner;
    Button btnFeed, btnDraft;
    List<MK23_ImagesModel> imagesList;
    List<MK23_MusicModel> musicList;
    List<MK23_VideoModel> videoList;
    List<File> compressedFilesList;
    MK23_ImagesAdapter imagesAdapter;
    MK23_VideoAdapter videoAdapter;
    MK23_MusicAdapter musicAdapter;
    ImageButton btnSimpleText, btnPickImageFromGallery, btnCaptureImageFromCamera, btnPoll, btnRecordAudio, btnPickAudio, btnPickVideo, btnCloseThisActivity, btnPickVideoFromCamera;
    String contentType;
    NotificationManagerCompat notificationManagerCompat;
    NotificationCompat.Builder notification;
    Camera camera;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        coreHelper = new CoreHelper(getActivity());
        View view = inflater.inflate(R.layout.uploadui, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                coreHelper.createCoreDirectories();
            } else {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}, ALL_PERMISSION_REQUEST);
            }
        } else {
            coreHelper.createCoreDirectories();
        }

        //Declarations Start
        modeSpinner = view.findViewById(R.id.spinner_mode);
        locationSpinner = view.findViewById(R.id.spinner_location);
        btnSimpleText = view.findViewById(R.id.feed_simple_text);
        btnPickImageFromGallery = view.findViewById(R.id.feed_image);
        btnCaptureImageFromCamera =view. findViewById(R.id.feed_image_camera);
        btnPoll = view.findViewById(R.id.feed_poll);
        btnRecordAudio = view.findViewById(R.id.feed_recorded_audio);
        btnPickAudio = view.findViewById(R.id.feed_music);
        btnPickVideo = view.findViewById(R.id.feed_video);
        btnPickVideoFromCamera = view.findViewById(R.id.feed_video_camera);
        btnFeed = view.findViewById(R.id.post_feed);
        btnDraft = view.findViewById(R.id.draft_feed);
        recyclerView =view. findViewById(R.id.recyclerView);
        mainInput = view.findViewById(R.id.input_text);
        btnCloseThisActivity =view. findViewById(R.id.btnCloseThisActivity);
        imagesList = new ArrayList<>();
        musicList = new ArrayList<>();
        videoList = new ArrayList<>();
        compressedFilesList = new ArrayList<>();
        imagesAdapter = new MK23_ImagesAdapter(getActivity(), imagesList);
        musicAdapter = new MK23_MusicAdapter(getActivity(), musicList);
        videoAdapter = new MK23_VideoAdapter(getActivity(), videoList);
        contentType = "text";
        notificationManagerCompat = NotificationManagerCompat.from(getActivity());
        //Declaration Finishes

        //Click Listeners Starts
        btnSimpleText.setOnClickListener(this);
        btnPickImageFromGallery.setOnClickListener(this);
        btnCaptureImageFromCamera.setOnClickListener(this);
        btnPoll.setOnClickListener(this);
        btnRecordAudio.setOnClickListener(this);
        btnPickAudio.setOnClickListener(this);
        btnPickVideo.setOnClickListener(this);
        btnFeed.setOnClickListener(this);
        btnDraft.setOnClickListener(this);
        btnCloseThisActivity.setOnClickListener(this);
        btnPickVideoFromCamera.setOnClickListener(this);

        //Click Listeners Finished

        //Spinners Code Starts
        String[] modeOptions = {"Public"};
        String[] locationOptions = {"Location"};
        ArrayAdapter mode_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, modeOptions);
        mode_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modeSpinner.setAdapter(mode_adapter);
        ArrayAdapter location_adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, locationOptions);
        location_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationSpinner.setAdapter(location_adapter);
        //Spinners Code Ends

        //Recycler View Code Starts
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(imagesAdapter);
        //Recycler View Code Ends

        changeView(false);
        return view;
    }

   @Override
   public void onClick(View view) {
       int id = view.getId();
       switch (id) {
           case R.id.feed_simple_text:
               contentType = "text";
               mainInput.setText("");
               changeView(false);
               break;
           case R.id.feed_image:
               checkPermissionAndGetImages();
               break;
           case R.id.feed_image_camera:
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                   if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                       captureImage();
                   } else {
                       requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, CAMERA_PERMISSION_REQUEST_CODE);
                   }
               } else {
                   captureImage();
               }
               break;
           case R.id.feed_poll:
               Toast.makeText(getActivity(), "Poll Clicked!", Toast.LENGTH_SHORT).show();
               break;
           case R.id.feed_recorded_audio:
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                   if ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                       recordAudio();
                   } else {
                       requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE}, RECORD_PERMISSION_CODE);
                   }
               } else {
                   recordAudio();
               }
               break;
           case R.id.feed_music:
               checkPermissionAndGetAudio();
               break;
           case R.id.feed_video:
               checkPermissionAndGetVideo();
               break;
           case R.id.post_feed:
               switch (contentType) {
                   case "text":
                       if (!mainInput.getText().toString().isEmpty()) {
                           getTokenAndUploadData(MIME_TEXT);
                       } else {
                           Toast.makeText(getActivity(), "What's happening around you? Please tell us something!", Toast.LENGTH_SHORT).show();
                       }
                       break;
                   case "image/*":
                       if (!mainInput.getText().toString().isEmpty()) {
                           getTokenAndUploadData(MIME_IMAGES);
                       } else {
                           Toast.makeText(getActivity(), "What's happening around you? Please tell us something!", Toast.LENGTH_SHORT).show();
                       }
                       break;
                   case "audio/*":
                       if (!mainInput.getText().toString().isEmpty()) {
                           getTokenAndUploadData(MIME_AUDIOS);
                       } else {
                           Toast.makeText(getActivity(), "What's happening around you? Please tell us something!", Toast.LENGTH_SHORT).show();
                       }
                       break;
                   case "video/*":
                       if (!mainInput.getText().toString().isEmpty()) {
                           getTokenAndUploadData(MIME_VIDEOS);
                       } else {
                           Toast.makeText(getActivity(), "What's happening around you? Please tell us something!", Toast.LENGTH_SHORT).show();
                       }
                       break;
               }
               break;
           case R.id.draft_feed:
               Toast.makeText(getActivity(), "Saving feed as draft!", Toast.LENGTH_SHORT).show();
               break;
           case R.id.btnCloseThisActivity:
             //  finish();
               break;
           case R.id.feed_video_camera:
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                   if ((ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) && ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)))) {
                       captureVideo();
                   } else {
                       requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, CAMERA_PERMISSION_REQUEST_CODE);
                   }
               } else {
                   captureVideo();
               }
               break;
       }
   }

    private void changeView(boolean activate) {
        if (activate) {
            mainInput.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            mainInput.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void checkPermissionAndGetImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                getImages();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
            }
        } else {
            getImages();
        }
    }

    private void getImages() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, GET_IMAGE_REQUEST_CODE);
    }

    private void captureImage() {
        camera = new Camera.Builder()
                .resetToCorrectOrientation(true)
                .setTakePhotoRequestCode(CAPTURE_IMAGE_REQUEST_CODE)
                .setDirectory(Environment.getExternalStorageDirectory().getAbsolutePath())
                .setName("ali_" + System.currentTimeMillis())
                .setImageFormat(Camera.IMAGE_JPEG)
                .setCompression(75)
                .setImageHeight(1000)
                .build(this);
        try {
            camera.takePicture();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Failed to capture image!", Toast.LENGTH_SHORT).show();
        }
    }

    private void recordAudio() {
        File temp_file = new File(Environment.getExternalStorageDirectory() + File.separator + "Instafeed" + File.separator + ".tempAudio");
        if (!temp_file.exists()) {
            if (!temp_file.mkdirs()) {
                Toast.makeText(getActivity(), "Unable to create directory to save recorded audio!", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        String currentTimeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "AUD_" + currentTimeStamp + "_audio.wav";
        recordedAudioFilePath = Environment.getExternalStorageDirectory() + File.separator + "Instafeed" + File.separator + ".tempAudio" + File.separator + fileName;
        int color = getResources().getColor(R.color.colorPrimaryDark);
        AndroidAudioRecorder.with(this)
                // Required
                .setFilePath(recordedAudioFilePath)
                .setColor(color)
                .setRequestCode(AUDIO_INTENT_REQUEST_CODE)

                // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(true)
                .setKeepDisplayOn(true)

                // Start recording
                .record();
    }

    private void checkPermissionAndGetAudio() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                getAudio();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
            }
        } else {
            getAudio();
        }
    }

    private void getAudio() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("audio/*");
        startActivityForResult(intent, GET_AUDIO_REQUEST_CODE);
    }

    private void checkPermissionAndGetVideo() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                getVideos();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
            }
        } else {
            getVideos();
        }
    }

    private void getVideos() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("video/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, GET_VIDEO_REQUEST_CODE);
    }

    private void captureVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(intent, VIDEO_CAPTURE_REQUEST_CODE);
    }

    private void compressImageAndUpload(final String token) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                compressedFilesList.clear();
                int counter = 0;
                File destinationFile = new File(Environment.getExternalStorageDirectory(), "Instafeed" + File.separator + ".tempIMG");
                if (!destinationFile.exists()) {
                    if (destinationFile.mkdirs()) {
                        Log.d("APP_LOG:", "Directory Created!");
                    } else {
                        Log.d("APP_LOG:", "Directory Not Created!");
                        Toast.makeText(getActivity(), "Can't upload images! May be you haven't allowed us to read/write your device storage.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                final int currentNotificationID = coreHelper.generateUniqueID();
                notification = new NotificationCompat.Builder(getActivity(), CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_upload)
                        .setContentTitle("Project MK23")
                        .setContentText("Preparing to upload ...")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setProgress(100, 0, true)
                        .setOngoing(true)
                        .setOnlyAlertOnce(true);
                notificationManagerCompat.notify(currentNotificationID, notification.build());
                for (int i = 0; i < imagesList.size(); i++) {
                    counter++;
                    final File actualImage = new File(FileUtils.getPath(getApplicationContext(), imagesList.get(i).getImageURI()));
                    try {
                        final File file = new Compressor(getApplicationContext())
                                .setQuality(50)
                                .setDestinationDirectoryPath(destinationFile.getAbsolutePath())
                                .compressToFile(actualImage);
                        final int finalCounter = counter;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                compressedFilesList.add(file);
                                if (finalCounter == imagesList.size()) {
                                    uploadImages(currentNotificationID, token);
                                }
                            }
                        });
                    } catch (final IOException e) {
                        e.printStackTrace();
                        final int finalCounter = counter;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (finalCounter == imagesList.size()) {
                                    uploadImages(currentNotificationID, token);
                                }else{
                                    notification.setSmallIcon(R.drawable.ic_cancel);
                                    notification.setContentText("Something went wrong while uploading images! Upload Failed.").setProgress(0, 0, false).setOngoing(false);
                                    notificationManagerCompat.notify(currentNotificationID, notification.build());
                                }
                                Log.e("APP_LOG", "Error:" + e.getMessage());
                                Toast.makeText(getActivity(), "Something went wrong with " + actualImage.getName() + "!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        });
    }

    private void uploadImages(final int currentNotificationID, final String token)  {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                File destinationFile = new File(Environment.getExternalStorageDirectory(), "Instafeed" + File.separator + ".tempIMG");
                if (!destinationFile.exists()) {
                    if (destinationFile.mkdirs()) {
                        Log.d("APP_LOG:", "Directory Created!");
                    } else {
                        Log.d("APP_LOG:", "Directory Not Created!");
                        Toast.makeText(getActivity(), "Can't upload images! May be you haven't allowed us to read/write your device storage.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                notification.setContentText("Upload in progress...").setProgress(0, 0, true).setOngoing(true);
                notificationManagerCompat.notify(currentNotificationID, notification.build());
                MultipartBody.Part bodyImage = null;
                MultipartBody.Part bodyImage1 = null;
                MultipartBody.Part bodyImage2 = null;
                MultipartBody.Part bodyImage3 = null;
                MultipartBody.Part bodyImage4 = null;
                MultipartBody.Part bodyImage5 = null;
                MultipartBody.Part bodyImage6 = null;
                MultipartBody.Part bodyImage7 = null;
                MultipartBody.Part bodyImage8 = null;
                MultipartBody.Part bodyImage9 = null;
                MultipartBody.Part bodyImage10 = null;
                for (int i = 0; i < compressedFilesList.size(); i++) {
                    File file = compressedFilesList.get(i);
                    //RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
                    switch (i) {
                        case 0:
                            bodyImage = MultipartBody.Part.createFormData("image", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 1. Name: " + file.getName());
                            break;
                        case 1:
                            bodyImage1 = MultipartBody.Part.createFormData("image1", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 2. Name: " + file.getName());
                            break;
                        case 2:
                            bodyImage2 = MultipartBody.Part.createFormData("image2", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 3. Name: " + file.getName());
                            break;
                        case 3:
                            bodyImage3 = MultipartBody.Part.createFormData("image3", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 4. Name: " + file.getName());
                        case 4:
                            bodyImage4 = MultipartBody.Part.createFormData("image4", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 5. Name: " + file.getName());
                            break;
                        case 5:
                            bodyImage5 = MultipartBody.Part.createFormData("image5", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 6. Name: " + file.getName());
                            break;
                        case 6:
                            bodyImage6 = MultipartBody.Part.createFormData("image6", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 7. Name: " + file.getName());
                            break;
                        case 7:
                            bodyImage7 = MultipartBody.Part.createFormData("image7", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 8. Name: " + file.getName());
                            break;
                        case 8:
                            bodyImage8 = MultipartBody.Part.createFormData("image8", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 9. Name: " + file.getName());
                            break;
                        case 9:
                            bodyImage9 = MultipartBody.Part.createFormData("image9", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 10. Name: " + file.getName());
                            break;
                        case 10:
                            bodyImage10 = MultipartBody.Part.createFormData("image10", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                            Log.d("APP_LOG:IMG_ASS", "Assigned image 11. Name: " + file.getName());
                            break;
                    }
                }
                RequestBody fToken = RequestBody.create(MediaType.parse("text/plain"), token);
                RequestBody fTitle = RequestBody.create(MediaType.parse("text/plain"), mainInput.getText().toString());
                RequestBody fCategory_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
                RequestBody fDescription = RequestBody.create(MediaType.parse("text/plain"), mainInput.getText().toString());
                RequestBody fLocation_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
                RequestBody fLat = RequestBody.create(MediaType.parse("text/plain"), "123456");
                RequestBody fLong = RequestBody.create(MediaType.parse("text/plain"), "234567");
                RequestBody fMod = RequestBody.create(MediaType.parse("text/plain"), "n");
                RequestBody fIs_An = RequestBody.create(MediaType.parse("text/plain"), "N");
                RequestBody fLang_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
                RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
                Call<String> call = requestInterface.uploadMulImages(fToken, fTitle, fCategory_ID, fDescription, bodyImage, bodyImage1, bodyImage2, bodyImage3, bodyImage4, bodyImage5, bodyImage6, bodyImage7, bodyImage8, bodyImage9, bodyImage10, fLocation_ID, fLat, fLong, fMod, fIs_An, fLang_ID);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, final Response<String> response) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    Log.d("APP_LOG:File_Uploaded", "Response: " + response.raw());
                                    Toast.makeText(getActivity(), "Files Uploaded Successfully!", Toast.LENGTH_SHORT).show();
                                    notification.setSmallIcon(R.drawable.ic_done);
                                    notification.setContentText("Uploaded all files successfully!").setProgress(0, 0, false).setOngoing(false);
                                    notificationManagerCompat.notify(currentNotificationID, notification.build());
                                } else {
                                    Log.d("APP_LOG:File_Uploaded", "Response: " + response.raw());
                                    Toast.makeText(getActivity(), "Couldn't upload files!", Toast.LENGTH_SHORT).show();
                                    notification.setSmallIcon(R.drawable.ic_cancel);
                                    notification.setContentText("Something went wrong while uploading images! Upload Failed.").setProgress(0, 0, false).setOngoing(false);
                                    notificationManagerCompat.notify(currentNotificationID, notification.build());
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<String> call, final Throwable t) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("APP_LOG:Upload_Error:", "Error Message: " + t.getMessage());
                                Toast.makeText(getActivity(), "Couldn't upload files!", Toast.LENGTH_SHORT).show();
                                notification.setSmallIcon(R.drawable.ic_cancel);
                                notification.setContentText("Something went wrong while uploading images! Upload Failed.").setProgress(0, 0, false).setOngoing(false);
                                notificationManagerCompat.notify(currentNotificationID, notification.build());
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GET_IMAGE_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    contentType = "image/*";
                    recyclerView.setAdapter(imagesAdapter);
                    musicList.clear();
                    musicAdapter.notifyDataSetChanged();
                    videoList.clear();
                    videoAdapter.notifyDataSetChanged();
                    changeView(true);
                    ClipData clipData = data.getClipData();
                    if (clipData != null) {
                        for (int i = 0; i < clipData.getItemCount(); i++) {
                            Uri uri = clipData.getItemAt(i).getUri();
                            if (!(imagesList.size() > 10)) {
                                MK23_ImagesModel model = new MK23_ImagesModel(uri);
                                if (!imagesList.contains(model)) {
                                    imagesList.add(model);
                                } else {
                                    Toast.makeText(getActivity(), coreHelper.getFileNameFromUri(uri) + " is already selected!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Unable to add " + coreHelper.getFileNameFromUri(uri) + " because you are allowed to pick maximum 11 images only!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        imagesAdapter.notifyDataSetChanged();
                    } else {
                        Uri uri = data.getData();
                        if (!(imagesList.size() > 10)) {
                            MK23_ImagesModel model = new MK23_ImagesModel(uri);
                            if (!imagesList.contains(model)) {
                                imagesList.add(model);
                            } else {
                                Toast.makeText(getActivity(), coreHelper.getFileNameFromUri(uri) + " is already selected!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Unable to add " + coreHelper.getFileNameFromUri(uri) + " because you are allowed to pick maximum 11 images only!", Toast.LENGTH_SHORT).show();
                        }
                        imagesAdapter.notifyDataSetChanged();
                    }
                }
                break;
            case GET_AUDIO_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    contentType = "audio/*";
                    recyclerView.setAdapter(musicAdapter);
                    musicList.clear();
                    musicAdapter.notifyDataSetChanged();
                    imagesList.clear();
                    imagesAdapter.notifyDataSetChanged();
                    videoList.clear();
                    videoAdapter.notifyDataSetChanged();
                    changeView(true);
                    Uri uri = data.getData();
                    MK23_MusicModel model = new MK23_MusicModel(coreHelper.getFileNameFromUri(uri), uri);
                    if (!musicList.contains(model)) {
                        musicList.add(model);
                    } else {
                        Toast.makeText(getActivity(), coreHelper.getFileNameFromUri(uri) + " is already selected!", Toast.LENGTH_SHORT).show();
                    }
                    musicAdapter.notifyDataSetChanged();
                }
                break;
            case GET_VIDEO_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    contentType = "video/*";
                    recyclerView.setAdapter(videoAdapter);
                    musicList.clear();
                    musicAdapter.notifyDataSetChanged();
                    imagesList.clear();
                    imagesAdapter.notifyDataSetChanged();
                    changeView(true);
                    ClipData clipData = data.getClipData();
                    if (clipData != null) {
                        for (int i = 0; i < clipData.getItemCount(); i++) {
                            Uri uri = clipData.getItemAt(i).getUri();
                            if (!(videoList.size() > 10)) {
                                MK23_VideoModel model = new MK23_VideoModel(uri);
                                if (!videoList.contains(model)) {
                                    videoList.add(model);
                                } else {
                                    Toast.makeText(getActivity(), coreHelper.getFileNameFromUri(uri) + " is already selected!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Unable to add " + coreHelper.getFileNameFromUri(uri) + " because you are allowed to pick maximum 11 images only!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        videoAdapter.notifyDataSetChanged();
                    } else {
                        Uri uri = data.getData();
                        if (!(videoList.size() > 10)) {
                            MK23_VideoModel model = new MK23_VideoModel(uri);
                            if (!videoList.contains(model)) {
                                videoList.add(model);
                            } else {
                                Toast.makeText(getActivity(), coreHelper.getFileNameFromUri(uri) + " is already selected!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Unable to add " + coreHelper.getFileNameFromUri(uri) + " because you are allowed to pick maximum 11 images only!", Toast.LENGTH_SHORT).show();
                        }
                        videoAdapter.notifyDataSetChanged();
                    }
                }
                break;
            case VIDEO_CAPTURE_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    contentType = "video/*";
                    recyclerView.setAdapter(videoAdapter);
                    musicList.clear();
                    musicAdapter.notifyDataSetChanged();
                    imagesList.clear();
                    imagesAdapter.notifyDataSetChanged();
                    changeView(true);
                    Uri uri = data.getData();
                    Log.d("APP_LOG:Vid_Cap_URI", uri.toString());
                    videoList.add(new MK23_VideoModel(uri));
                    videoAdapter.notifyDataSetChanged();
                }
                break;
            case AUDIO_INTENT_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Toast.makeText(getActivity(), "Recording saved...", Toast.LENGTH_SHORT).show();
                    File file = new File(recordedAudioFilePath);
                    Uri uri = Uri.fromFile(file);
                    contentType = "audio/*";
                    recyclerView.setAdapter(musicAdapter);
                    musicList.clear();
                    musicAdapter.notifyDataSetChanged();
                    imagesList.clear();
                    imagesAdapter.notifyDataSetChanged();
                    videoList.clear();
                    videoAdapter.notifyDataSetChanged();
                    changeView(true);
                    MK23_MusicModel model = new MK23_MusicModel(coreHelper.getFileNameFromUri(uri), uri);
                    if (!musicList.contains(model)) {
                        musicList.add(model);
                    } else {
                        Toast.makeText(getActivity(), coreHelper.getFileNameFromUri(uri) + " is already selected!", Toast.LENGTH_SHORT).show();
                    }
                    musicAdapter.notifyDataSetChanged();
                    Log.d("APP_LOG:AUD_URI", uri.toString());
                }
                break;
            case CAPTURE_IMAGE_REQUEST_CODE:
                Bitmap bitmap = camera.getCameraBitmap();
                if (bitmap != null) {
                    File file = coreHelper.saveCapturedBitmap(bitmap);
                    if (file != null) {
                        contentType = "image/*";
                        recyclerView.setAdapter(imagesAdapter);
                        musicList.clear();
                        musicAdapter.notifyDataSetChanged();
                        videoList.clear();
                        videoAdapter.notifyDataSetChanged();
                        changeView(true);
                        Uri uri = Uri.fromFile(file);
                        if (!(imagesList.size() > 10)) {
                            MK23_ImagesModel model = new MK23_ImagesModel(uri);
                            if (!imagesList.contains(model)) {
                                imagesList.add(model);
                            } else {
                                Toast.makeText(getActivity(), coreHelper.getFileNameFromUri(uri) + " is already selected!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Unable to add " + coreHelper.getFileNameFromUri(uri) + " because you are allowed to pick maximum 11 images only!", Toast.LENGTH_SHORT).show();
                        }
                        imagesAdapter.notifyDataSetChanged();
                    }
                    //Toast.makeText(this.getApplicationContext(),"Picture taken!",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Cancelled!", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_PERMISSION_CODE:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(getActivity(), "Storage permission granted!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Storage permission denied!", Toast.LENGTH_SHORT).show();
                }
                break;
            case CAMERA_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && (grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED))) {
                    Toast.makeText(getActivity(), "Permission granted!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Permission denied!", Toast.LENGTH_SHORT).show();
                }
                break;
            case RECORD_PERMISSION_CODE:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    recordAudio();
                } else {
                    Toast.makeText(getActivity(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;
            case ALL_PERMISSION_REQUEST:
                if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED)) {
                    coreHelper.createCoreDirectories();
                } else {
                    Toast.makeText(getActivity(), "Necessary permissions denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    public void getTokenAndUploadData(final String operation){
        String accessURL = "http://13.234.116.90/api/login?email="+getResources().getString(R.string.tokenAccessEmail)+"&password="+getResources().getString(R.string.tokenAccessPassword);
        StringRequest request = new StringRequest(Request.Method.POST, accessURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("APP_LOG:TOKEN_RESPONSE", response);
                try {
                    JSONObject object = new JSONObject(response).getJSONObject("data");
                    String token = object.getString("token");
                    Log.d("APP_LOG:Token", "New Token: " + token);
                    switch (operation){
                        case MIME_TEXT:
                            uploadSimpleMessage(token);
                            break;
                        case MIME_IMAGES:
                            compressImageAndUpload(token);
                            break;
                        case MIME_VIDEOS:
                            compressVideoAndUpload(token);
                            break;
                        case MIME_AUDIOS:
                            uploadAudio(token);
                            break;
                    }

                    //sharedPreferences.putAccessTokenAndTime(token);
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "Failed to authenticate! Can't feed right now.", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Failed to authenticate! Can't feed right now.", Toast.LENGTH_SHORT).show();
                Log.d("APP_LOG:TOKEN_RESPONSE", error.toString());
            }
        });
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(request);
    }

    private void uploadSimpleMessage(String token) {
        RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);

        RequestBody fToken = RequestBody.create(MediaType.parse("text/plain"), token);
        RequestBody fTitle = RequestBody.create(MediaType.parse("text/plain"), mainInput.getText().toString());
        RequestBody fCategory_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody fDescription = RequestBody.create(MediaType.parse("text/plain"), mainInput.getText().toString());
        RequestBody fLocation_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody fLat = RequestBody.create(MediaType.parse("text/plain"), "123456");
        RequestBody fLong = RequestBody.create(MediaType.parse("text/plain"), "234567");
        RequestBody fMod = RequestBody.create(MediaType.parse("text/plain"), "n");
        RequestBody fIs_An = RequestBody.create(MediaType.parse("text/plain"), "N");
        RequestBody fLang_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
        Call<String> call = requestInterface.uploadSimpleMessage(fToken, fTitle, fCategory_ID, fDescription, fLocation_ID, fLat, fLong, fMod, fIs_An, fLang_ID);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    Log.d("APP_LOG:Server Response", "Response: "+response.toString());
                    Toast.makeText(getActivity(), ""+response.toString(), Toast.LENGTH_SHORT).show();
                }else{
                    Log.d("APP_LOG:Server Response", "Response: "+response.toString() +" || Error Body: "+response.body() + response.code());
                    Toast.makeText(getActivity(), "Failed to post!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e("APP_LOG:Server Error", ""+t.toString());
                Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void uploadAudio(final String token) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                final int currentNotificationID = coreHelper.generateUniqueID();
                notification = new NotificationCompat.Builder(getActivity(), CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_upload)
                        .setContentTitle("Project MK23")
                        .setContentText("Upload in progress...")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setProgress(100, 0, true)
                        .setOngoing(true)
                        .setOnlyAlertOnce(true);
                notificationManagerCompat.notify(currentNotificationID, notification.build());
                MultipartBody.Part bodyAudio = null;
                for (int i = 0; i < musicList.size(); i++) {
                    File file = FileUtils.getFile(getApplicationContext(), musicList.get(i).getMusicUri());
                    if (i == 0) {
                        bodyAudio = MultipartBody.Part.createFormData("audio", file.getName(), RequestBody.create(MediaType.parse("audio/*"), file));
                        Log.d("APP_LOG:AUD_ASS", "Assigned audio 1. Name: " + file.getName());
                    }
                }
                RequestBody fToken = RequestBody.create(MediaType.parse("text/plain"), token);
                RequestBody fTitle = RequestBody.create(MediaType.parse("text/plain"), mainInput.getText().toString());
                RequestBody fCategory_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
                RequestBody fDescription = RequestBody.create(MediaType.parse("text/plain"), mainInput.getText().toString());
                RequestBody fLocation_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
                RequestBody fLat = RequestBody.create(MediaType.parse("text/plain"), "123456");
                RequestBody fLong = RequestBody.create(MediaType.parse("text/plain"), "234567");
                RequestBody fMod = RequestBody.create(MediaType.parse("text/plain"), "n");
                RequestBody fIs_An = RequestBody.create(MediaType.parse("text/plain"), "N");
                RequestBody fLang_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
                RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);

                Call<String> call = requestInterface.uploadSingleAudio(fToken, fTitle, fCategory_ID, fDescription, bodyAudio, fLocation_ID, fLat, fLong, fMod, fIs_An, fLang_ID);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, final Response<String> response) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    Log.d("APP_LOG:File_Uploaded", "Response: " + response.raw());
                                    Toast.makeText(getActivity(), "Files Uploaded Successfully!", Toast.LENGTH_SHORT).show();
                                    notification.setSmallIcon(R.drawable.ic_done);
                                    notification.setContentText("Uploaded audio successfully!").setProgress(0, 0, false).setOngoing(false);
                                    notificationManagerCompat.notify(currentNotificationID, notification.build());
                                } else {
                                    Log.d("APP_LOG:File_Uploaded", "Response: " + response.raw());
                                    Toast.makeText(getActivity(), "Couldn't upload file!", Toast.LENGTH_SHORT).show();
                                    notification.setSmallIcon(R.drawable.ic_cancel);
                                    notification.setContentText("Something went wrong while uploading audio! Upload Failed.").setProgress(0, 0, false).setOngoing(false);
                                    notificationManagerCompat.notify(currentNotificationID, notification.build());
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<String> call, final Throwable t) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("APP_LOG:Upload_Error:", "Error Message: " + t.getMessage());
                                Toast.makeText(getActivity(), "Couldn't upload file!", Toast.LENGTH_SHORT).show();
                                notification.setSmallIcon(R.drawable.ic_cancel);
                                notification.setContentText("Something went wrong while uploading audios! Upload Failed.").setProgress(0, 0, false).setOngoing(false);
                                notificationManagerCompat.notify(currentNotificationID, notification.build());
                            }
                        });
                    }
                });
            }
        });
    }

    private void compressVideoAndUpload(final String token) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                compressedFilesList.clear();
                int counter = 0;
                File destinationFile = new File(Environment.getExternalStorageDirectory(), "Instafeed" + File.separator + ".tempVids");
                if (!destinationFile.exists()) {
                    if (destinationFile.mkdirs()) {
                        Log.d("APP_LOG:", "Directory Created!");
                    } else {
                        Log.d("APP_LOG:", "Directory Not Created!");
                        Toast.makeText(getActivity(), "Can't upload videos! May be you haven't allowed us to read/write your device storage.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                final int currentNotificationID = coreHelper.generateUniqueID();
                notification = new NotificationCompat.Builder(getActivity(), CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_upload)
                        .setContentTitle("Project MK23")
                        .setContentText("Preparing to upload ...")
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setProgress(100, 0, true)
                        .setOngoing(true)
                        .setOnlyAlertOnce(true);
                notificationManagerCompat.notify(currentNotificationID, notification.build());
                for (int i = 0; i < videoList.size(); i++) {
                    counter++;
                    final File actualVideo = new File(FileUtils.getPath(getApplicationContext(), videoList.get(i).getVideoURI()));
                    try {
                        final String file = SiliCompressor.with(getApplicationContext()).compressVideo(actualVideo.getAbsolutePath(), destinationFile.getAbsolutePath());
                        final int finalCounter = counter;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                compressedFilesList.add(new File(file));
                                if (finalCounter == videoList.size()) {
                                    uploadVideo(currentNotificationID, token);
                                }
                            }
                        });
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        notification.setSmallIcon(R.drawable.ic_cancel);
                        notification.setContentText("Something went wrong while preparing videos for upload! Upload Failed.").setProgress(0, 0, false).setOngoing(false);
                        notificationManagerCompat.notify(currentNotificationID, notification.build());
                    }
                }
            }
        });
    }

    private void uploadVideo(final int currentNotificationID, final String token) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                File destinationFile = new File(Environment.getExternalStorageDirectory(), "Instafeed" + File.separator + ".tempVideo");
                if (!destinationFile.exists()) {
                    if (destinationFile.mkdirs()) {
                        Log.d("APP_LOG:", "Directory Created!");
                    } else {
                        Log.d("APP_LOG:", "Directory Not Created!");
                        Toast.makeText(getActivity(), "Can't upload videos! May be you haven't allowed us to read/write your device storage.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                notification.setContentText("Upload in progress...").setProgress(0, 0, true).setOngoing(true);
                notificationManagerCompat.notify(currentNotificationID, notification.build());
                MultipartBody.Part bodyVideo = null;
                MultipartBody.Part bodyVideo1 = null;
                MultipartBody.Part bodyVideo2 = null;
                MultipartBody.Part bodyVideo3 = null;
                MultipartBody.Part bodyVideo4 = null;
                MultipartBody.Part bodyVideo5 = null;
                MultipartBody.Part bodyVideo6 = null;
                MultipartBody.Part bodyVideo7 = null;
                MultipartBody.Part bodyVideo8 = null;
                MultipartBody.Part bodyVideo9 = null;
                MultipartBody.Part bodyVideo10 = null;
                for (int i = 0; i < compressedFilesList.size(); i++) {
                    File file = compressedFilesList.get(i);
                    switch (i) {
                        case 0:
                            bodyVideo = MultipartBody.Part.createFormData("video", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 1. Name: " + file.getName());
                            break;
                        case 1:
                            bodyVideo1 = MultipartBody.Part.createFormData("video1", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 2. Name: " + file.getName());
                            break;
                        case 2:
                            bodyVideo2 = MultipartBody.Part.createFormData("video2", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 3. Name: " + file.getName());
                            break;
                        case 3:
                            bodyVideo3 = MultipartBody.Part.createFormData("video3", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 4. Name: " + file.getName());
                        case 4:
                            bodyVideo4 = MultipartBody.Part.createFormData("video4", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 5. Name: " + file.getName());
                            break;
                        case 5:
                            bodyVideo5 = MultipartBody.Part.createFormData("video5", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 6. Name: " + file.getName());
                            break;
                        case 6:
                            bodyVideo6 = MultipartBody.Part.createFormData("video6", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 7. Name: " + file.getName());
                            break;
                        case 7:
                            bodyVideo7 = MultipartBody.Part.createFormData("video7", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 8. Name: " + file.getName());
                            break;
                        case 8:
                            bodyVideo8 = MultipartBody.Part.createFormData("video8", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 9. Name: " + file.getName());
                            break;
                        case 9:
                            bodyVideo9 = MultipartBody.Part.createFormData("video9", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 10. Name: " + file.getName());
                            break;
                        case 10:
                            bodyVideo10 = MultipartBody.Part.createFormData("video10", file.getName(), RequestBody.create(MediaType.parse("video/*"), file));
                            Log.d("APP_LOG:VID_ASS", "Assigned video 11. Name: " + file.getName());
                            break;
                    }
                }
                RequestBody fToken = RequestBody.create(MediaType.parse("text/plain"), token);
                RequestBody fTitle = RequestBody.create(MediaType.parse("text/plain"), mainInput.getText().toString());
                RequestBody fCategory_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
                RequestBody fDescription = RequestBody.create(MediaType.parse("text/plain"), mainInput.getText().toString());
                RequestBody fLocation_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
                RequestBody fLat = RequestBody.create(MediaType.parse("text/plain"), "123456");
                RequestBody fLong = RequestBody.create(MediaType.parse("text/plain"), "234567");
                RequestBody fMod = RequestBody.create(MediaType.parse("text/plain"), "n");
                RequestBody fIs_An = RequestBody.create(MediaType.parse("text/plain"), "N");
                RequestBody fLang_ID = RequestBody.create(MediaType.parse("text/plain"), "1");
                RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
                Call<String> call = requestInterface.uploadMulVideos(fToken, fTitle, fCategory_ID, fDescription, bodyVideo, bodyVideo1, bodyVideo2, bodyVideo3, bodyVideo4, bodyVideo5, bodyVideo6, bodyVideo7, bodyVideo8, bodyVideo9, bodyVideo10, fLocation_ID, fLat, fLong, fMod, fIs_An, fLang_ID);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, final Response<String> response) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccessful()) {
                                    Log.d("APP_LOG:Video_Uploaded", "Response: " + response.raw());
                                    Toast.makeText(getActivity(), "Files Uploaded Successfully!", Toast.LENGTH_SHORT).show();
                                    notification.setSmallIcon(R.drawable.ic_done);
                                    notification.setContentText("Uploaded all files successfully!").setProgress(0, 0, false).setOngoing(false);
                                    notificationManagerCompat.notify(currentNotificationID, notification.build());
                                } else {
                                    Log.d("APP_LOG:Video_Uploaded", "Response: " + response.raw());
                                    Toast.makeText(getActivity(), "Couldn't upload files!", Toast.LENGTH_SHORT).show();
                                    notification.setSmallIcon(R.drawable.ic_cancel);
                                    notification.setContentText("Something went wrong while uploading videos! Upload Failed.").setProgress(0, 0, false).setOngoing(false);
                                    notificationManagerCompat.notify(currentNotificationID, notification.build());
                                }
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<String> call, final Throwable t) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("APP_LOG:Upload_Error:", "Error Message: " + t.getMessage());
                                Toast.makeText(getActivity(), "Couldn't upload files!", Toast.LENGTH_SHORT).show();
                                notification.setSmallIcon(R.drawable.ic_cancel);
                                notification.setContentText("Something went wrong while uploading videos! Upload Failed.").setProgress(0, 0, false).setOngoing(false);
                                notificationManagerCompat.notify(currentNotificationID, notification.build());
                            }
                        });
                    }
                });
            }
        });
    }



}
