package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;


/**
 * Created by Instafeed2 on 9/6/2019.
 */

public class NotificationSettingLayout extends Activity{
    public TextView citizen_feeds,supe_and_brand_feed,following_and_follwer,from_instafeed,other_notification_types,email_sms;
    Switch stop_all_notifications_switch;
    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_setting_layout);
        citizen_feeds = (TextView)findViewById(R.id.citizen_feeds);
        supe_and_brand_feed = (TextView)findViewById(R.id.supe_and_brand_feed);
        following_and_follwer = (TextView)findViewById(R.id.following_and_follwer);
        from_instafeed = (TextView)findViewById(R.id.from_instafeed);
        other_notification_types = (TextView)findViewById(R.id.other_notification_types);
        email_sms = (TextView)findViewById(R.id.email_sms);
        stop_all_notifications_switch = (Switch) findViewById(R.id.stop_all_notifications_switch);
        stop_all_notifications_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                } else {
                    // The toggle is disabled
                }
            }
        });
        citizen_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        supe_and_brand_feed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        following_and_follwer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        from_instafeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        other_notification_types.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        email_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }
}
