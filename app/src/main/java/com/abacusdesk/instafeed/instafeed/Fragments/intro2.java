package com.abacusdesk.instafeed.instafeed.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.R;

public class intro2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.introim, container, false);
        ImageView im=(ImageView)view.findViewById(R.id.im);
        im.setImageResource(R.drawable.two);
        return view;
    }
}