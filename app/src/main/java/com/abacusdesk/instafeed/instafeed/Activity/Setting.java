package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;

public class Setting extends Activity {
    LinearLayout social, emaillogin, l1, l2,logins;
    ImageView img_back;
    TextView loginss;
    LinearLayout followAndInvite,yourActivity,NotificationSetting,privacySetting,accountSetting,help,about;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);
        img_back=(ImageView)findViewById(R.id.img_back);
        loginss=(TextView)findViewById(R.id.loginss);
        logins=(LinearLayout) findViewById(R.id.logins);
        followAndInvite=(LinearLayout) findViewById(R.id.follow_invite_setting);
        yourActivity=(LinearLayout) findViewById(R.id.useracitivity_setting);
        NotificationSetting=(LinearLayout) findViewById(R.id.notify_setting);
        privacySetting=(LinearLayout) findViewById(R.id.privacy_setting);
        accountSetting=(LinearLayout) findViewById(R.id.account_setting);
        help=(LinearLayout) findViewById(R.id.help_setting);
        about=(LinearLayout) findViewById(R.id.about_setting);

        followAndInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this,followandinvitefrnd.class));
            }
        });
        yourActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this,followandinvitefrnd.class));
            }
        });
        NotificationSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this,followandinvitefrnd.class));
            }
        });
        privacySetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this,followandinvitefrnd.class));
            }
        });
        accountSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this,followandinvitefrnd.class));
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this,followandinvitefrnd.class));
            }
        });
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Setting.this,followandinvitefrnd.class));
            }
        });



        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        logins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loginss.getText().toString().trim().equalsIgnoreCase("Login")){
                    Intent in = new Intent(Setting.this,login.class);
                    startActivity(in);
                    finish();
                }else {

                }
            }
        });


    }
}
