package com.abacusdesk.instafeed.instafeed.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.Models.followlistingmodel;
import com.abacusdesk.instafeed.instafeed.Mycode.CommonMethod;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Responsemodel.forgotmodel;
import com.abacusdesk.instafeed.instafeed.Responsemodel.signupresponse;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class login extends FragmentActivity implements  GoogleApiClient.OnConnectionFailedListener {
    LinearLayout social, emaillogin, l1, l2,forgot;
    String email;
    Button phoneno,SignInb,fb,gm;
    TextInputEditText emailid,pass;
    TextView signup;
    private CallbackManager callbackManager;
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    String fname="",lname="",semail="",socialid="",provider="";
    String token;
    private int RC_SIGN_IN = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login);
        social=(LinearLayout)findViewById(R.id.social);
        forgot=(LinearLayout)findViewById(R.id.forgot);
        signup=(TextView)findViewById(R.id.signup);
        phoneno=(Button)findViewById(R.id.phoneno);
        emaillogin=(LinearLayout)findViewById(R.id.emaillogin);
        emailid=(TextInputEditText)findViewById(R.id.emailid);
        pass=(TextInputEditText)findViewById(R.id.pass);
        SignInb=(Button)findViewById(R.id.SignInb);
        fb=(Button)findViewById(R.id.fb);
        gm=(Button) findViewById(R.id.gm);
        l1=(LinearLayout)findViewById(R.id.l1);
        l2=(LinearLayout)findViewById(R.id.l2);
        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent in = new Intent(login.this,forgotpass.class);
                startActivity(in);
            }
        });
        l1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                l1.setBackgroundResource(R.drawable.leftside);
                l2.setBackgroundResource(R.drawable.rightside);
                hideupper(v);
            }
        });
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                l2.setBackgroundResource(R.drawable.rightsidegray);
                l1.setBackgroundResource(R.drawable.leftsidewithe);
                hidelower(v);
            }
        });
        phoneno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(login.this,loginwithmobile.class);
                startActivity(in);
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(login.this,SignUp.class);
                startActivity(in);
            }
        });
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facaBook();
            }
        });
        gm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInS();
            }
        });
        SignInb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailid.getText().toString().trim();
                pass.getText().toString().trim();
                SigIn();
            }
        });
        try{
            callbackManager = CallbackManager.Factory.create();
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            //Initializing google api client
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }catch (Exception e){

        }

    }
    public void hideupper(View v) {
        social.setVisibility(View.GONE);
        emaillogin.setVisibility(View.VISIBLE);
    }
    public void hidelower(View v) {
        social.setVisibility(View.VISIBLE);
        emaillogin.setVisibility(View.GONE);
    }
    private void SigIn() {
        RequestInterface requestInterface = ApiFactory.createService(login.this, RequestInterface.class);
        RequestBody Email = RequestBody.create(okhttp3.MultipartBody.FORM, emailid.getText().toString().trim());
        RequestBody passW = RequestBody.create(okhttp3.MultipartBody.FORM, pass.getText().toString().trim());
        RequestBody Token =RequestBody.create(okhttp3.MultipartBody.FORM, "Gaurav123456");
        Call<signupresponse> call = requestInterface.SignupLogin(Email, passW,Token);
        call.enqueue(new Callback<signupresponse>() {
            @Override
            public void onResponse(Call<signupresponse> call, Response<signupresponse> response) {

                if (response.isSuccessful()){
                    signupresponse loginResponse = response.body();
                        Log.e("loginResponse",loginResponse.toString());
                    if (!loginResponse.isError()) {
                        SaveSharedPreference.setToken(login.this, response.body().getData().getToken());
                        Intent in = new Intent(login.this, MainActivity.class);
                        startActivity(in);
                        finish();
                          } else {
                        Log.e("loginResponse1",loginResponse.toString());
                        Toast.makeText(login.this, loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else{
                    CommonMethod.showAlert1(" Incorrect User Name Or Password", login.this);
                    ResponseBody errorBody = response.errorBody();
                    Log.e("loginResponse3",errorBody.toString());
                    // check error.
                }

                   /* if (response.body().getStatus()==401) {

                        Log.e("Strring", "TEST" + response.body().getStatus().toString());
                        Log.e("Strring", "TEST" + response.body().getData().toString());
                        SaveSharedPreference.setToken(login.this, response.body().getData().getToken());

                    } else {
                        Log.e("Strring", "TEST" + response.body().getData().toString());
                        SaveSharedPreference.setToken(login.this, response.body().getData().getToken());
                        Intent in = new Intent(login.this, upload.class);
                        startActivity(in);
                        finish();


                        Log.e("String", "TEST111");
                    }*/

            }
            @Override
            public void onFailure(Call<signupresponse> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());

            }
        });

    }

    public void facaBook() {

            LoginManager.getInstance().logInWithReadPermissions(login.this, Arrays.asList("public_profile", "user_friends", "email"));
            callbackManager = CallbackManager.Factory.create();
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.e("access", "" + AccessToken.getCurrentAccessToken().getToken().toString());
                    RequestData1();
                }

                @Override
                public void onCancel() {
                    //Toast.makeText(Login.this, "Login cancelled by user!", Toast.LENGTH_LONG).show();
                    System.out.println("Facebook Login failed!!");
                }

                @Override
                public void onError(FacebookException exception) {
                    // Toast.makeText(Login.this, "Facebook Login failed!!", Toast.LENGTH_LONG).show();
                    System.out.println("Facebook Login failed!!");
                    System.out.println("Facebook Login " + exception.toString());
                    if (exception instanceof FacebookAuthorizationException) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut();
                        }
                    }
                }
            });
        }


    public void RequestData1() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                try {
                    if (json != null) {
                        fname= json.getString("first_name");
                        lname= json.getString("last_name");
                        socialid=  json.getString("id");
                        provider="facebook";
                        if (json.has("email")) {
                            semail=json.getString("email");
                        }
                        Log.e("responsesuccess",""+fname+lname+socialid+provider);
                       socialLogin();
                    }
                } catch (Exception e) {
                    // e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void socialLogin() {
        RequestInterface requestInterface = ApiFactory.createService(login.this, RequestInterface.class);
        RequestBody Sc = RequestBody.create(okhttp3.MultipartBody.FORM, socialid);
        RequestBody Pro = RequestBody.create(okhttp3.MultipartBody.FORM, provider);
        RequestBody Fname =RequestBody.create(okhttp3.MultipartBody.FORM, fname);
        RequestBody LName =RequestBody.create(okhttp3.MultipartBody.FORM, lname);
        RequestBody EName =RequestBody.create(okhttp3.MultipartBody.FORM, semail);
        RequestBody Device =RequestBody.create(okhttp3.MultipartBody.FORM, "device");
        Call<forgotmodel> call = requestInterface.social(Sc, Pro,Fname,LName,EName,Device);
        call.enqueue(new Callback<forgotmodel>() {
            @Override
            public void onResponse(Call<forgotmodel> call, Response<forgotmodel> response) {

                if (response.body().getMessage().equals("success")) {
                    try {
                        checkSocial(response.body().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CommonMethod.showAlert(response.body().getData().getMessage().toString().trim(),  login.this);
                    //  SaveSharedPreference.setToken(loginwithmobile.this,response.body().getData().getToken());
                } else {

                }

            }
            @Override
            public void onFailure(Call<forgotmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());

            }
        });
    }

    private void signInS() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling add new function to handle signin
            Log.e("res", "" + result.getStatus());
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.d("social googlweplus",""+acct.getId()+acct.getGivenName()+acct.getEmail());
            socialid=acct.getId();
            fname=acct.getGivenName();
            lname="";
            semail= acct.getEmail();
            provider="google";

           socialLogin();

            try{
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            }catch (Exception e){

            }
        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }

    private void checkSocial(String response) throws JSONException {
        JSONObject jsonObject= new JSONObject(response);
        if(!jsonObject.getJSONObject("data").has("error")){
            Log.e("log", "" + response);
            Toast.makeText(login.this,"Successfully logged into", Toast.LENGTH_SHORT).show();
            SaveSharedPreference.setUserID(login.this,jsonObject.getJSONObject("data").getString("user_id"));
            SaveSharedPreference.setPrefToken(login.this,jsonObject.getJSONObject("data").getString("token"));
            SaveSharedPreference.setUserIMAGE(login.this,"");
         /*   if (email!=null && email.matches("[0-9]+")){*/
                SaveSharedPreference.setMobileLogin(login.this,"true");
            /*}else{
                SaveSharedPreference.setMobileLogin(login.this,"false");
            }*/
            startActivity(new Intent(login.this,MainActivity.class)); 
            finish();
        }
        else if (jsonObject.getJSONObject("data").has("error")){
          //  Dialogs.showDialog(Login_Activity.this, jsonObject.getJSONObject("data").getString("error"));
        } else {
          //  Dialogs.showDialog(Login_Activity.this, "Server Failed");
        }
    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

  /*  public void socialLogin() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Apis.social,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("log", "" + response);
                        try {
                            checkSocial(response);
                        } catch (JSONException e) {
                            // e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialogs.disDialog();
                        DialogInterface.OnClickListener onClickTryAgain = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                socialLogin();
                            }
                        };
                        if (error instanceof TimeoutError) {


                        } else if (error instanceof NoConnectionError) {
                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("social_id",socialid);
                map.put("provider_id",provider);
                map.put("first_name",fname);
                map.put("last_name",lname);
                map.put("email",semail);
                map.put("device_id",dev_id);
                Log.e("responsesocial",""+map);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }*/

    private void Sub() {

        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(login.this, RequestInterface.class);
        RequestBody Token=RequestBody.create(MultipartBody.FORM,token);

        Call<followlistingmodel> call = requestInterface.followwer(Token);
        call.enqueue(new Callback<followlistingmodel>() {
            @Override
            public void onResponse(Call<followlistingmodel> call, final Response<followlistingmodel> response) {
                if(response.isSuccessful()){

                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                }else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }
            @Override
            public void onFailure(Call<followlistingmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}