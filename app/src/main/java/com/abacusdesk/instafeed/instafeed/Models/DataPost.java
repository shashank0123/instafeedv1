package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class DataPost {
    @SerializedName("news_id")
      
    private String newsId;
    @SerializedName("user_id")
      
    private String userId;
    @SerializedName("comment")
      
    private String comment;
    @SerializedName("status")
      
    private String status;

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
