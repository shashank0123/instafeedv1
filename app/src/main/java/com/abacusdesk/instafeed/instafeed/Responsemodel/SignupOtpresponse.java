package com.abacusdesk.instafeed.instafeed.Responsemodel;

import com.google.gson.annotations.SerializedName;

public class SignupOtpresponse {
    @SerializedName("status")
       
    private Integer status;
    @SerializedName("message")
       
    private String message;
    @SerializedName("data")

    private DataOTP data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataOTP getData() {
        return data;
    }

    public void setData(DataOTP data) {
        this.data = data;
    }

}
