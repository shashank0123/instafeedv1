package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
public class DataN {
    @SerializedName("news")
    private ArrayList<NewsN> news = null;
    @SerializedName("brands")
    private ArrayList<BrandN> brands = null;
    @SerializedName("star")
    private ArrayList<StarN> star = null;
    public ArrayList<NewsN> getNews() {
        return news;
    }
    public void setNews(ArrayList<NewsN> news) {
        this.news = news;
    }
    public ArrayList<BrandN> getBrands() {
        return brands;
    }
    public void setBrands(ArrayList<BrandN> brands) {
        this.brands = brands;
    }
    public ArrayList<StarN> getStar() {
        return star;
    }
    public void setStar(ArrayList<StarN> star) {
        this.star = star;
    }
}
