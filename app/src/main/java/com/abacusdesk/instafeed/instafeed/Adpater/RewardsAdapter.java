package com.abacusdesk.instafeed.instafeed.Adpater;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.Rewardpoint;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.Claimedmodel;
import com.abacusdesk.instafeed.instafeed.Models.DataRewards;
import com.abacusdesk.instafeed.instafeed.Models.Datalistfollow;
import com.abacusdesk.instafeed.instafeed.Models.isfollowmodel;
import com.abacusdesk.instafeed.instafeed.Models.rewardsmodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RewardsAdapter extends RecyclerView.Adapter<RewardsAdapter.MyViewHolder> {
    String id = "";
    String point = "";
    Context context;
    ArrayList<DataRewards> data;
    public static String comment = "", comment_id;
    Dialog dialog;
    String upperString;
    String token, username;

    public RewardsAdapter(ArrayList<DataRewards> data, Context context) {
        this.context = context;
        this.data = data;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, dis;
        public ImageView imge;
        CardView buy;

        public MyViewHolder(View view) {
            super(view);
            this.imge = (ImageView) itemView.findViewById(R.id.imge);
            this.name = (TextView) itemView.findViewById(R.id.name);
            this.dis = (TextView) itemView.findViewById(R.id.dis);
            this.buy = (CardView) itemView.findViewById(R.id.buy);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rewardrow, parent, false);
        token = SaveSharedPreference.getToken(context);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        Picasso.with(context).load(data.get(position).getImage()).into(holder.imge);

        holder.name.setText(data.get(position).getDescription());
        holder.dis.setText("REDEEM WITH " + data.get(position).getTotalPoints() + " pts");
        holder.buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id = data.get(position).getId();
                point = data.get(position).getTotalPoints();
                RewardsC();
            }
        });

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    private void RewardsC() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Gid =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, id);

        RequestBody Point =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, point);


        Call<Claimedmodel> call = requestInterface.rewardgifthistroy(Token, Gid, Point);
        call.enqueue(new Callback<Claimedmodel>() {
            @Override
            public void onResponse(Call<Claimedmodel> call,
                                   Response<Claimedmodel> response) {
                if (response.isSuccessful()) {
                    Intent in = new Intent(context, Rewardpoint.class);
                    context.startActivity(in);
                    ((Rewardpoint) context).finish();
                }
            }

            @Override
            public void onFailure(Call<Claimedmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

}
