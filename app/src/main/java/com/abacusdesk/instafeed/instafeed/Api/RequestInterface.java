package com.abacusdesk.instafeed.instafeed.Api;

import com.abacusdesk.instafeed.instafeed.Models.Citizen;
import com.abacusdesk.instafeed.instafeed.Models.Claimedmodel;
import com.abacusdesk.instafeed.instafeed.Models.Commentlistmodel;
import com.abacusdesk.instafeed.instafeed.Models.PeopleModel;
import com.abacusdesk.instafeed.instafeed.Models.Searchmodel;
import com.abacusdesk.instafeed.instafeed.Models.brand;
import com.abacusdesk.instafeed.instafeed.Models.brandmodelcat;
import com.abacusdesk.instafeed.instafeed.Models.brandpromodel;
import com.abacusdesk.instafeed.instafeed.Models.category;
import com.abacusdesk.instafeed.instafeed.Models.citizendetailmodel;
import com.abacusdesk.instafeed.instafeed.Models.citizenprofiledata;
import com.abacusdesk.instafeed.instafeed.Models.comentpostmodel;
import com.abacusdesk.instafeed.instafeed.Models.favmodel;
import com.abacusdesk.instafeed.instafeed.Models.favoritesModel;
import com.abacusdesk.instafeed.instafeed.Models.followlistingmodel;
import com.abacusdesk.instafeed.instafeed.Models.followmodel;
import com.abacusdesk.instafeed.instafeed.Models.followunfollowmodel;
import com.abacusdesk.instafeed.instafeed.Models.isfollowmodel;
import com.abacusdesk.instafeed.instafeed.Models.languages;
import com.abacusdesk.instafeed.instafeed.Models.likemodel;
import com.abacusdesk.instafeed.instafeed.Models.notificationlistingmodel;
import com.abacusdesk.instafeed.instafeed.Models.profilemodel;
import com.abacusdesk.instafeed.instafeed.Models.rewardsmodel;
import com.abacusdesk.instafeed.instafeed.Models.starmodel;
import com.abacusdesk.instafeed.instafeed.Models.userlikemodel;
import com.abacusdesk.instafeed.instafeed.Models.votestatus;
import com.abacusdesk.instafeed.instafeed.Responsemodel.forgotmodel;
import com.abacusdesk.instafeed.instafeed.Responsemodel.getbookmarkmodel;
import com.abacusdesk.instafeed.instafeed.Responsemodel.logoutresponse;
import com.abacusdesk.instafeed.instafeed.Responsemodel.mobileresponse;
import com.abacusdesk.instafeed.instafeed.Responsemodel.signupresponse;
import com.abacusdesk.instafeed.instafeed.Responsemodel.subscmodel;
import com.abacusdesk.instafeed.instafeed.Responsemodel.veryfyotp;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by ${Gaurav} on 6/10/2017.
 *///,,,,

public interface RequestInterface {

    @Multipart
    @POST("api/signup")
    Call<signupresponse> Signup(
            @Part("first_name") RequestBody first_name,
            @Part("email") RequestBody email,
            @Part("mobile") RequestBody mobile,
            @Part("password") RequestBody password,
            @Part("device_id") RequestBody device_id
    );
    @Multipart
    @POST("api/signup/otp")
    Call<signupresponse> SignupOTP(
            @Part("mobile") RequestBody mobile,
            @Part("password") RequestBody password,
            @Part("device_id") RequestBody device_id
    );
    @Multipart
    @POST("api/login")
    Call<signupresponse> SignupLogin(
            @Part("email") RequestBody email,
            @Part("password") RequestBody password,
            @Part("device_id") RequestBody device_id
    );
    @Multipart
    @POST("api/logout")
    Call<logoutresponse> Logout(
            @Part("token") RequestBody token
    );
    @GET("api/citizen/news")
    ErrorCallback.MyCall<Citizen> getvf(@Query("lang_id") String lang_id, @Query("token") String token, @Query("start") int start);
    @GET("api/citizen/news")
    ErrorCallback.MyCall<Citizen> getvfeed(/*@Query("lang_id") String lang_id,*/ @Query("start") int start);
    @GET("api/news/languages")
    ErrorCallback.MyCall<languages> getlang();
    @GET("api/citizen/recents")
    ErrorCallback.MyCall<category> category();
    @GET("api/news/category")
    ErrorCallback.MyCall<category> categorylist();
    @GET("api/brands/news")
    ErrorCallback.MyCall<brand> getbrand(@Query("lang_id") String lang_id, @Query("token") String token, @Query("start") String start);
    @GET("api/brands/news")
    ErrorCallback.MyCall<brand> getbrand1(@Query("lang_id") String lang_id, @Query("start") String start);
    @GET("api/star/news")
    ErrorCallback.MyCall<starmodel> getstar(@Query("lang_id") String lang_id, @Query("token") String token, @Query("start") String start);
    @GET("api/star/news")
    ErrorCallback.MyCall<starmodel> getstar1(@Query("lang_id") String lang_id, @Query("start") String start);
    @Multipart
    @POST("api/citizen/vote")
    Call<likemodel> vote(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("vote") RequestBody vote
    );
    @Multipart
    @POST("api/citizen/vote")
    Call<likemodel> voted(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("vote") RequestBody vote
    );
    @Multipart
    @POST("api/star/vote")
    Call<likemodel> votestar(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("vote") RequestBody vote
    );

    @Multipart
    @POST("api/star/vote")
    Call<likemodel> votedS(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("vote") RequestBody vote
    );

    @Multipart
    @POST("api/brands/vote")
    Call<likemodel> votebrand(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("vote") RequestBody vote
    );
    @Multipart
    @POST("api/brands/vote")
    Call<likemodel> votebrandD(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("vote") RequestBody vote
    );
     @Multipart
    @POST("api/citizen/comment")
    Call<comentpostmodel> commentciti(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("comment") RequestBody comment
    );
     @Multipart
    @POST("api/brands/comment")
    Call<comentpostmodel> commentBrand(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("comment") RequestBody comment
    );
     @Multipart
    @POST("api/star/comment")
    Call<comentpostmodel> commentStar(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("comment") RequestBody comment
    );
    @Multipart
    @POST("api/bookmark")
    Call<favmodel> fav(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id,
            @Part("module_id") RequestBody module_id

    );


    @Multipart
    @POST("api/unbookmark")
    Call<favmodel> Unbookmarks(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id, @Part("module_id") RequestBody module_id
    );

    @Multipart
    @POST("api/citizen/userLikeNews")
    Call<userlikemodel> userLikeNews(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id
    );
    @Multipart
    @POST("api/brands/userLikeNews")
    Call<userlikemodel> userLikeNewsB(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id
    );
    @Multipart
    @POST("api/star/userLikeNews")
    Call<userlikemodel> userLikeNewsS(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id
    );
    @Multipart
    @POST("api/profile")
    Call<profilemodel> userprofile(
            @Part("token") RequestBody token);
    @Multipart
    @POST("api/star-suggest")
    Call<favoritesModel> starSuggest(
            @Part("token") RequestBody token);


    @Multipart
    @POST("api/profile/public")
    Call<citizenprofiledata> userprofiles(
            @Part("username") RequestBody username);
   // http://13.234.116.90/api/profile/public
    @GET("api/citizen/comments")
    ErrorCallback.MyCall<Commentlistmodel> Comm(@Query("id") String id);
    @GET("api/brands/comments")
    ErrorCallback.MyCall<Commentlistmodel> CommB(@Query("id") String id);
    @GET("api/star/comments")
    ErrorCallback.MyCall<Commentlistmodel> CommS(@Query("id") String id);
    @Multipart
    @POST("api/profile/public/follow")
    Call<followmodel> Follow(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id
    );
    @Multipart
    @POST("api/profile/public/unfollow")
    Call<followmodel> UNFollow(
            @Part("token") RequestBody token,
            @Part("id") RequestBody id
    );
    @Multipart
    @POST("api/signup/otp")
    Call<mobileresponse> mobilelogin(
            @Part("mobile") RequestBody mobile,
            @Part("password") RequestBody password
    );
    @Multipart
    @POST("api/signup/otp/verify")
    Call<veryfyotp> mobileverify(
            @Part("mobile") RequestBody mobile,
            @Part("otp") RequestBody otp
    );
    @Multipart
    @POST("api/forgot")
    Call<forgotmodel> forgot(
            @Part("email") RequestBody mobile);
    @Multipart
    @POST(" api/social")
    Call<forgotmodel> social(
            @Part("social_id") RequestBody social_id,
            @Part("provider_id") RequestBody provider_id,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("email") RequestBody email,  @Part("device_id") RequestBody device_id
    );
    @Multipart
    @POST("api/getbookmarks")
    Call<getbookmarkmodel> getbookmarks(
            @Part("token") RequestBody token);
    @GET("api/brands/categories")
    ErrorCallback.MyCall<brandmodelcat> getcatrun(@Query("id") String id);

   @GET("api/brands/info")
   ErrorCallback.MyCall<brandpromodel> branprofiles(@Query("id") String id);

    @GET("api/brands/posts-all")
    ErrorCallback.MyCall<brand> branpidlisting(@Query("id") String id);

    @GET("api/brands/posts-all")
    ErrorCallback.MyCall<brand> branpidlis(@Query("cat_id") String id);

    @GET("api/profile/public/news")
    ErrorCallback.MyCall<Citizen> ublicnews(@Query("username") String username);

    @GET("api/citizen/posts")
    ErrorCallback.MyCall<citizendetailmodel> details(@Query("id") String id);

    @Multipart
    @POST("api/profile/public/follow")
    Call<followunfollowmodel> follow(
            @Part("token") RequestBody token, @Part("username") RequestBody username);
    @Multipart
    @POST("api/profile/public/unfollow")
    Call<followunfollowmodel> unfollow(
            @Part("token") RequestBody token, @Part("username") RequestBody username);

    @Multipart
    @POST("api/profile/followers")
    Call<followlistingmodel> followlist(
            @Part("token") RequestBody token);


    @Multipart
    @POST("api/profile/following")
    Call<followlistingmodel> followwer(
            @Part("token") RequestBody token);

    @GET("api/star/posts")
    ErrorCallback.MyCall<citizendetailmodel> detailss(@Query("id") String id);

    @GET("api/brands/posts")
    ErrorCallback.MyCall<citizendetailmodel> detailssb(@Query("id") String id);

    @Multipart
    @POST("api/set-sub")
    Call<subscmodel> Subc(
            @Part("token") RequestBody token);
    @Multipart
    @POST("api/citizen/isvote")
    Call<votestatus> isvote(
            @Part("token") RequestBody token, @Part("id") RequestBody id);
    @Multipart
    @POST("api/brands/comment/edit")
    Call<votestatus> commentedit(
            @Part("token") RequestBody token, @Part("id") RequestBody id);


    @Multipart
    @POST("api/search")
    Call<Searchmodel> Search(
            @Part("query") RequestBody query, @Part("type") RequestBody type); @Multipart
    @POST("api/search")
    Call<PeopleModel> SearchPeople(
            @Part("query") RequestBody query, @Part("type") RequestBody type);


    @Multipart
    @POST("api/profile/public/isfollow")
    Call<isfollowmodel> isfollow(
            @Part("token") RequestBody token, @Part("username") RequestBody username);



    @Multipart
    @POST("api/reward/rewardgift")
    Call<rewardsmodel> Rewards(
            @Part("token") RequestBody token);

    @Multipart
    @POST("api/reward/claimreward")
    Call<Claimedmodel> rewardgifthistroy(
            @Part("token") RequestBody token, @Part("gift_id") RequestBody gift_id, @Part("point") RequestBody point);


    @Multipart
    @POST("api/reward/rewardgifthistroy")
    Call<rewardsmodel> Rewardsh(
            @Part("token") RequestBody token);


    @Multipart
    @POST("api/notification/getnotify")
    Call<notificationlistingmodel> Notyfy(
            @Part("token") RequestBody token, @Part("lang_id") RequestBody lang_id);



    @Multipart
    @POST("api/brands/post")
    Call < String > uploadMulImages(
            @Part("token") RequestBody token,
            @Part("title") RequestBody title,
            @Part("category_id") RequestBody category_id,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part image,
            @Part MultipartBody.Part image1,
            @Part MultipartBody.Part image2,
            @Part MultipartBody.Part image3,
            @Part MultipartBody.Part image4,
            @Part MultipartBody.Part image5,
            @Part MultipartBody.Part image6,
            @Part MultipartBody.Part image7,
            @Part MultipartBody.Part image8,
            @Part MultipartBody.Part image9,
            @Part MultipartBody.Part image10,
            @Part("location_id") RequestBody location_id,
            @Part("lat") RequestBody latitude,
            @Part("long") RequestBody longitude,
            @Part("mod") RequestBody mod,
            @Part("is_an") RequestBody is_an,
            @Part("lang_id") RequestBody lang_id
    );

    @Multipart
    @POST("api/brands/post")
    Call < String > uploadMulVideos(
            @Part("token") RequestBody token,
            @Part("title") RequestBody title,
            @Part("category_id") RequestBody category_id,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part video,
            @Part MultipartBody.Part video1,
            @Part MultipartBody.Part video2,
            @Part MultipartBody.Part video3,
            @Part MultipartBody.Part video4,
            @Part MultipartBody.Part video5,
            @Part MultipartBody.Part video6,
            @Part MultipartBody.Part video7,
            @Part MultipartBody.Part video8,
            @Part MultipartBody.Part video9,
            @Part MultipartBody.Part video10,
            @Part("location_id") RequestBody location_id,
            @Part("lat") RequestBody latitude,
            @Part("long") RequestBody longitude,
            @Part("mod") RequestBody mod,
            @Part("is_an") RequestBody is_an,
            @Part("lang_id") RequestBody lang_id
    );


    @Multipart
    @POST("api/brands/post")
    Call < String > uploadSingleAudio(
            @Part("token") RequestBody token,
            @Part("title") RequestBody title,
            @Part("category_id") RequestBody category_id,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part audio,
            @Part("location_id") RequestBody location_id,
            @Part("lat") RequestBody latitude,
            @Part("long") RequestBody longitude,
            @Part("mod") RequestBody mod,
            @Part("is_an") RequestBody is_an,
            @Part("lang_id") RequestBody lang_id
    );

    //#Working piece of code
    @Multipart
    @POST("api/brands/post")
    Call< String > uploadSimpleMessage(
            @Part("token") RequestBody token,
            @Part("title") RequestBody title,
            @Part("category_id") RequestBody category_id,
            @Part("description") RequestBody description,
            @Part("location_id") RequestBody location_id,
            @Part("lat") RequestBody latitude,
            @Part("long") RequestBody longitude,
            @Part("mod") RequestBody mod,
            @Part("is_an") RequestBody is_an,
            @Part("lang_id") RequestBody lang_id
    );
}

