package com.abacusdesk.instafeed.instafeed.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abacusdesk.instafeed.instafeed.Adpater.Feedadapter;
import com.abacusdesk.instafeed.instafeed.Adpater.staradapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.starmodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;


import java.util.ArrayList;

public class Superstar extends Fragment {
    RecyclerView recyclerView;
    staradapter mstaradapter;
    String Token="";
    int index=0;
    ArrayList ab;
    SwipeRefreshLayout swipeRefreshLayout;
    private AdView adView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.brand, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView);
        MobileAds.initialize(getActivity(), "ca-app-pub-3940256099942544~3347511713");
        adView = (AdView)view.findViewById(R.id.adView);
        AdRequest request = new AdRequest.Builder().build();
        adView.loadAd(request);
 /*       LinearLayoutManager manager1 = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager1);*/
 ab= new ArrayList();
        final LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        mstaradapter= new staradapter(getActivity(),ab);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(mstaradapter);
        Token= SaveSharedPreference.getToken(getActivity());
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.simpleSwipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                    index = 0;
                    ab.clear();
                    listener.resetState();
                    Star(true);
                    // swipeRefreshLayout.setRefreshing(false);  // This hides the spinner
                }
            }
        });
      Star(false);
        listener = new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.e("index", "index" +index);
                Star(false);
            }
        };
         recyclerView.addOnScrollListener(listener);
        return view;
    }
    private EndlessRecyclerViewScrollListener listener;
    private void Star(final boolean shouldRefresh) {
        Log.e("contryFF", "started...");
        final RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        ErrorCallback.MyCall<starmodel> myCall;
        if (!Token.equals("")) {
         //   Log.e("indexSper",""+index);
            myCall = requestInterface.getstar( "1",Token ,String.valueOf(index));
        } else {
            myCall = requestInterface.getstar1("1", String.valueOf(index));
        }
        myCall.enqueue(new ErrorCallback.MyCallback<starmodel>() {
            @Override
            public void success(final retrofit2.Response<starmodel> res) {
                getActivity().runOnUiThread(new  Runnable() {
                    @Override
                    public void run() {
                       /* mstaradapter= new staradapter(getActivity(),res.body().getData());
                        recyclerView.setAdapter(mstaradapter);
                        Log.e("setData", "shouldRefresh"+shouldRefresh);*/
                        if (mstaradapter != null) {
                            swipeRefreshLayout.setRefreshing(false);
                            if (shouldRefresh)
                                mstaradapter.refreshData(res.body().getData());
                            else

                                mstaradapter.setData(res.body().getData());

                        }
                        ++index;
                    }
                });
            }
            @Override
            public void error(String errorMessage) {
            }
        }, getActivity(), Boolean.FALSE);
    }
    @Override
    public void onResume() {
        super.onResume();
        index=0;
        Log.e("indexStar",""+"onResume");
    }
}