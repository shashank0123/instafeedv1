package com.abacusdesk.instafeed.instafeed.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Adpater.tabruntym;
import com.abacusdesk.instafeed.instafeed.Fragments.intor3;
import com.abacusdesk.instafeed.instafeed.Fragments.intro1;
import com.abacusdesk.instafeed.instafeed.Fragments.intro2;
import com.abacusdesk.instafeed.instafeed.Fragments.intro4;
import com.abacusdesk.instafeed.instafeed.Fragments.intro5;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import me.relex.circleindicator.CircleIndicator;

public class Introscreeen extends FragmentActivity {
    private ViewPager viewPager;
    private tabruntym adapter;
    int currentposition;
    ImageView next;
    TextView skip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inro);
        viewPager = (ViewPager)  findViewById(R.id.pager);
        adapter = new tabruntym(getSupportFragmentManager());
        next=(ImageView)findViewById(R.id.next);
        skip=(TextView)findViewById(R.id.skip);
        adapter.addFragment(new intro1(), "Citizen");
        adapter.addFragment(new intro2(), "Superstar");
        adapter.addFragment(new intor3(), "Brand");
        adapter.addFragment(new intro4(), "Brand");
        adapter.addFragment(new intro5(), "Brand");
        viewPager.setAdapter(adapter);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                currentposition=position;

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveSharedPreference.setIsSplash(Introscreeen.this,"true");
                startActivity(new Intent(Introscreeen.this, MainActivity.class));
                finish();
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(getItem(+1), true);
            }
        });


    }
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }
}
