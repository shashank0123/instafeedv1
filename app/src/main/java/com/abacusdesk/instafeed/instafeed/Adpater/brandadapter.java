package com.abacusdesk.instafeed.instafeed.Adpater;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Activity.Citizendetailpage;
import com.abacusdesk.instafeed.instafeed.Activity.commentactivity;
import com.abacusdesk.instafeed.instafeed.Activity.sample;
import com.abacusdesk.instafeed.instafeed.Activity.who_like;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.Databrand;
import com.abacusdesk.instafeed.instafeed.Models.favmodel;
import com.abacusdesk.instafeed.instafeed.Models.likemodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.devs.readmoreoption.ReadMoreOption;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class brandadapter extends RecyclerView.Adapter<brandadapter.ViewHolder> {
    private ArrayList<Databrand> arrayList;
    String formattedDate;
    Context context;
    String upperString;
    String id;
    String vote;
    private int listSize = 0;
    String token;
    ReadMoreOption readMoreOption ;


    public brandadapter(Context context, ArrayList<Databrand> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        Log.e("arrayList", "arrayList..."+arrayList.size());
    }
    public void setData(ArrayList<Databrand> arrayList) {
        this.arrayList.addAll(arrayList);
        listSize = this.arrayList.size() /*+ 1*/;
        notifyDataSetChanged();
    }
    public void refreshData(ArrayList<Databrand> arrayList) {
        this.arrayList.clear();
        this.arrayList.addAll(arrayList);
        listSize = this.arrayList.size() /*+ 1*/;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public  ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.citizenrow, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        token= SaveSharedPreference.getToken(context);
        readMoreOption  = new ReadMoreOption.Builder(context).build();

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        String myFormat = "yyyy-MM-ddHH:mm:ss";
        Log.e("contryFF", "success..."+arrayList.size());
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy hh mm");
        if(arrayList.get(i).getIsLike().equals("1")){
            viewHolder.like.setImageResource(R.mipmap.heartcol);
        }else {
            viewHolder.like.setImageResource(R.mipmap.heart);
        }
        viewHolder.bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    id = arrayList.get(i).getId();
                    Log.e("","");
                    Bookmark(viewHolder);
                }else {

                }
            }
        });
        if (arrayList.get(i).getVideoThumb() != null && !arrayList.get(i).getVideoThumb().isEmpty()) {
            viewHolder.vedios.setVisibility(View.VISIBLE);
            Picasso.with(context).load(arrayList.get(i).getVideoThumb()).error(R.drawable.app_icon).into(viewHolder.feedimage);
        }else if (arrayList.get(i).getImage360x290() != null && !arrayList.get(i).getImage360x290().isEmpty()) {
            Log.e("Vedio","Image" +i);
            viewHolder.vedios.setVisibility(View.GONE);
            Picasso.with(context).load(arrayList.get(i).getImage360x290()).error(R.drawable.app_icon).into(viewHolder.feedimage);
        }
        viewHolder.proimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, sample.class);
                Bundle bundle = new Bundle();
                bundle.putString("username", arrayList.get(i).getUsername());
                bundle.putString("id", arrayList.get(i).getUserId());
                in.putExtras(bundle);
                context.startActivity(in);
            }
        });
        viewHolder.dots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        viewHolder.feedimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, Citizendetailpage.class);
                Bundle bundle = new Bundle();
                bundle.putString("postid", arrayList.get(i).getId());
                bundle.putString("type", "Brand");
                in.putExtras(bundle);
                context.startActivity(in);
            }
        });
        viewHolder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    if (arrayList.get(i).getIsLike().equals("0")) {
                        id = arrayList.get(i).getId();
                        arrayList.get(i).setIsLike("1");
                        vote = "u";
                        voteBrand(viewHolder);
                    } else {
                        id = arrayList.get(i).getId();
                        arrayList.get(i).setIsLike("0");
                        vote = "d";
                        votedown(viewHolder);
                    }
                }else {}
            }
        });
        viewHolder.likecount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    Intent in = new Intent(context, who_like.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("postid", arrayList.get(i).getId());
                    bundle.putString("type", "Brand");
                    in.putExtras(bundle);
                    context.startActivity(in);
                }else {

                }
            }
        });
        viewHolder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Log.e("I am", "HERE");
                    Intent in = new Intent(context, commentactivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("postid", arrayList.get(i).getId());
                    bundle.putString("type", "Brand");
                    in.putExtras(bundle);
                    context.startActivity(in);
            }
        });
        viewHolder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareTextUrl();
            }
        });


        if (arrayList.get(i).getIsAnonymous().equalsIgnoreCase("Y")) {

            viewHolder.name.setText("Anonymous");
            viewHolder
                    .proimage.setImageResource(R.drawable.user);
        }else {

            if (arrayList.get(i).getFirstName()==null) {
                upperString= arrayList.get(i).getUsername().substring(0,1).toUpperCase() + arrayList.get(i).getUsername().substring(1);
                viewHolder.name.setText(upperString);
            } else{
                upperString  = arrayList.get(i).getFirstName().substring(0,1).toUpperCase() + arrayList.get(i).getFirstName().substring(1);

                viewHolder.name.setText(upperString);

            }
            Picasso.with(context).load(arrayList.get(i).getAvatar()).error(R.drawable.user).
                    into(viewHolder.proimage);

        }
        try {
            formattedDate = targetFormat.format(sdformat.parse(arrayList.get(i).getDtAdded()));
            viewHolder.dateontym.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
       // Picasso.with(context).load(arrayList.get(i).getImage360x290()).into(viewHolder.feedimage);
        upperString  = arrayList.get(i).getTitle().substring(0,1).toUpperCase() + arrayList.get(i).getTitle().substring(1);
     //   viewHolder.txt_title.setText(upperString);
        readMoreOption = new ReadMoreOption.Builder(context)
                .textLength(2, ReadMoreOption.TYPE_LINE) // OR
                //.textLength(300, ReadMoreOption.TYPE_CHARACTER)
                .moreLabel("See More")
                .lessLabel(".See Less")
                .moreLabelColor(Color.parseColor("#FF4500"))
                .lessLabelColor(Color.parseColor("#FF4500"))
                .expandAnimation(true)
                .build();

        readMoreOption.addReadMoreTo(viewHolder.txt_title, upperString);
        viewHolder.likecount.setText(arrayList.get(i).getTotalLikes());
        viewHolder.commntcount.setText(arrayList.get(i).getTotalComments());
        viewHolder.sharecount.setText(arrayList.get(i).getTotalLikes());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView proimage,dots,bookmark,feedimage,like,share,comment,vedios;
        public TextView name,dateontym,txt_title,likecount,commntcount,sharecount;
        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            dateontym = (TextView) itemView.findViewById(R.id.dateontym);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            likecount = (TextView) itemView.findViewById(R.id.likecount);
            commntcount = (TextView) itemView.findViewById(R.id.commntcount);
            sharecount = (TextView) itemView.findViewById(R.id.sharecount);
            proimage=(ImageView)itemView.findViewById(R.id.proimage);
            dots=(ImageView)itemView.findViewById(R.id.dots);
            bookmark=(ImageView)itemView.findViewById(R.id.bookmark);
            like = (ImageView) itemView.findViewById(R.id.like);
            feedimage = (ImageView) itemView.findViewById(R.id.feedimage);
            share = (ImageView) itemView.findViewById(R.id.share);
            vedios=(ImageView)itemView.findViewById(R.id.vedios);

            comment = (ImageView) itemView.findViewById(R.id.comment);
        }
    }
    private void votedown(final ViewHolder viewHolder) {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, id);
        RequestBody Vote =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, vote);
        Call<likemodel> call = requestInterface.votebrandD(Token, Id, Vote);
        call.enqueue(new Callback<likemodel>() {
            @Override
            public void onResponse(Call<likemodel> call,
                                   Response<likemodel> response) {

                if (response.isSuccessful()) {
                    Log.e("Strring", "TEST" + response.body().getData().toString());
                    viewHolder.likecount.setText(response.body().getData().getTotalLikes());
                    viewHolder.like.setImageResource(R.mipmap.heart);
                } else {
                    //Toast.makeText(context,response.body().getData().getMessage(),Toast.LENGTH_SHORT).show();
                    Log.e("String", "TEST111");
                }
            }
            @Override
            public void onFailure(Call<likemodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    private void voteBrand(final ViewHolder viewHolder) {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, id);
        RequestBody Vote =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, vote);


        Call<likemodel> call = requestInterface.votebrand(Token, Id, Vote);
        call.enqueue(new Callback<likemodel>() {
            @Override
            public void onResponse(Call<likemodel> call,
                                   Response<likemodel> response) {

                if (response.isSuccessful()) {
                    Log.e("Strring", "TEST" + response.body().getData().toString());
                    viewHolder.likecount.setText(response.body().getData().getTotalLikes());
                    viewHolder.like.setImageResource(R.mipmap.heartcol);
                } else {
                    Toast.makeText(context,response.body().getData().getMessage(),Toast.LENGTH_SHORT).show();
                    Log.e("String", "TEST111");
                }
            }

            @Override
            public void onFailure(Call<likemodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    private void Bookmark(final ViewHolder viewHolder) {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, id);
        RequestBody IdM =   RequestBody.create(
                okhttp3.MultipartBody.FORM, "3");
        Call<favmodel> call = requestInterface.fav(Token, Id,IdM);
        call.enqueue(new Callback<favmodel>() {
            @Override
            public void onResponse(Call<favmodel> call,
                                   Response<favmodel> response) {
                if (response.isSuccessful()) {
                    viewHolder.bookmark.setImageResource(R.mipmap.bookmarked);
                } else {
                    Log.e("String", "TEST111");
                }
            }
            @Override
            public void onFailure(Call<favmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    private void shareTextUrl() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, "I AM GAURAV" + "\n\n" + "Download app for more updates" + "\n" + "https://bit.ly/2pDJWrB");
        //  Log.e("Link", Apis.talent_share + data.getSlug()/*+"\n\n"+"Download app for more updates"+"\n"+"https://bit.ly/2pDJWrB"*/);
        context.startActivity(Intent.createChooser(share, "Share link!"));
    }
}
