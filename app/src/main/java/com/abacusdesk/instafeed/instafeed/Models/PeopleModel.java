package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Instafeed2 on 9/7/2019.
 */

public class PeopleModel {

    @SerializedName("status")

    private Integer status;
    @SerializedName("message")

    private String message;
    @SerializedName("data")

    private ArrayList<DataPeople> data = new ArrayList<>();
    @SerializedName("type")

    private String type;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DataPeople> getData() {
        return data;
    }

    public void setData(ArrayList<DataPeople> data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
   
}
