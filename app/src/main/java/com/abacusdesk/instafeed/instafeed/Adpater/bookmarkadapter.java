package com.abacusdesk.instafeed.instafeed.Adpater;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.abacusdesk.instafeed.instafeed.Activity.Citizendetailpage;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.favmodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Responsemodel.DATAGETBOOK;

import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class bookmarkadapter extends RecyclerView.Adapter<bookmarkadapter.ViewHolder> {
    private ArrayList<DATAGETBOOK> arrayList;
    Context context;
    String formattedDate;
    String upperString,Token="",id="",TYPE,Mid="";

    public bookmarkadapter(Context context, ArrayList<DATAGETBOOK> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmarkrow, parent, false);
   ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull  ViewHolder viewHolder, final int i) {
        String myFormat = "yyyy-MM-ddHH:mm:ss";
        Log.e("contryFF", "success..."+arrayList.size());
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
        Token= SaveSharedPreference.getToken(context);
        String module = arrayList.get(i).getModule_id();
        Log.e("module", "module..."+module);
        if(module.contains("1")) {
            Log.e("module", " inside module...4");
            viewHolder.first_layout_boookmark.setVisibility(View.VISIBLE);
            viewHolder.second_layout_boookmark.setVisibility(View.GONE);

            if (arrayList.get(i).getVideoThumb() != null && !arrayList.get(i).getVideoThumb().isEmpty()) {
                viewHolder.vedios.setVisibility(View.VISIBLE);
                Picasso.with(context).load(arrayList.get(i).getVideoThumb()).into(viewHolder.feedimage);
            } else {
                viewHolder.vedios.setVisibility(View.GONE);
                Picasso.with(context).load(arrayList.get(i).getImageOriginal()).into(viewHolder.feedimage);
            }


            Picasso.with(context).load(arrayList.get(i).getAvatar()).into(viewHolder.proimage);
            if (arrayList.get(i).getIsAnonymous().equalsIgnoreCase("Y")) {
                viewHolder.name.setText("Anonymous");
                viewHolder.proimage.setImageResource(R.drawable.user);
            } else {

                if (arrayList.get(i).getFirstName() == null) {
                    upperString = arrayList.get(i).getUsername().substring(0, 1).toUpperCase() + arrayList.get(i).getUsername().substring(1);
                    viewHolder.name.setText(upperString);
                } else {
                    upperString = arrayList.get(i).getFirstName().substring(0, 1).toUpperCase() + arrayList.get(i).getFirstName().substring(1);
                    viewHolder.name.setText(upperString);
                }
                Glide.with(context).load(arrayList.get(i).getAvatar()).centerCrop().error(R.drawable.user).
                        into(viewHolder.proimage);
            }
            try {
                formattedDate = targetFormat.format(sdformat.parse(arrayList.get(i).getDtAdded()));
                viewHolder.dateontym.setText(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            viewHolder.dots.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Mid = arrayList.get(i).getModule_id();
                    id = arrayList.get(i).getId();
                    Bookmarks();
                    removeItem(i);

                }
            });
            viewHolder.bookrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arrayList.get(i).getModule_id();
                    if (arrayList.get(i).getModule_id().equals("1")) {
                        TYPE = "Citi";
                    } else if (arrayList.get(i).getModule_id().equals("3")) {
                        TYPE = "Brand";
                    } else if (arrayList.get(i).getModule_id().equals("2")) {
                        TYPE = "Star";
                    }
                    Intent in = new Intent(context, Citizendetailpage.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("postid", arrayList.get(i).getId());
                    bundle.putString("type", TYPE);
                    in.putExtras(bundle);
                    context.startActivity(in);
                }
            });
        }else
        {
            Log.e("module", " inside module...else");

            viewHolder.first_layout_boookmark.setVisibility(View.GONE);
            viewHolder.second_layout_boookmark.setVisibility(View.VISIBLE);
            if (arrayList.get(i).getVideoThumb() != null && !arrayList.get(i).getVideoThumb().isEmpty()) {
                Picasso.with(context).load(arrayList.get(i).getVideoThumb()).into(viewHolder.image_view_second_layout);
            } else {
                Picasso.with(context).load(arrayList.get(i).getImageOriginal()).into(viewHolder.image_view_second_layout);
            }
            if (arrayList.get(i).getFirstName() == null) {
                upperString = arrayList.get(i).getUsername().substring(0, 1).toUpperCase() + arrayList.get(i).getUsername().substring(1);
                viewHolder.headline_from.setText(upperString);
            } else {
                upperString = arrayList.get(i).getFirstName().substring(0, 1).toUpperCase() + arrayList.get(i).getFirstName().substring(1);
                viewHolder.headline_from.setText(upperString);
            }
            try {
                formattedDate = targetFormat.format(sdformat.parse(arrayList.get(i).getDtAdded()));
                viewHolder.date_second_layout.setText(formattedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            viewHolder.headline.setText(arrayList.get(i).getTitle());
        }
    }
    public void removeItem(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,position);
        notifyItemChanged(position);
    }
    private void Bookmarks() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Ttoken = RequestBody.create(okhttp3.MultipartBody.FORM, Token);
        RequestBody Pid = RequestBody.create(okhttp3.MultipartBody.FORM, id);
        RequestBody Module_id = RequestBody.create(okhttp3.MultipartBody.FORM, Mid);
        Call<favmodel> call = requestInterface.Unbookmarks(Ttoken,Pid,Module_id);
        call.enqueue(new Callback<favmodel>() {
            @Override
            public void onResponse(Call<favmodel> call, final Response<favmodel> response) {

                if (response.isSuccessful()) {

                } else {

                }
            }
            @Override
            public void onFailure(Call<favmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name,dateontym,txt_title,headline_from,headline,date_second_layout;
        ImageView proimage,feedimage,dots,vedios,image_view_second_layout,bookmark_second;
        LinearLayout bookrow;
        CardView second_layout_boookmark;
        CardView first_layout_boookmark;
        public ViewHolder(View itemView) {
            super(itemView);
            proimage=(ImageView)itemView.findViewById(R.id.proimage);
            feedimage=(ImageView)itemView.findViewById(R.id.feedimage);
            dots=(ImageView)itemView.findViewById(R.id.dots);
            vedios=(ImageView)itemView.findViewById(R.id.vedios);
            image_view_second_layout=(ImageView)itemView.findViewById(R.id.image_view_second_layout);
            bookmark_second=(ImageView)itemView.findViewById(R.id.bookmark_second);
            name=(TextView)itemView.findViewById(R.id.name);
            bookrow=(LinearLayout)itemView.findViewById(R.id.bookrow);
            second_layout_boookmark=(CardView) itemView.findViewById(R.id.second_layout_boookmark);
            first_layout_boookmark=(CardView)itemView.findViewById(R.id.first_layout_boookmark);
            dateontym=(TextView)itemView.findViewById(R.id.dateontym);
            txt_title=(TextView)itemView.findViewById(R.id.txt_title);
            headline_from=(TextView)itemView.findViewById(R.id.headline_from);
            headline=(TextView)itemView.findViewById(R.id.headline);
            date_second_layout=(TextView)itemView.findViewById(R.id.date_second_layout);

        }
    }
}
