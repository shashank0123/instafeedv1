package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.abacusdesk.instafeed.instafeed.Adpater.notificationadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.notificationlistingmodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notificationlistingclass extends Activity {
 RecyclerView recy;
String token;
    notificationadapter mnotificationadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);
        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        token= SaveSharedPreference.getToken(Notificationlistingclass.this);
        recy=(RecyclerView)findViewById(R.id.recy);
        recy.setLayoutManager(new LinearLayoutManager(Notificationlistingclass.this));
        recy.setNestedScrollingEnabled(false);
        recy.setHasFixedSize(false);
        Noty();
    }

    private void Noty() {

        RequestInterface requestInterface = ApiFactory.createService(Notificationlistingclass.this, RequestInterface.class);
        RequestBody Token=RequestBody.create(MultipartBody.FORM,"6e1553ba4f845d8474a00647b9d30cdcdcd1b5cb42e80ac9810c89cf8457d1899934ee4f3e534e5f5da6da691c50dee4aa5ff96a73079a8e49d7d87d35156fb4");
        RequestBody Lid=RequestBody.create(MultipartBody.FORM,"1");
        Call<notificationlistingmodel> call = requestInterface.Notyfy(Token,Lid);
        call.enqueue(new Callback<notificationlistingmodel>() {
            @Override
            public void onResponse(Call<notificationlistingmodel> call, final Response<notificationlistingmodel> response) {
                if(response.isSuccessful()){
                    mnotificationadapter = new notificationadapter(response.body().getData(), Notificationlistingclass.this);
                    recy.setAdapter(mnotificationadapter);
                }else {

                }
            }
            @Override
            public void onFailure(Call<notificationlistingmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}
