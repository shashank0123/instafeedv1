package com.abacusdesk.instafeed.instafeed.Adpater;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Models.DATALANG;
import com.abacusdesk.instafeed.instafeed.R;

import java.util.ArrayList;

public class Langadapter extends RecyclerView.Adapter<Langadapter.ViewHolder> {
    private ArrayList<DATALANG> arrayList;
    Context context;
    public Langadapter(Context context, ArrayList<DATALANG> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.langrow, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Langadapter.ViewHolder viewHolder, int i) {
    viewHolder.lang.setText(arrayList.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView lang;
        public ViewHolder(View itemView) {
            super(itemView);
            lang = (TextView) itemView.findViewById(R.id.lang);
        }
    }
}
