package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.Mycode.CommonMethod;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Responsemodel.mobileresponse;
import com.abacusdesk.instafeed.instafeed.Responsemodel.signupresponse;
import com.abacusdesk.instafeed.instafeed.Responsemodel.veryfyotp;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class loginwithmobile extends Activity {
    TextInputEditText name,emailid,mobile,password;
    String Mobile,Otp;
    Button signup;
    String otpval;
    PopupWindow popupWindow;
    LinearLayout poup;
    ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginwithmobile);
        mobile=(TextInputEditText)findViewById(R.id.mobile);
        password=(TextInputEditText)findViewById(R.id.password);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        signup=(Button)findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobile.getText().toString().trim();
                mobile.getText().toString().trim();
                SigIn();
            }
        });

    }
    private void SigIn() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(loginwithmobile.this, RequestInterface.class);
        RequestBody Email = RequestBody.create(okhttp3.MultipartBody.FORM, mobile.getText().toString().trim());
        RequestBody passW = RequestBody.create(okhttp3.MultipartBody.FORM, password.getText().toString().trim());
        RequestBody Token =RequestBody.create(okhttp3.MultipartBody.FORM, "Gaurav123456");
        Call<mobileresponse> call = requestInterface.mobilelogin(Email, passW);
        call.enqueue(new Callback<mobileresponse>() {
            @Override
            public void onResponse(Call<mobileresponse> call, final Response<mobileresponse> response) {


                    if(response.isSuccessful()){
                        otpval=response.body().getData().getOtp();
                        ViewGroup viewGroup = findViewById(android.R.id.content);
                        //then we will inflate the custom alert dialog xml that we created
                        View dialogView = LayoutInflater.from(loginwithmobile.this).inflate(R.layout.popupveryfy, viewGroup, false);
                        //Now we need an AlertDialog.Builder object
                        AlertDialog.Builder builder = new AlertDialog.Builder(loginwithmobile.this);
                        Button back = (Button) dialogView.findViewById(R.id.back);
                        Button next = (Button) dialogView.findViewById(R.id.next);
                        TextView resend = (TextView) dialogView.findViewById(R.id.re);
                        resend.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SigIn();
                            }
                        });

                        builder.setView(dialogView);
                        final AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                        alertDialog.setCancelable(false);
                        back.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.cancel();
                            }
                        });
                        next.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                otp();
                            }
                        });


                    //  SaveSharedPreference.setToken(loginwithmobile.this,response.body().getData().getToken());
                } else {
                        CommonMethod.showAlert("Mobile No already exists",  loginwithmobile.this);
                }
            }
            @Override
            public void onFailure(Call<mobileresponse> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    private void otp() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(loginwithmobile.this, RequestInterface.class);
        RequestBody Email = RequestBody.create(okhttp3.MultipartBody.FORM, mobile.getText().toString().trim());
        RequestBody passW = RequestBody.create(okhttp3.MultipartBody.FORM, otpval);
        RequestBody Token =RequestBody.create(okhttp3.MultipartBody.FORM, "Gaurav123456");
        Call<veryfyotp> call = requestInterface.mobileverify(Email, passW);
        call.enqueue(new Callback<veryfyotp>() {
            @Override
            public void onResponse(Call<veryfyotp> call, final Response<veryfyotp> response) {

                if (response.body().getMessage().equals("success")) {
                    SaveSharedPreference.setToken(loginwithmobile.this,response.body().getData().getToken());
                    Intent in = new Intent(loginwithmobile.this,MainActivity.class);
                    startActivity(in);
                    finish();
                    //  SaveSharedPreference.setToken(loginwithmobile.this,response.body().getData().getToken());
                } else {

                }
            }
            @Override
            public void onFailure(Call<veryfyotp> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}
