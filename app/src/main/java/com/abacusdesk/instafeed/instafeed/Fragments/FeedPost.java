package com.abacusdesk.instafeed.instafeed.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.abacusdesk.instafeed.instafeed.R;

public class FeedPost extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("I am in FEED","FEED");
        return inflater.inflate(R.layout.feedpost, container, false);
    }
}
