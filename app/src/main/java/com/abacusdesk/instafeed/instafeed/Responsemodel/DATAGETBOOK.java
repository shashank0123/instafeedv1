package com.abacusdesk.instafeed.instafeed.Responsemodel;

import com.google.gson.annotations.SerializedName;

public class DATAGETBOOK {


    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    @SerializedName("module_id")

    private String module_id;

    @SerializedName("id")
   
    private String id;
    @SerializedName("news_category_id")
   
    private String newsCategoryId;
    @SerializedName("title")
   
    private String title;
    @SerializedName("slug")
   
    private String slug;
    @SerializedName("short_description")
   
    private String shortDescription;
    @SerializedName("location_id")
   
    private String locationId;
    @SerializedName("total_likes")
   
    private String totalLikes;
    @SerializedName("total_dislikes")
   
    private String totalDislikes;
    @SerializedName("total_comments")
   
    private String totalComments;
    @SerializedName("total_views")
   
    private String totalViews;
    @SerializedName("total_flags")
   
    private String totalFlags;
    @SerializedName("dt_added")
   
    private String dtAdded;
    @SerializedName("dt_modified")
   
    private String dtModified;
    @SerializedName("status")
   
    private String status;
    @SerializedName("user_id")
   
    private String userId;
    @SerializedName("first_name")
   
    private String firstName;
    @SerializedName("last_name")
   
    private  String lastName;
    @SerializedName("nickname")
   
    private String nickname;
    @SerializedName("avatar")
   
    private  String avatar;
    @SerializedName("username")
   
    private String username;
    @SerializedName("image")
   
    private String image;
    @SerializedName("image_zoom")
   
    private String imageZoom;
    @SerializedName("image_original")
   
    private String imageOriginal;
    @SerializedName("image_100x100")
   
    private String image100x100;
    @SerializedName("image_256x170")
   
    private String image256x170;
    @SerializedName("image_264x200")
   
    private String image264x200;
    @SerializedName("image_360x290")
   
    private String image360x290;
    @SerializedName("video_thumb")
   
    private String videoThumb;
    @SerializedName("video_original")
   
    private String videoOriginal;
    @SerializedName("video_zoom")
   
    private String videoZoom;
    @SerializedName("video_100x100")
   
    private String video100x100;
    @SerializedName("video_256x170")
   
    private String video256x170;
    @SerializedName("video_264x200")
   
    private String video264x200;
    @SerializedName("video_360x290")
   
    private String video360x290;
    @SerializedName("source")
   
    private String source;
    @SerializedName("latitude")
   
    private String latitude;
    @SerializedName("longitude")
   
    private String longitude;
    @SerializedName("is_anonymous")
   
    private String isAnonymous;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNewsCategoryId() {
        return newsCategoryId;
    }

    public void setNewsCategoryId(String newsCategoryId) {
        this.newsCategoryId = newsCategoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String getTotalDislikes() {
        return totalDislikes;
    }

    public void setTotalDislikes(String totalDislikes) {
        this.totalDislikes = totalDislikes;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(String totalViews) {
        this.totalViews = totalViews;
    }

    public String getTotalFlags() {
        return totalFlags;
    }

    public void setTotalFlags(String totalFlags) {
        this.totalFlags = totalFlags;
    }

    public String getDtAdded() {
        return dtAdded;
    }

    public void setDtAdded(String dtAdded) {
        this.dtAdded = dtAdded;
    }

    public String getDtModified() {
        return dtModified;
    }

    public void setDtModified(String dtModified) {
        this.dtModified = dtModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public  String getLastName() {
        return lastName;
    }

    public void setLastName( String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public  String getAvatar() {
        return avatar;
    }

    public void setAvatar( String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageZoom() {
        return imageZoom;
    }

    public void setImageZoom(String imageZoom) {
        this.imageZoom = imageZoom;
    }

    public String getImageOriginal() {
        return imageOriginal;
    }

    public void setImageOriginal(String imageOriginal) {
        this.imageOriginal = imageOriginal;
    }

    public String getImage100x100() {
        return image100x100;
    }

    public void setImage100x100(String image100x100) {
        this.image100x100 = image100x100;
    }

    public String getImage256x170() {
        return image256x170;
    }

    public void setImage256x170(String image256x170) {
        this.image256x170 = image256x170;
    }

    public String getImage264x200() {
        return image264x200;
    }

    public void setImage264x200(String image264x200) {
        this.image264x200 = image264x200;
    }

    public String getImage360x290() {
        return image360x290;
    }

    public void setImage360x290(String image360x290) {
        this.image360x290 = image360x290;
    }

    public String getVideoThumb() {
        return videoThumb;
    }

    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    public String getVideoOriginal() {
        return videoOriginal;
    }

    public void setVideoOriginal(String videoOriginal) {
        this.videoOriginal = videoOriginal;
    }

    public String getVideoZoom() {
        return videoZoom;
    }

    public void setVideoZoom(String videoZoom) {
        this.videoZoom = videoZoom;
    }

    public String getVideo100x100() {
        return video100x100;
    }

    public void setVideo100x100(String video100x100) {
        this.video100x100 = video100x100;
    }

    public String getVideo256x170() {
        return video256x170;
    }

    public void setVideo256x170(String video256x170) {
        this.video256x170 = video256x170;
    }

    public String getVideo264x200() {
        return video264x200;
    }

    public void setVideo264x200(String video264x200) {
        this.video264x200 = video264x200;
    }

    public String getVideo360x290() {
        return video360x290;
    }

    public void setVideo360x290(String video360x290) {
        this.video360x290 = video360x290;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIsAnonymous() {
        return isAnonymous;
    }

    public void setIsAnonymous(String isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    @Override
    public String toString() {
        return "DATAGETBOOK{" +
                "module_id='" + module_id + '\'' +
                ", id='" + id + '\'' +
                ", newsCategoryId='" + newsCategoryId + '\'' +
                ", title='" + title + '\'' +
                ", slug='" + slug + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", locationId='" + locationId + '\'' +
                ", totalLikes='" + totalLikes + '\'' +
                ", totalDislikes='" + totalDislikes + '\'' +
                ", totalComments='" + totalComments + '\'' +
                ", totalViews='" + totalViews + '\'' +
                ", totalFlags='" + totalFlags + '\'' +
                ", dtAdded='" + dtAdded + '\'' +
                ", dtModified='" + dtModified + '\'' +
                ", status='" + status + '\'' +
                ", userId='" + userId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", nickname='" + nickname + '\'' +
                ", avatar='" + avatar + '\'' +
                ", username='" + username + '\'' +
                ", image='" + image + '\'' +
                ", imageZoom='" + imageZoom + '\'' +
                ", imageOriginal='" + imageOriginal + '\'' +
                ", image100x100='" + image100x100 + '\'' +
                ", image256x170='" + image256x170 + '\'' +
                ", image264x200='" + image264x200 + '\'' +
                ", image360x290='" + image360x290 + '\'' +
                ", videoThumb='" + videoThumb + '\'' +
                ", videoOriginal='" + videoOriginal + '\'' +
                ", videoZoom='" + videoZoom + '\'' +
                ", video100x100='" + video100x100 + '\'' +
                ", video256x170='" + video256x170 + '\'' +
                ", video264x200='" + video264x200 + '\'' +
                ", video360x290='" + video360x290 + '\'' +
                ", source='" + source + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", isAnonymous='" + isAnonymous + '\'' +
                '}';
    }
}
