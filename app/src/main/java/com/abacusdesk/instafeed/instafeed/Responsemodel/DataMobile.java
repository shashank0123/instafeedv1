package com.abacusdesk.instafeed.instafeed.Responsemodel;

import com.google.gson.annotations.SerializedName;

public class DataMobile {
    @SerializedName("mobile")
      
    private String mobile;
    @SerializedName("otp")
      
    private String otp;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
