package com.abacusdesk.instafeed.instafeed.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.abacusdesk.instafeed.instafeed.Adpater.likeadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.userlikemodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class who_like extends FragmentActivity {
    likeadapter likead;
    RecyclerView vvvv;
    ImageView imageView5;
    ArrayList av = new ArrayList();
    RelativeLayout relativeLayout;
    String postid,Type;
    String token;
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wholikeui);
        vvvv = (RecyclerView) findViewById(R.id.recyclerView);
        token= SaveSharedPreference.getToken(who_like.this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            postid = bundle.getString("postid");
            Type= bundle.getString("type");
        }
        relativeLayout = findViewById(R.id.vvvv);
        findViewById(R.id.imageView5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    finish();

            }
        });
        vvvv.setLayoutManager(new LinearLayoutManager(who_like.this));

        vvvv.setNestedScrollingEnabled(false);
        vvvv.setHasFixedSize(false);
        av = new ArrayList();
        Getuserlike();
    }
    Call<userlikemodel> call;
    private void Getuserlike() {
        av.clear();

        //   RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(who_like.this, RequestInterface.class);

        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, postid);
        if(Type.equalsIgnoreCase("Citi")){
            call = requestInterface.userLikeNews(Token, Id);
        }else if(Type.equalsIgnoreCase("Brand")){
            call = requestInterface.userLikeNewsB(Token, Id);
        }else if(Type.equalsIgnoreCase("Star")){
            call = requestInterface.userLikeNewsS(Token, Id);
        }

        call.enqueue(new Callback<userlikemodel>() {
            @Override
            public void onResponse(Call<userlikemodel> call,
                                   Response<userlikemodel> response) {

                if (response.isSuccessful()) {

                    av = response.body().getData();
                    likead = new likeadapter(av, who_like.this);
                    vvvv.setAdapter(likead);
                } else {
                    Log.e("String", "TEST111");
                }
            }

            @Override
            public void onFailure(Call<userlikemodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

            finish();
        }
    }


