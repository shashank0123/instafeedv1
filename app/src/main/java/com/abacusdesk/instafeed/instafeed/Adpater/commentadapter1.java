package com.abacusdesk.instafeed.instafeed.Adpater;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.commentactivity;
import com.abacusdesk.instafeed.instafeed.Models.Comentnews;
import com.abacusdesk.instafeed.instafeed.Models.DATAComment;
import com.abacusdesk.instafeed.instafeed.R;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class commentadapter1 extends RecyclerView.Adapter<commentadapter1.MyViewHolder> {

    Context context;
    ArrayList<DATAComment> data;
    public static String comment="",comment_id;
    Dialog dialog;
    String type;

    public commentadapter1(ArrayList<DATAComment> data,  Context context) {
        this.context=context;
        this.data=data;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtname,txtcomment;
        public ImageView imgprofile, imgedit;
        //public LinearLayout lnrow;

        public MyViewHolder(View view) {
            super(view);
            this.imgprofile=(ImageView)itemView.findViewById(R.id.img_profilepic);
            this.imgedit=(ImageView)itemView.findViewById(R.id.img_edit);
            this.txtname=(TextView)itemView.findViewById(R.id.txt_name);
            this.txtcomment=(TextView)itemView.findViewById(R.id.txt_comment);
        }
    }




    @Override
    public  MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rowlist, parent, false);
        return new commentadapter1.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder, final int position) {

        String myFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

        holder.txtcomment.setText(data.get(position).getComment());
        holder.txtname.setText(data.get(position).getFirstName());
        if (data.get(position).getAvatar()!=null && !data.get(position).getAvatar().isEmpty()){
            Glide.with(context).load(data.get(position).getAvatar()).error(R.drawable.user).into(holder.imgprofile);
        }else {
            holder.imgprofile.setImageResource(R.drawable.user);
        }


       holder.imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get prompts.xml view
                String ab=data.get(position).getComment();
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.ed_comment);
                userInput.setText(ab);
                // set dialog message
               /* alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // get user input and set it to result
                                        // edit text
                                        //result.setText(userInput.getText());
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });*/

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
       });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }
}
