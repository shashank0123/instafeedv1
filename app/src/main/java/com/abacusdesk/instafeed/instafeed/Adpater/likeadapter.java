package com.abacusdesk.instafeed.instafeed.Adpater;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.forgotpass;
import com.abacusdesk.instafeed.instafeed.Activity.who_like;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.DATAUSER;
import com.abacusdesk.instafeed.instafeed.Models.followunfollowmodel;
import com.abacusdesk.instafeed.instafeed.Models.userlikemodel;
import com.abacusdesk.instafeed.instafeed.Mycode.CommonMethod;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Responsemodel.forgotmodel;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class likeadapter extends RecyclerView.Adapter<likeadapter.MyViewHolder> {

    Context context;
    ArrayList<DATAUSER> arrayList;
    public static String comment="",comment_id;
    Dialog dialog;
    String foloow;
    public static Boolean isfollow=false;
    public static String  Apitemp_follow="";
    public static String  username="",userid="";
    String followerid="";
    String token;
    String usernames;
    public likeadapter(ArrayList<DATAUSER> arrayList, Context context) {
        this.context=context;
        this.arrayList=arrayList;
        Log.e("test12",""+arrayList.size());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.whonewlike, parent, false);
        token= SaveSharedPreference.getToken(context);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        foloow = arrayList.get(position).getFollowStatus().toString();
        holder.txtname.setText(arrayList.get(position).getUsername());

        if(arrayList.get(position).getFollowStatus().equals("1")){
            holder.btn_follow.setText("UnFollow");
        }else {
            holder.btn_follow.setText("Follow");
        }
        holder.vvvvv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (context instanceof who_like) {


                }
            }
        });
        holder.btn_follow.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        usernames=arrayList.get(position).getUsername();
        if(arrayList.get(position).getFollowStatus().equals("1")){
            Unfollow(holder);
        }else {
            follow(holder);
        }

    }
});

    }
    private void follow(final MyViewHolder holder) {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token=RequestBody.create(MultipartBody.FORM,token);
        RequestBody userid = RequestBody.create(okhttp3.MultipartBody.FORM, usernames);
        Call<followunfollowmodel> call = requestInterface.follow(Token,userid);
        call.enqueue(new Callback<followunfollowmodel>() {
            @Override
            public void onResponse(Call<followunfollowmodel> call, final Response<followunfollowmodel> response) {
if(response.isSuccessful()){
    holder.btn_follow.setText("UnFollow");
}else {

}
            }
            @Override
            public void onFailure(Call<followunfollowmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    private void Unfollow(final MyViewHolder holder) {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token=RequestBody.create(MultipartBody.FORM,token);
        RequestBody userid = RequestBody.create(okhttp3.MultipartBody.FORM, usernames);
        Call<followunfollowmodel> call = requestInterface.unfollow(Token,userid);
        call.enqueue(new Callback<followunfollowmodel>() {
            @Override
            public void onResponse(Call<followunfollowmodel> call, final Response<followunfollowmodel> response) {
                if(response.isSuccessful()){
                    holder.btn_follow.setText("Follow");
                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                }else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }
            @Override
            public void onFailure(Call<followunfollowmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtname,txtcomment;
        public ImageView imgprofile, imaglike;
        TextView btn_follow;
        public LinearLayout vvvvv;
        public MyViewHolder(View view) {
            super(view);
            this.imgprofile=(ImageView)itemView.findViewById(R.id.img_profilepic);
            this.btn_follow=(TextView) itemView.findViewById(R.id.btn_follow);
            this.vvvvv=(LinearLayout)itemView.findViewById(R.id.vvvvv);
            this.txtname=(TextView)itemView.findViewById(R.id.txt_name);
           // this.imaglike=(ImageView)itemView.findViewById(R.id.imaglike);


        }
    }

}