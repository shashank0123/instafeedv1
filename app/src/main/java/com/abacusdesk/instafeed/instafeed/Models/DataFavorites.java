package com.abacusdesk.instafeed.instafeed.Models;

/**
 * Created by Instafeed2 on 9/4/2019.
 */
public class DataFavorites
{
    private String user_type;

    private String last_name;

    private String id;

    private String avatar;

    private String first_name;

    private String username;

    public String getUser_type ()
    {
        return user_type;
    }

    public void setUser_type (String user_type)
    {
        this.user_type = user_type;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAvatar ()
    {
        return avatar;
    }

    public void setAvatar (String avatar)
    {
        this.avatar = avatar;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [user_type = "+user_type+", last_name = "+last_name+", id = "+id+", avatar = "+avatar+", first_name = "+first_name+", username = "+username+"]";
    }
}