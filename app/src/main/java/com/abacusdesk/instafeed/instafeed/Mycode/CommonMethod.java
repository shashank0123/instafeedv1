package com.abacusdesk.instafeed.instafeed.Mycode;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.login;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMethod {

	private static Editor editor;

	/** Called for setting the value based on key in Prefs */

	public static void setPrefsData(Context context, String prefsKey,
                                    String prefValue) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		editor = sharedPreferences.edit();
		editor.putString(prefsKey, prefValue);
		editor.commit();
	}

	public static boolean getPrefsLoginData(Context context, String prefsKey,
                                            boolean defaultValue) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		boolean prefsValue = sharedPreferences.getBoolean(prefsKey,
				defaultValue);
		return prefsValue;
	}

	public static void savePreferences(Context activity, String key,
                                       String value) {

		Editor editor = PreferenceManager.getDefaultSharedPreferences(activity)
				.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String getPreferences(Context context1, String key) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context1);
		return sharedPreferences.getString(key, "");

	}

	/** Called for getting the value based on key from Prefs */
	public static String getPrefsData(Context context, String prefsKey,
                                      String defaultValue) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		String prefsValue = sharedPreferences.getString(prefsKey, defaultValue);
		return prefsValue;
	}

	/** Called for Showing Alert in Application */
	public static void showAlert(String message, final Activity context) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false)

		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			//	Intent intent=new Intent(context, upload.class);
			//	context.startActivity(intent);
				dialog.dismiss();
			}
		});
		try {

			AlertDialog alert = builder.show();
			TextView messageText = (TextView) alert
					.findViewById(android.R.id.message);
			messageText.setGravity(Gravity.CENTER);
			Button buttonbackground = alert
					.getButton(DialogInterface.BUTTON_POSITIVE);
			buttonbackground.setBackgroundColor(Color.parseColor("#2D6EB0"));
			buttonbackground.setTextColor(Color.WHITE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void showAlert2(String message, final Activity context) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false)

				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

Intent in = new Intent(context, login.class);
context.startActivity(in);
						dialog.dismiss();
					}
				});
		try {

			AlertDialog alert = builder.show();
			TextView messageText = (TextView) alert
					.findViewById(android.R.id.message);
			messageText.setGravity(Gravity.CENTER);
			Button buttonbackground = alert
					.getButton(DialogInterface.BUTTON_POSITIVE);
			buttonbackground.setBackgroundColor(Color.parseColor("#2D6EB0"));
			buttonbackground.setTextColor(Color.WHITE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public static void showAlert1(String message, final Activity context) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false)

				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.dismiss();
					}
				});
		try {

			AlertDialog alert = builder.show();
			TextView messageText = (TextView) alert
					.findViewById(android.R.id.message);
			messageText.setGravity(Gravity.CENTER);
			Button buttonbackground = alert
					.getButton(DialogInterface.BUTTON_POSITIVE);
			buttonbackground.setBackgroundColor(Color.parseColor("#2D6EB0"));
			buttonbackground.setTextColor(Color.WHITE);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

    public static void showAlert(String message, String title, final Activity context, final boolean shouldExit) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(title).setMessage(message).setCancelable(false)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (shouldExit)
                            context.finish();
                        //dialog.dismiss();
                    }
                });
        try {

            AlertDialog alert = builder.show();
            TextView messageText = (TextView) alert
                    .findViewById(android.R.id.message);
            messageText.setGravity(Gravity.CENTER);
            Button buttonbackground = alert
                    .getButton(DialogInterface.BUTTON_POSITIVE);
            buttonbackground.setBackgroundColor(Color.RED);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

	/** Called for checking Email Validation */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	/** Called for checking Internet connection */
	public static boolean isOnline(Context context) {

		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo;
		try {
			netInfo = cm.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnectedOrConnecting()) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}



	public static String getUserName(Activity conteActivity) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(conteActivity);
		String userID = sharedPreferences.getString("NAME", "");

		return userID;
	}

	public static void setGalleryImageId(Context context, String value) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("GalleryImageId", value);
		editor.commit();

	}

	public static String getGalleryImageID(Activity conteActivity) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(conteActivity);
		String GalleryImageId = sharedPreferences.getString("GalleryImageId",
				"");

		return GalleryImageId;
	}

	public static String getProductID(Activity conteActivity) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(conteActivity);
		String ProductID = sharedPreferences.getString("USER_ID", "");

		return ProductID;
	}

	public static void setProductid(Context context, String value) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("PRODUCT_ID", value);
		editor.commit();

	}

	public static String getProductCategoryID(Activity conteActivity) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(conteActivity);
		String CatogryID = sharedPreferences.getString("CATEGORY_ID", "");

		return CatogryID;
	}

	public static void setProductCategoryid(Context context, int strdata) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putInt("CATEGORY_ID", strdata);
		editor.commit();

	}

	public static void setCustomerid(Context context, String value) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("CustomerId", value);
		editor.commit();

	}

	public static String getCustomerId(Activity conteActivity) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(conteActivity);
		String CustomerId = sharedPreferences.getString("CustomerId", "");

		return CustomerId;

	}

	public static void saveLoginPreferences(Context context, String key,
                                            String value) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();

	}

	public static void setUserid(Context context, String value) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("USER_ID", value);
		editor.commit();

	}

	public static String getUserID(Activity conteActivity) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(conteActivity);
		String userID = sharedPreferences.getString("USER_ID", "");

		return userID;
	}

	public static void setAddressId(Context context, String value) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = sharedPreferences.edit();
		editor.putString("AddressID", value);
		editor.commit();

	}

	public static String getAddressID(Activity conteActivity) {

		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(conteActivity);
		String AddressIDD = sharedPreferences.getString("AddressID", "");

		return AddressIDD;
	}

	public static String convertDate(String timestamp) {

		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"dd MMM yyyy hh:mm aa");

			Date resultdate = new Date(Long.valueOf(timestamp));
			System.out.println(dateFormat.format(resultdate));

			// Calendar calendar = Calendar.getInstance();
			// TimeZone tz = TimeZone.getDefault();
			// calendar.setTimeInMillis(Long.valueOf(timestamp) * 1000);
			// calendar.add(Calendar.MILLISECOND,
			// tz.getOffset(calendar.getTimeInMillis()));
			// SimpleDateFormat sdf = new
			// SimpleDateFormat("dd-MMMM-yyyy-HH-mm-ss");
			// SimpleDateFormat dateFormat = new
			// SimpleDateFormat("dd MMM yyyy HH:mm aa");

			// Date currenTimeZone = (Date) calendar.getTime();

			return dateFormat.format(resultdate);
		} catch (Exception e) {
		}

		return "";
	}

	public static boolean copyStream(InputStream input, OutputStream output)
			throws IOException {
		boolean isCopied = false;
		byte[] buffer = new byte[1024];
		int bytesRead;
		try {
			while ((bytesRead = input.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
			isCopied = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isCopied = false;
		}
		return isCopied;
	}

	public static void closeKeyboard(final Context context, final View view) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	@SuppressWarnings("deprecation")
	public static void showAleart(final Activity mActivity, String message) {
		AlertDialog alertDialog = new AlertDialog.Builder(mActivity).create();

		// Setting Dialog Title
		/*
		 * alertDialog.setTitle(mActivity.getResources().getString(
		 * R.string.sliderheading));
		 */
		// alertDialog.setIcon(mActivity.getResources().getDrawable(R.drawable.ic_launcher));
		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting Icon to Dialog

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// Write your code here to execute after dialog closed

			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	public static float convertDpToPixel(float dp, Context context) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}



}
