package com.abacusdesk.instafeed.instafeed.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.abacusdesk.instafeed.instafeed.Adpater.TabAdapterC;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Fragments.Citizen;
import com.abacusdesk.instafeed.instafeed.Fragments.Mylike;
import com.abacusdesk.instafeed.instafeed.Fragments.Mypost;
import com.abacusdesk.instafeed.instafeed.Fragments.Superstar;
import com.abacusdesk.instafeed.instafeed.Models.citizenprofiledata;
import com.abacusdesk.instafeed.instafeed.Models.followunfollowmodel;
import com.abacusdesk.instafeed.instafeed.Models.isfollowmodel;
import com.abacusdesk.instafeed.instafeed.Models.profilemodel;
import com.abacusdesk.instafeed.instafeed.Mycode.helper;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.AndroidMultiPartEntity;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.bumptech.glide.Glide;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class Citizenprofile extends FragmentActivity {

ImageView img_back,img_profile;
String token,upperString;
    private Uri mCropImageUri;
    long totalSize = 0;
TextView image,imag1e,img,name,btn_follow;
    Bitmap bitmap;
    public static String update_pic ="http://13.234.116.90/api/profile/update_pic";
    int statusCode;
    String username="",user="",Type="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ViewPager viewPager;
        TabLayout tabLayout;
        TabAdapterC adapter;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.citizenprofile);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            username = bundle.getString("username");
            user = bundle.getString("userid");
        }
        img_profile=(ImageView)findViewById(R.id.img_profile);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectImageClick(v);
            }
        });
        name=(TextView)findViewById(R.id.name);
        btn_follow=(TextView)findViewById(R.id.btn_follow);
        image=(TextView)findViewById(R.id.image);
        imag1e=(TextView)findViewById(R.id.imag1e);
        img=(TextView)findViewById(R.id.img);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        adapter = new TabAdapterC(getSupportFragmentManager());
        adapter.addFragment(new Mypost(), "Post");
        adapter.addFragment(new Mylike(), "Liked");
        token= SaveSharedPreference.getToken(Citizenprofile.this);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        btn_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn_follow.getText().toString().trim().equalsIgnoreCase("Follow")){
                    btn_follow.setText("UnFollow");
                    Unfollow();
                }else {
                    btn_follow.setText("Follow");
                    follow();
                }
            }
        });
        helper.userid=user;
        USERDEtail();
    }
    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }
    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {


                //   fileUri = data.getData();
                try {
                    if (result.getUri()!=null){
                        bitmap = MediaStore.Images.Media.getBitmap(Citizenprofile.this.getContentResolver(), result.getUri());
                        //bitmap=changeorientation(fileUri.getPath());

                        Log.e("bitmap",""+bitmap);
                        img_profile.setImageBitmap(bitmap);
                        //imgdemo.setVisibility(View.GONE);
                      // new UploadFileToServer().execute();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }
    private void USERDEtail() {
        RequestInterface requestInterface = ApiFactory.createService(Citizenprofile.this, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, username);
        Call<citizenprofiledata> call = requestInterface.userprofiles(Token);
        call.enqueue(new Callback<citizenprofiledata>() {
            @Override
            public void onResponse(Call<citizenprofiledata> call,
                                   Response<citizenprofiledata> response) {

                if (response.isSuccessful()) {
                    if (!token.equals("")
                            && token != null) {
                        ISfollow();
                    }
                    ISfollow();
                    image.setText(response.body().getData().getTotals().getTotalNews());
                    imag1e.setText(response.body().getData().getTotals().getTotalFollowers());
                    img.setText(response.body().getData().getTotals().getTotalFollowing());
                    if (response.body().getData().getFirstName() == null) {
                        upperString = response.body().getData().getUsername().substring(0, 1).toUpperCase() + response.body().getData().getUsername().substring(1);
                        name.setText(upperString);
                    } else {
                        upperString = response.body().getData().getFirstName().substring(0, 1).toUpperCase() +response.body().getData().getFirstName().substring(1);
                        name.setText(upperString);

                    }
                }
            }


            @Override
            public void onFailure(Call<citizenprofiledata> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
  private class UploadFileToServer extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }
        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
            byte[] data = bos.toByteArray();
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(update_pic);
            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                ByteArrayBody bab = new ByteArrayBody(data, "bt.jpg");
                entity.addPart("image", bab);
                entity.addPart("token", new StringBody(SaveSharedPreference.getPrefToken(Citizenprofile.this)));
               // entity.addPart("user", new StringBody(SaveSharedPreference.getUserID(Citizenprofile.this)));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                statusCode = response.getStatusLine().getStatusCode();
                responseString = EntityUtils.toString(r_entity);
                JSONObject jsonObject = new JSONObject(responseString);
                Log.e("respon", "" + responseString);

                if (statusCode == 200) {
                    // Server response
                    try {
                        SaveSharedPreference.setUserIMAGE(Citizenprofile.this,jsonObject.getJSONObject("data")
                                .getJSONObject("avatar").getString("200x200") );
                    }catch (Exception e){

                    }
                } else if (statusCode == 201) {
                    // Server response
                    try {
                        SaveSharedPreference.setUserIMAGE(Citizenprofile.this,jsonObject.getJSONObject("data")
                                .getJSONObject("avatar").getString("200x200") );
                    }catch (Exception e){

                    }

                } else {
                    responseString = "Error occurred! Http Status Code: " +
                            EntityUtils.toString(r_entity) + statusCode;
                }
            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (Exception e) {
                responseString = e.toString();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                if (statusCode == 200) {
                    Toast.makeText(Citizenprofile.this,"Updated Successfully",Toast.LENGTH_SHORT).show();
                    //if (bitmap==null)
                    if (bitmap!=null){
                        if (!SaveSharedPreference.getUserIMAGE(Citizenprofile.this).isEmpty()){
                            Glide.with(Citizenprofile.this).load(SaveSharedPreference.getUserIMAGE(Citizenprofile.this)).error(R.drawable.user).into(img_profile);
                        }
                    }
                  //  NavDrawerActivity.updateProfile(UpdateProfile_Fragment.this);
                } else if (statusCode == 201) {
                    Toast.makeText(Citizenprofile.this,"Updated Successfully",Toast.LENGTH_SHORT).show();
                  //  Dialogs.showCenterToast(UpdateProfile_Fragment.this,"Updated Successfully");
                } else {

                }
            }catch (Exception e){

            }
        }

    }
    private void follow() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Citizenprofile.this, RequestInterface.class);
        RequestBody Token = RequestBody.create(MultipartBody.FORM, token);
        RequestBody userid = RequestBody.create(okhttp3.MultipartBody.FORM, username);
        Call<followunfollowmodel> call = requestInterface.follow(Token, userid);
        call.enqueue(new Callback<followunfollowmodel>() {
            @Override
            public void onResponse(Call<followunfollowmodel> call, final Response<followunfollowmodel> response) {
                if (response.isSuccessful()) {
                    btn_follow.setText("Follow");
                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                } else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }

            @Override
            public void onFailure(Call<followunfollowmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    private void Unfollow() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Citizenprofile.this, RequestInterface.class);
        RequestBody Token=RequestBody.create(MultipartBody.FORM,token);
        RequestBody userid = RequestBody.create(okhttp3.MultipartBody.FORM, username);
        Call<followunfollowmodel> call = requestInterface.unfollow(Token,userid);
        call.enqueue(new Callback<followunfollowmodel>() {
            @Override
            public void onResponse(Call<followunfollowmodel> call, final Response<followunfollowmodel> response) {
                if(response.isSuccessful()){
                    btn_follow.setText("UnFollow");
                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                }else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }
            @Override
            public void onFailure(Call<followunfollowmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    private void ISfollow() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(Citizenprofile.this, RequestInterface.class);
        RequestBody Token=RequestBody.create(MultipartBody.FORM,token);
        RequestBody userid = RequestBody.create(okhttp3.MultipartBody.FORM, username);
        Call<isfollowmodel> call = requestInterface.isfollow(Token,userid);
        call.enqueue(new Callback<isfollowmodel>() {
            @Override
            public void onResponse(Call<isfollowmodel> call, final Response<isfollowmodel> response) {
                if(response.isSuccessful()){
                    if(response.body().getData().getIsFollowing().booleanValue()==false){
                        btn_follow.setText("Follow");
                    }else {
                        btn_follow.setText("UnFollow");
                    }


                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                }else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }
            @Override
            public void onFailure(Call<isfollowmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}
