package com.abacusdesk.instafeed.instafeed.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.Seachactivity;
import com.abacusdesk.instafeed.instafeed.Adpater.Catadappter;
import com.abacusdesk.instafeed.instafeed.Adpater.CatlistingAdapter;
import com.abacusdesk.instafeed.instafeed.Adpater.Langadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.Models.category;
import com.abacusdesk.instafeed.instafeed.Models.languages;
import com.abacusdesk.instafeed.instafeed.R;

public class Category extends Fragment {

    RecyclerView cate;
    CatlistingAdapter mCatlistingAdapter;
    ViewGroup header;
    ImageView menu;
    LinearLayout Search;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.cat, container, false);
        cate=(RecyclerView)view.findViewById(R.id.cate);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        cate.setLayoutManager(manager);
        cate();
        header=(ViewGroup)view.findViewById(R.id.header);
        menu=(ImageView) header.findViewById(R.id.menu);

        Search=(LinearLayout) header.findViewById(R.id.Search);
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Seachactivity.class);
                startActivity(in);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.Open();
            }
        });
        return view;
    }
    private void cate() {
        RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        ErrorCallback.MyCall<category> myCall;
        myCall = requestInterface.categorylist();
        Log.e("response ", myCall.toString());
        myCall.enqueue(new ErrorCallback.MyCallback<category>() {
            @Override
            public void success(final retrofit2.Response<category> response) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body().getMessage().equals("success")) {
                            mCatlistingAdapter= new CatlistingAdapter(getActivity(), response.body().getData());
                            cate.setAdapter(mCatlistingAdapter);
                        } else {


                        }
                    }
                });
                // Showing Alert Message


            }

            @Override
            public void error(String errorMessage) {

            }
        }, getActivity(), Boolean.TRUE);
    }
}
