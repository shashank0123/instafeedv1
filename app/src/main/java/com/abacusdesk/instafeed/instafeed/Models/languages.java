package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class languages {
    @SerializedName("status")
     
    private Integer status;
    @SerializedName("message")
     
    private String message;
    @SerializedName("data")
     
    private ArrayList<DATALANG> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DATALANG> getData() {
        return data;
    }

    public void setData(ArrayList<DATALANG> data) {
        this.data = data;
    }

}
