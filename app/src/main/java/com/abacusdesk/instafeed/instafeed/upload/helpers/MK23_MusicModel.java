package com.abacusdesk.instafeed.instafeed.upload.helpers;

import android.net.Uri;

public class MK23_MusicModel {
    String musicName;
    Uri musicUri;

    public MK23_MusicModel(String musicName, Uri musicUri) {
        this.musicName = musicName;
        this.musicUri = musicUri;
    }

    public String getMusicName() {
        return musicName;
    }

    public Uri getMusicUri() {
        return musicUri;
    }
}
