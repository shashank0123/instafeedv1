package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class StarN {
    @SerializedName("id")
     
    private String id;
    @SerializedName("star_category_id")
     
    private String starCategoryId;
    @SerializedName("title")
     
    private String title;
    @SerializedName("status")
     
    private String status;
    @SerializedName("slug")
     
    private String slug;
    @SerializedName("short_description")
     
    private String shortDescription;
    @SerializedName("dt_added")
     
    private String dtAdded;
    @SerializedName("total_comments")
     
    private String totalComments;
    @SerializedName("total_views")
     
    private String totalViews;
    @SerializedName("image")
     
    private String image;
    @SerializedName("username")
     
    private String username;
    @SerializedName("avatar")
     
    private String avatar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStarCategoryId() {
        return starCategoryId;
    }

    public void setStarCategoryId(String starCategoryId) {
        this.starCategoryId = starCategoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDtAdded() {
        return dtAdded;
    }

    public void setDtAdded(String dtAdded) {
        this.dtAdded = dtAdded;
    }

    public String getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(String totalComments) {
        this.totalComments = totalComments;
    }

    public String getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(String totalViews) {
        this.totalViews = totalViews;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
