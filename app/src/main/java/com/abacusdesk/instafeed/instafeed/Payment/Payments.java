package com.abacusdesk.instafeed.instafeed.Payment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.R;

/**
 * Created by Instafeed2 on 7/16/2019.
 */

public class Payments extends AppCompatActivity{
    ImageView img_back;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payments);
        img_back=(ImageView)findViewById(R.id.back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
