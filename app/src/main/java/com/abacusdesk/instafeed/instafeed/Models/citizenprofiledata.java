package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class citizenprofiledata {
    @SerializedName("status")
    
    private Integer status;
    @SerializedName("message")
    
    private String message;
    @SerializedName("data")
    
    private DataCitizen data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataCitizen getData() {
        return data;
    }

    public void setData(DataCitizen data) {
        this.data = data;
    }
}
