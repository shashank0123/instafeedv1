package com.abacusdesk.instafeed.instafeed.Adpater;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.abacusdesk.instafeed.instafeed.Models.MasterModel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import java.util.ArrayList;


public class Feedadapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<MasterModel> arrayList;
    String formattedDate;
    Context context;
    String upperString;
    String id;
    String vote;
    private int listSize = 0;
    String token;
    public Feedadapter(Context context, ArrayList<MasterModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        Log.e("Feedadapter", "Feedadapter" );

    }


    public void setData(ArrayList<MasterModel> arrayList) {
        this.arrayList.addAll(arrayList);
        listSize = this.arrayList.size() /*+ 1*/;
        notifyDataSetChanged();
    }

    public void refreshData(ArrayList<MasterModel> arrayList) {
        this.arrayList.clear();
        this.arrayList.addAll(arrayList);
        listSize = this.arrayList.size() /*+ 1*/;
        notifyDataSetChanged();
    }
    private final int VERTICAL = 1;
    private final int HORIZONTAL = 2;
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view ;/*= LayoutInflater.from(parent.getContext()).inflate(R.layout.citizenrow, parent, false);*/
        RecyclerView.ViewHolder holder;
        token= SaveSharedPreference.getToken(context);
        Log.e("viewType", "" + "viewType"+viewType);
        switch (viewType) {

            case VERTICAL:
                view = inflater.inflate(R.layout.vertical, parent, false);
                holder = new VerticalViewHolder(view);
                break;
            case HORIZONTAL:
                Log.e("HORIZONTAL", "" + "HORIZONTAL");
                view = inflater.inflate(R.layout.horizontal, parent, false);
                holder = new HorizontalViewHolder(view);
                break;
            default:
                Log.e("default", "" + "default");
                view = inflater.inflate(R.layout.default_layout, parent, false);
                holder = new EmptyViewHolder(view);
                break;
        }
        return holder;
    }
    private void verticalView(VerticalViewHolder holder, int position) {

        VerticalAdapter adapter1 = new VerticalAdapter(arrayList.get(position).getVerticalObj(), context);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.recyclerView.setAdapter(adapter1);
        ((SimpleItemAnimator) holder.recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        holder.recyclerView.setHasFixedSize(true);

    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {
        if (viewHolder.getItemViewType() == VERTICAL)
            verticalView((VerticalViewHolder) viewHolder, i );
        else if (viewHolder.getItemViewType() == HORIZONTAL)
            horizontalView((HorizontalViewHolder) viewHolder, i );


        Log.e("onbind","position"+i+" list size :"+arrayList);

    }
    @Override
    public int getItemViewType(int position) {
        Log.e("getItemViewType"," position:"+position);
        if(position == 0) {
            return HORIZONTAL;
        }else {
            if (MasterModel.Category.V == arrayList.get(position).getCategory()) {
                return VERTICAL;
            }
        }
        return 0;
    }

    private void horizontalView(HorizontalViewHolder holder, int position) {
        Catadappter mCatadappter = new Catadappter(context, arrayList.get(position).getHorizobtalObjs());
        LinearLayoutManager manager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerView.setLayoutManager(manager1);
        holder.recyclerView.setAdapter(mCatadappter);
    }

    @Override
    public int getItemCount() {
        Log.e("arrayList.size()","arrayList.size()"+arrayList.size());
        return arrayList.size();
    }
    public class HorizontalViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;


        HorizontalViewHolder(View itemView) {
            super(itemView);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.inner_recyclerView);
        }
    }
    public class EmptyViewHolder extends RecyclerView.ViewHolder {



        EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }
    public class VerticalViewHolder extends RecyclerView.ViewHolder {
        RecyclerView recyclerView;

        VerticalViewHolder(View itemView) {
            super(itemView);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.inner_recyclerView);
        }
    }

    private void shareTextUrl() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, "I AM GAURAV" + "\n\n" + "Download app for more updates" + "\n" + "https://bit.ly/2pDJWrB");
        //  Log.e("Link", Apis.talent_share + data.getSlug()/*+"\n\n"+"Download app for more updates"+"\n"+"https://bit.ly/2pDJWrB"*/);
        context.startActivity(Intent.createChooser(share, "Share link!"));
    }
}
