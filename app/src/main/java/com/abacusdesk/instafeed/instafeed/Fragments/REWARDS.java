package com.abacusdesk.instafeed.instafeed.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abacusdesk.instafeed.instafeed.Activity.Rewardpoint;
import com.abacusdesk.instafeed.instafeed.Activity.Rewards;
import com.abacusdesk.instafeed.instafeed.Adpater.RewardsAdapter;
import com.abacusdesk.instafeed.instafeed.Adpater.brandadapter;
import com.abacusdesk.instafeed.instafeed.Adpater.followadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.rewardsmodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class REWARDS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    RecyclerView recy;
    String token;
    RewardsAdapter mRewardsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.rew, container, false);
        token = SaveSharedPreference.getToken(getActivity());
        recy = (RecyclerView) view.findViewById(R.id.recy);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        recy.setLayoutManager(manager);
        Rewards();
        return view;
    }

    private void Rewards() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);

        Call<rewardsmodel> call = requestInterface.Rewards(Token);
        call.enqueue(new Callback<rewardsmodel>() {
            @Override
            public void onResponse(Call<rewardsmodel> call,
                                   Response<rewardsmodel> response) {
                if (response.isSuccessful()) {
                    mRewardsAdapter = new RewardsAdapter(response.body().getData(), getActivity());
                    recy.setAdapter(mRewardsAdapter);
                }
            }

            @Override
            public void onFailure(Call<rewardsmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}
