package com.abacusdesk.instafeed.instafeed.Payment;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.abacusdesk.instafeed.instafeed.R;
public class PanVerification extends Activity
{
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pan_verify);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
