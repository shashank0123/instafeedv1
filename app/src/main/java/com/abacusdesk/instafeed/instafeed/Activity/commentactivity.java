package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.Commentlistmodel;
import com.abacusdesk.instafeed.instafeed.Models.DATAComment;
import com.abacusdesk.instafeed.instafeed.Models.comentpostmodel;
import com.abacusdesk.instafeed.instafeed.Models.commentnewsmodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class commentactivity extends Activity {
    EditText userInput;
    RecyclerView recyclerView;
    EditText edcomment;
    LinearLayout lncomment;
    ImageView imgsend, img_back;
    static String type = "", catid = "";
    String Api_temp = "", postid = "", temp_title, Type;
    ArrayList<HashMap<String, String>> arrayhashComent = new ArrayList<>();
    String token,pid;
    commentadapter1 mcommentadapter1;
    String comment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.commentui);
        token = SaveSharedPreference.getToken(commentactivity.this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(commentactivity.this));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            postid = bundle.getString("postid");
            Type = bundle.getString("type");
        }
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        edcomment = (EditText) findViewById(R.id.ed_comment);
        imgsend = (ImageView) findViewById(R.id.img_send);
        imgsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    postComment();
                } else {

                }
            }
        });
        getComments_list();
    }

    ErrorCallback.MyCall<Commentlistmodel> myCall;

    private void getComments_list() {
        RequestInterface requestInterface = ApiFactory.createService(commentactivity.this, RequestInterface.class);

        Log.e("TYPE", "" + Type);
        if (Type.equalsIgnoreCase("Citi")) {
            myCall = requestInterface.Comm(postid);

        } else if (Type.equalsIgnoreCase("Brand")) {
            myCall = requestInterface.CommB(postid);

        } else if (Type.equalsIgnoreCase("Star")) {
            myCall = requestInterface.CommS(postid);
        }

        myCall.enqueue(new ErrorCallback.MyCallback<Commentlistmodel>() {
            @Override
            public void success(final retrofit2.Response<Commentlistmodel> response) {
                commentactivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body().getMessage().equals("success")) {
                            Log.e("getComments_list", response.body().toString());
                            mcommentadapter1 = new commentadapter1(response.body().getData(), commentactivity.this);
                            recyclerView.setAdapter(mcommentadapter1);
                        } else {


                        }
                    }
                });
                // Showing Alert Message
            }

            @Override
            public void error(String errorMessage) {

            }
        }, commentactivity.this, Boolean.TRUE);

    }

    Call<comentpostmodel> call;

    //{comment=nice , id=11224, user=8221, token=$1$65oMZWUj$qH1CYMPZyVtxb3jw2gVK11}
    private void postComment() {

        String id = postid;
        comment = edcomment.getText().toString().trim();
        RequestInterface requestInterface = ApiFactory.createService(commentactivity.this, RequestInterface.class);

        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, id);
        RequestBody Comment =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, comment);
        if (Type.equalsIgnoreCase("Citi")) {
            call = requestInterface.commentciti(Token, Id, Comment);
        } else if (Type.equalsIgnoreCase("Brand")) {
            call = requestInterface.commentBrand(Token, Id, Comment);
        } else if (Type.equalsIgnoreCase("Star")) {
            call = requestInterface.commentStar(Token, Id, Comment);
        }
        call.enqueue(new Callback<comentpostmodel>() {
            @Override
            public void onResponse(Call<comentpostmodel> call,
                                   retrofit2.Response<comentpostmodel> response) {
                //  Log.e("getStatus()", response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    edcomment.setText("");
                    getComments_list();
                 /*   mcommentlistadapter = new commentlistadapter(response.body().getCommentData().getData(), vdetailclass.this);
                    list_product.setAdapter(mcommentlistadapter);*/
                } else {
                    Log.e("String", "TEST111");
                }
            }

            @Override
            public void onFailure(Call<comentpostmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    class commentadapter1 extends RecyclerView.Adapter<commentadapter1.MyViewHolder> {

        Context context;
        ArrayList<DATAComment> data;
        public String comment = "", comment_id;
        Dialog dialog;
        String type;

        public commentadapter1(ArrayList<DATAComment> data, Context context) {
            this.context = context;
            this.data = data;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView txtname, txtcomment;
            public ImageView imgprofile, imgedit;
            //public LinearLayout lnrow;

            public MyViewHolder(View view) {
                super(view);
                this.imgprofile = (ImageView) itemView.findViewById(R.id.img_profilepic);
                this.imgedit = (ImageView) itemView.findViewById(R.id.img_edit);
                this.txtname = (TextView) itemView.findViewById(R.id.txt_name);
                this.txtcomment = (TextView) itemView.findViewById(R.id.txt_comment);
            }
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_rowlist, parent, false);
            return new commentadapter1.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            String myFormat = "yyyy-MM-dd HH:mm:ss";
            DateFormat sdformat = new SimpleDateFormat(myFormat);
            DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");

            holder.txtcomment.setText(data.get(position).getComment());
            holder.txtname.setText(data.get(position).getFirstName());
            if (data.get(position).getAvatar() != null && !data.get(position).getAvatar().isEmpty()) {
                Glide.with(context).load(data.get(position).getAvatar()).error(R.drawable.user).into(holder.imgprofile);
            } else {
                holder.imgprofile.setImageResource(R.drawable.user);
            }


            holder.imgedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // get prompts.xml view
                     pid = data.get(position).getCommentId();

                    String ab = data.get(position).getComment();
                    LayoutInflater li = LayoutInflater.from(context);
                    View promptsView = li.inflate(R.layout.prompts, null);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            context);

                    // set prompts.xml to alertdialog builder
                    alertDialogBuilder.setView(promptsView);

                      userInput = (EditText) promptsView
                            .findViewById(R.id.ed_comment);
                     ImageView send = (ImageView) promptsView
                            .findViewById(R.id.img_send);
                    userInput.setText(ab);

                    send.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
if(userInput.getText().toString().trim().length()>1){
    postCommentEd();
    dialog.cancel();
}

                        }
                    });
                    // set dialog message
               /* alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // get user input and set it to result
                                        // edit text
                                        //result.setText(userInput.getText());
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });*/

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }
            });
        }


        @Override
        public int getItemCount() {
            return data.size();
        }
    }
    private void postCommentEd() {



        RequestInterface requestInterface = ApiFactory.createService(commentactivity.this, RequestInterface.class);

        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, pid);
        RequestBody Comment =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, userInput.getText().toString().trim());
        if (Type.equalsIgnoreCase("Citi")) {
            call = requestInterface.commentciti(Token, Id, Comment);
        } else if (Type.equalsIgnoreCase("Brand")) {
            call = requestInterface.commentBrand(Token, Id, Comment);
        } else if (Type.equalsIgnoreCase("Star")) {
            call = requestInterface.commentStar(Token, Id, Comment);
        }
        call.enqueue(new Callback<comentpostmodel>() {
            @Override
            public void onResponse(Call<comentpostmodel> call,
                                   retrofit2.Response<comentpostmodel> response) {
                //  Log.e("getStatus()", response.body().getStatus().toString());
                if (response.isSuccessful()) {
                    userInput.setText("");

                    getComments_list();
                 /*   mcommentlistadapter = new commentlistadapter(response.body().getCommentData().getData(), vdetailclass.this);
                    list_product.setAdapter(mcommentlistadapter);*/
                } else {
                    Log.e("String", "TEST111");
                }
            }

            @Override
            public void onFailure(Call<comentpostmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

}

