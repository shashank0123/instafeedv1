package com.abacusdesk.instafeed.instafeed.Adpater;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.Activity.Citizendetailpage;
import com.abacusdesk.instafeed.instafeed.Activity.Citizenprofile;
import com.abacusdesk.instafeed.instafeed.Activity.MYFav;
import com.abacusdesk.instafeed.instafeed.Activity.commentactivity;
import com.abacusdesk.instafeed.instafeed.Activity.who_like;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ApiURL;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Fragments.ExampleBottomSheetDialog;
import com.abacusdesk.instafeed.instafeed.Fragments.Feedfrgment;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.Models.Citizendata;
import com.abacusdesk.instafeed.instafeed.Models.favmodel;
import com.abacusdesk.instafeed.instafeed.Models.likemodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;
import com.devs.readmoreoption.ReadMoreOption;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerticalAdapter extends RecyclerView.Adapter<VerticalAdapter.ViewHolder> {
    Citizendata data;
    private String imagetemp = "";
    String formattedDate;
    Context context;
    Feedfrgment ctx;
    String uid;
    String temp_str;
    int videoPaused = 0;
    String token;
    String id;
    String vote;
    private ArrayList<Citizendata> arrayList;
    String userid, userImage;
    String upperString;
    ReadMoreOption readMoreOption;

    public VerticalAdapter(Citizendata data, Context context) {
        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view;
        ViewHolder holder;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.citizenrow, parent, false);
        holder = new ViewHolder(view);
        token = SaveSharedPreference.getToken(context);
        readMoreOption = new ReadMoreOption.Builder(context).build();
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        Log.e("state", "VerticalAdapteronBindViewHolder position :" + i);
        String myFormat = "yyyy-MM-ddHH:mm:ss";
        DateFormat sdformat = new SimpleDateFormat(myFormat);
        DateFormat targetFormat = new SimpleDateFormat("dd MMMM yyyy");
        //backgroundImg.setBackgroundColor(Color.parseColor("#FFFFFF"));
        if (data.getIsLike().equals("1")) {
            viewHolder.like.setImageResource(R.mipmap.heartcol);
        } else {
            viewHolder.like.setImageResource(R.mipmap.heart);
        }
        if (data.isBookmark()) {
            viewHolder.bookmark.setImageResource(R.mipmap.bookmarked);
        } else {
            viewHolder.bookmark.setImageResource(R.mipmap.bookmark);

        }
        if (data.getVideoThumb() != null && !data.getVideoThumb().isEmpty()) {
            Log.e("Vedio", "Vedio" + data.getVideoThumb() + i);
            viewHolder.vedios.setVisibility(View.VISIBLE);
            Picasso.with(context).load(data.getVideoThumb()).error(R.drawable.app_icon).into(viewHolder.feedimage);
        } else if (data.getImage360x290() != null && !data.getImage360x290().isEmpty()) {
            viewHolder.vedios.setVisibility(View.GONE);
            Log.e("Vedio", "Image" + i);
            Picasso.with(context).load(data.getImage360x290()).error(R.drawable.app_icon).into(viewHolder.feedimage);
        }
        viewHolder.bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    id = data.getId();
                    data.setBookmark(true);
                    Bookmark(viewHolder);
                } else {

                }
            }
        });

        viewHolder.proimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, Citizenprofile.class);
                Bundle bundle = new Bundle();
                bundle.putString("username", data.getUsername());
                bundle.putString("userid", data.getUserId());
                in.putExtras(bundle);
                context.startActivity(in);
             /*
                context.startActivity(in);*/
            }
        });

        viewHolder.dots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = MainActivity.ctx.getSupportFragmentManager();
                ExampleBottomSheetDialog newFragment = new ExampleBottomSheetDialog();
                newFragment.show(fm, "abc");
            }
        });
        viewHolder.likecount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    Intent in = new Intent(context, who_like.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("postid", data.getId());
                    bundle.putString("type", "Citi");
                    in.putExtras(bundle);
                    context.startActivity(in);
                } else {

                }

            }
        });
        viewHolder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!token.equals("")
                        && token != null) {
                    if (data.getIsLike().equals("0")) {
                        id = data.getId();
                        vote = "u";
                        data.setIsLike("1");
                        vote(viewHolder);
                    } else {
                        id = data.getId();
                        vote = "d";
                        data.setIsLike("0");
                        votedown(viewHolder);
                    }
                } else {

                }
            }
        });
        viewHolder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareTextUrl();
            }
        });

        viewHolder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, commentactivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("postid", data.getId());
                bundle.putString("type", "Citi");
                in.putExtras(bundle);
                context.startActivity(in);
            }
        });


        if (data.getIsAnonymous().equalsIgnoreCase("Y")) {

            viewHolder.name.setText("Anonymous");
            viewHolder
                    .proimage.setImageResource(R.drawable.user);
        } else {

            if (data.getFirstName() == null) {
                upperString = data.getUsername().substring(0, 1).toUpperCase() + data.getUsername().substring(1);
                viewHolder.name.setText(upperString);
            } else {
                upperString = data.getFirstName().substring(0, 1).toUpperCase() + data.getFirstName().substring(1);

                viewHolder.name.setText(upperString);

            }
            Picasso.with(context).load(data.getAvatar()).error(R.drawable.navuser).into(viewHolder.proimage);
            viewHolder.feedimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent in = new Intent(context, Citizendetailpage.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("postid", data.getId());
                    bundle.putString("type", "Citi");
                    in.putExtras(bundle);
                    context.startActivity(in);

                }
            });
         /*   Glide.with(context).load(data.getAvatar()).centerCrop().fitCenter().error(R.drawable.user).
                    into(viewHolder.proimage);*/
        }
        try {
            formattedDate = targetFormat.format(sdformat.parse(data.getDtAdded()));
            viewHolder.dateontym.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //   Picasso.with(context).load(data.getImage360x290()).error(R.drawable.app_icon).into(viewHolder.feedimage);
        //    Glide.with(context).load(data.getImage360x290()).centerCrop().fitCenter().into(viewHolder.feedimage);
        if (data.getTitle() != null && !data.getTitle().isEmpty()) {
            upperString = data.getTitle().substring(0, 1).toUpperCase() + data.getTitle().substring(1);
        }

        readMoreOption = new ReadMoreOption.Builder(context)
                .textLength(2, ReadMoreOption.TYPE_LINE) // OR
                //.textLength(300, ReadMoreOption.TYPE_CHARACTER)
                .moreLabel("See More")
                .lessLabel(".See Less")
                .moreLabelColor(Color.parseColor("#FF4500"))
                .lessLabelColor(Color.parseColor("#FF4500"))
                .expandAnimation(true)
                .build();

        readMoreOption.addReadMoreTo(viewHolder.txt_title, upperString);

        //  viewHolder.txt_title.setText(upperString);
        viewHolder.likecount.setText(data.getTotalLikes());
        viewHolder.commntcount.setText(data.getTotalComments());
        viewHolder.sharecount.setText(data.getTotalLikes());

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView proimage, dots, bookmark, like, share, comment, vedios;
        public SimpleDraweeView feedimage;
        public TextView name, dateontym, txt_title, likecount, commntcount, sharecount;
        CoordinatorLayout coordinatorLayout;

        //public  VideoView videoView;
//        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            dateontym = (TextView) itemView.findViewById(R.id.dateontym);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            likecount = (TextView) itemView.findViewById(R.id.likecount);
            commntcount = (TextView) itemView.findViewById(R.id.commntcount);
            sharecount = (TextView) itemView.findViewById(R.id.sharecount);
            coordinatorLayout = (CoordinatorLayout) itemView.findViewById(R.id.coordinatorLayout);
            proimage = (ImageView) itemView.findViewById(R.id.proimage);
            dots = (ImageView) itemView.findViewById(R.id.dots);
            bookmark = (ImageView) itemView.findViewById(R.id.bookmark);
            like = (ImageView) itemView.findViewById(R.id.like);
            feedimage = (SimpleDraweeView) itemView.findViewById(R.id.feedimage);
            vedios = (ImageView) itemView.findViewById(R.id.vedios);
            share = (ImageView) itemView.findViewById(R.id.share);
            comment = (ImageView) itemView.findViewById(R.id.comment);
        }
    }

    private void vote(final ViewHolder viewHolder) {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, id);
        RequestBody Vote =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, vote);
        Call<likemodel> call = requestInterface.vote(Token, Id, Vote);
        call.enqueue(new Callback<likemodel>() {
            @Override
            public void onResponse(Call<likemodel> call,
                                   Response<likemodel> response) {

                if (response.isSuccessful()) {
                    Log.e("Strring", "TEST" + response.body().getData().toString());
                    viewHolder.likecount.setText(response.body().getData().getTotalLikes());
                    viewHolder.like.setImageResource(R.mipmap.heartcol);
                } else {
                    Toast.makeText(context, response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("String", "TEST111");
                }
            }

            @Override
            public void onFailure(Call<likemodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    private void votedown(final ViewHolder viewHolder) {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, id);
        RequestBody Vote =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, vote);
        Call<likemodel> call = requestInterface.voted(Token, Id, Vote);
        call.enqueue(new Callback<likemodel>() {
            @Override
            public void onResponse(Call<likemodel> call,
                                   Response<likemodel> response) {
                if (response.isSuccessful()) {
                    Log.e("Strring", "TEST" + response.body().getData().toString());
                    viewHolder.likecount.setText(response.body().getData().getTotalLikes());
                    viewHolder.like.setImageResource(R.mipmap.heart);
                } else {
                    Toast.makeText(context, response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("String", "TEST111");
                }
            }

            @Override
            public void onFailure(Call<likemodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    private void Bookmark(final ViewHolder viewHolder) {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(context, RequestInterface.class);
        RequestBody Token =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, token);
        RequestBody Id =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, id);
        RequestBody IdM = RequestBody.create(
                okhttp3.MultipartBody.FORM, "1");
        Log.e("token", "" + token + "id" + id);
        Call<favmodel> call = requestInterface.fav(Token, Id, IdM);
        call.enqueue(new Callback<favmodel>() {
            @Override
            public void onResponse(Call<favmodel> call,
                                   Response<favmodel> response) {
                if (response.isSuccessful()) {
                    viewHolder.bookmark.setImageResource(R.mipmap.bookmarked);
                    Snackbar snackbar = Snackbar
                            .make(viewHolder.coordinatorLayout, "Added to bookmarks", Snackbar.LENGTH_LONG)
                            .setActionTextColor(Color.parseColor("#FF4500"))
                            .setAction("See All", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent in = new Intent(context, MYFav.class);
                                    context.startActivity(in);
                                }
                            });
                    snackbar.show();
                } else {
                }
            }

            @Override
            public void onFailure(Call<favmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    private void shareTextUrl() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        share.putExtra(Intent.EXTRA_TEXT, ApiURL.Base + data.getSlug() + "\n\n" + "Download app for more updates" + "\n" + "https://bit.ly/2pDJWrB");
        //  Log.e("Link", Apis.talent_share + data.getSlug()/*+"\n\n"+"Download app for more updates"+"\n"+"https://bit.ly/2pDJWrB"*/);
        context.startActivity(Intent.createChooser(share, "Share link!"));
    }
}


