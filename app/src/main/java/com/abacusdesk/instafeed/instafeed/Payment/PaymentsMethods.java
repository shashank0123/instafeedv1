package com.abacusdesk.instafeed.instafeed.Payment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.abacusdesk.instafeed.instafeed.R;

/**
 * Created by Instafeed2 on 7/16/2019.
 */

public class PaymentsMethods extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payments_method);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
