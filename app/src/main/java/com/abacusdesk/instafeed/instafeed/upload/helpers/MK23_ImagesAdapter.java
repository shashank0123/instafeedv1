package com.abacusdesk.instafeed.instafeed.upload.helpers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.R;
import com.bumptech.glide.Glide;

import java.util.List;




public class MK23_ImagesAdapter extends RecyclerView.Adapter<MK23_ImagesAdapter.ViewHolder> {

    Context context;
    List<MK23_ImagesModel> imagesList;

    public MK23_ImagesAdapter(Context context, List<MK23_ImagesModel> imagesList) {
        this.context = context;
        this.imagesList = imagesList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.images_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Glide.with(context).load(imagesList.get(position).getImageURI()).centerCrop().into(holder.imageView);
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeImage(position);
            }
        });
    }

    private void removeImage(int position) {
        imagesList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        ImageButton remove;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.video_layout_video_container);
            remove = itemView.findViewById(R.id.image_layout_image_remover);
        }
    }
}
