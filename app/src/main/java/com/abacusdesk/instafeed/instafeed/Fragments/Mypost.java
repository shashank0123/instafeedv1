package com.abacusdesk.instafeed.instafeed.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abacusdesk.instafeed.instafeed.Adpater.mypostadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.Citizendata;
import com.abacusdesk.instafeed.instafeed.Models.MasterModel;
import com.abacusdesk.instafeed.instafeed.Mycode.helper;
import com.abacusdesk.instafeed.instafeed.R;

import java.util.ArrayList;

public class Mypost extends Fragment {
    private ArrayList<MasterModel> citizenData;
    RecyclerView recy;
    LinearLayoutManager manager;
    ArrayList<Citizendata> citizendata;
    ArrayList<MasterModel> masterModels;
    mypostadapter mmypostadapter;
    public Mypost() {
    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View view;
        view = inflater.inflate(R.layout.fragment_2, container, false);
        recy = (RecyclerView) view.findViewById(R.id.rec);
        recy.setLayoutManager(new LinearLayoutManager(getActivity()));
        recy.setNestedScrollingEnabled(false);
        recy.setHasFixedSize(false);
        contryFF();
        return view;
    }
    private void contryFF() {
        final RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        ErrorCallback.MyCall<com.abacusdesk.instafeed.instafeed.Models.Citizen> myCall;
        myCall = requestInterface.ublicnews(helper.userid);
        myCall.enqueue(new ErrorCallback.MyCallback<com.abacusdesk.instafeed.instafeed.Models.Citizen>() {
            @Override
            public void success(final retrofit2.Response<com.abacusdesk.instafeed.instafeed.Models.Citizen> res) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mmypostadapter = new mypostadapter(res.body().getData(),  getActivity());
                        recy.setAdapter(mmypostadapter);
                        }
                });
            }
            @Override
            public void error(String errorMessage) {
            }
        }, getActivity(), Boolean.FALSE);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }
}