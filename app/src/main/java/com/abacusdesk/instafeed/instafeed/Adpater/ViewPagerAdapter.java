package com.abacusdesk.instafeed.instafeed.Adpater;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.abacusdesk.instafeed.instafeed.Fragments.MyFragment;
import com.abacusdesk.instafeed.instafeed.R;
/**
 * Created by HNAbbasi on 8/18/15.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    String[] tabs;

    public ViewPagerAdapter(FragmentManager fm, String[] tabs) {
        super(fm);
        this.tabs = tabs;
    }

    @Override
    public Fragment getItem(int i) {
        return MyFragment.newInstance( tabs[i]);
    }

    @Override
    public int getCount() {
        return tabs.length;
    }
}