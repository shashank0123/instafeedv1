package com.abacusdesk.instafeed.instafeed.Responsemodel;

import com.google.gson.annotations.SerializedName;

public class subscmodel {
    @SerializedName("status")
 
    private Integer status;
    @SerializedName("message")
 
    private String message;
    @SerializedName("data")
 
    private DataS data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataS getData() {
        return data;
    }

    public void setData(DataS data) {
        this.data = data;
    }

}
