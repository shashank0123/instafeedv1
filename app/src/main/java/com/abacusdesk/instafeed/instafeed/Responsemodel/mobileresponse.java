package com.abacusdesk.instafeed.instafeed.Responsemodel;

import com.google.gson.annotations.SerializedName;

public class mobileresponse {
    @SerializedName("status")
   
    private Integer status;
    @SerializedName("message")
   
    private String message;
    @SerializedName("data")
   
    private DataMobile data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataMobile getData() {
        return data;
    }

    public void setData(DataMobile data) {
        this.data = data;
    }

}
