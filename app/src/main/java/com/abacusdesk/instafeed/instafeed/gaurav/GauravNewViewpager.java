package com.abacusdesk.instafeed.instafeed.gaurav;

/**
 * Created by Instafeed2 on 9/9/2019.
 */


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.Citizendetailpage;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.webview.VideoEnabledWebChromeClient;
import com.abacusdesk.instafeed.instafeed.webview.VideoEnabledWebView;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import tcking.github.com.giraffeplayer2.VideoView;

public class GauravNewViewpager extends PagerAdapter {

    private LayoutInflater inflater;
    private Citizendetailpage context;
    ArrayList<HashMap<String, String>> arrayhashmap;

    private String type = "";
    //JCVideoPlayer jcVideoPlayer;
    static String audioUrl = "";
    public SeekBar seekBar;
    private double startTime = 0;
    private double finalTime = 0;
    private int forwardTime = 5000;
    private int backwardTime = 5000;
    public int oneTimeOnly = 0;

    private TextView txttime, txttimeend;
    private Boolean isplay = false;
    static MediaController mediaController;
    public ImageView btnplay, imgbackward, imgforward;
    public boolean playPause;
    public MediaPlayer mediaPlayer;
    boolean intialStage = true;
    //  public static VideoView videoView;
    Handler myHandler = new Handler();
    public VideoEnabledWebView webView;
    public VideoEnabledWebChromeClient webChromeClient;

    public GauravNewViewpager(Citizendetailpage context, ArrayList<HashMap<String, String>> arrayhashmap, String type) {
        this.context = context;
        this.type = type;
        this.arrayhashmap = arrayhashmap;
        inflater = LayoutInflater.from(context);
        notifyDataSetChanged();
        Log.e("arrayhashmap", "" + arrayhashmap.size());
    }

    @Override
    public int getCount() {
        return arrayhashmap.size();
    }

    public void setupWebView(View imageLayout, View myview) {
        webView = (VideoEnabledWebView) imageLayout.findViewById(R.id.webView);
       // Initialize the VideoEnabledWebChromeClient and set event handlers
        View nonVideoLayout = imageLayout.findViewById(R.id.nonVideoLayout); // Your own view, read class comments
        ViewGroup videoLayout = (ViewGroup) imageLayout.findViewById(R.id.videoLayout); // Your own view, read class comments
        //noinspection all
        View loadingView = inflater.inflate(R.layout.view_loading_video, null); // Your own view, read class comments
        webChromeClient = new VideoEnabledWebChromeClient(nonVideoLayout, videoLayout, loadingView, webView,myview) {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                //super.onProgressChanged(view, newProgress);
                View decorView = ((Activity) context).getWindow().getDecorView();
                decorView.setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                      /*  | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar*/
                                | View.SYSTEM_UI_FLAG_IMMERSIVE);
            }
        };
        webChromeClient.setOnToggledFullscreen(new VideoEnabledWebChromeClient.ToggledFullscreenCallback() {
            @Override
            public void toggledFullscreen(boolean fullscreen) {
                // Your code to handle the full-screen change, for example showing and hiding the title bar. Example:
                if (fullscreen) {
                    WindowManager.LayoutParams attrs = ((Activity) context).getWindow().getAttributes();
                    attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    ((Activity) context).getWindow().setAttributes(attrs);
                    if (android.os.Build.VERSION.SDK_INT >= 14) {
                        ((Activity) context).getWindow().getDecorView().setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
                    }
                } else {
                    Log.e("else ", "fullscreen");
                    WindowManager.LayoutParams attrs = ((Activity) context).getWindow().getAttributes();
                    Log.e("else ", "fullscreen1");
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
                    Log.e("else ", "fullscreen2");
                    attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    Log.e("else ", "fullscreen3");
                    ((Activity) context).getWindow().setAttributes(attrs);
                    Log.e("else ", "fullscreen4");
                    if (android.os.Build.VERSION.SDK_INT >= 14) {
                        //noinspection all
                        Log.e("else ", "fullscreen5");

                         ((Activity) context).getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
                      /*  ((Activity) context).getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);*/
                        Log.e("else ", "fullscreen6");
                    }
                }
            }
        });
        webView.setWebChromeClient(webChromeClient);
        // Call private class InsideWebViewClient
        webView.setWebViewClient(new InsideWebViewClient());

        // Navigate anywhere you want, but consider that this classes have only been tested on YouTube's mobile site
    }

    private class InsideWebViewClient extends WebViewClient {
        @Override
        // Force links to be opened inside WebView and not in Default Browser
        // Thanks http://stackoverflow.com/a/33681975/1815624
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout;
        Log.e("arrayhashmap", "" + arrayhashmap.get(position).toString());
        if (arrayhashmap.get(position).containsKey("video")) {
            imageLayout = inflater.inflate(R.layout.item_videoview, view, false);
            View myview = inflater.inflate(R.layout.activity_scrolling, view, false);

            setupWebView(imageLayout, myview);
            String videoUrl = arrayhashmap.get(position).get("video");
            String src = "<video style=\"width: 100%;\" src=\"" + videoUrl + "\" controls controlsList=\"nodownload\" \"fullscreen\"></video>";
            webView.loadData(src, "text/html", "UTF-8");

          /*  videoView = (VideoView)
                    imageLayout.findViewById(R.id.video_view);
            Glide.with(context).load(arrayhashmap.get(position).get("image")).into(videoView.getCoverView());

            videoView.setVideoPath(arrayhashmap.get(position).get("video"));*/
            //videoView.getPlayer().start();
            view.addView(imageLayout);
        } else if (arrayhashmap.get(position).containsKey("audio")) {
            imageLayout = inflater.inflate(R.layout.item_audio, view, false);

            view.addView(imageLayout);
        } else {
            imageLayout = inflater.inflate(R.layout.item_viewimage, view, false);
            final ImageView imgview = (ImageView) imageLayout.findViewById(R.id.img_view);

            if (type.equalsIgnoreCase("news")) {
                if (!arrayhashmap.get(position).get("image").isEmpty()) {
                    Glide.with(context).load(arrayhashmap.get(position).get("image")).into(imgview);
                    // Picasso.with(context).load(arrayhashmap.get(position).get("image")).error(R.drawable.newsdefault).into(imgview);
                    Log.e("imge testtt1", arrayhashmap.get(position).get("image"));
                }
                view.addView(imageLayout);
            }
        }
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


}