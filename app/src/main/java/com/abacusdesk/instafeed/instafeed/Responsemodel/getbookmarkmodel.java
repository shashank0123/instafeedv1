package com.abacusdesk.instafeed.instafeed.Responsemodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class getbookmarkmodel {
  //  {"status":200,"message":"success","data":[{"id":"18","news_category_id":"2","title":"testing news post","slug":"stormy-sterling-sinks-again-stocks-wilt-before-fed-1355566168","short_description":"testing news post","location_id":"1","total_likes":"2","total_dislikes":"4","total_comments":"4","total_views":"12","total_flags":"3","dt_added":"2019-07-30 17:32:20","dt_modified":"2019-07-30 17:32:20","status":"P","user_id":"4","image":"http:\/\/13.234.116.90\/storage\/news_mime\/256x170-1564488140-download (2).jpg","image_zoom":"http:\/\/13.234.116.90\/storage\/news_mime\/748x420-1564488140-download (2).jpg","image_original":"http:\/\/13.234.116.90\/storage\/news_mime\/1564488140-download (2).jpg","image_100x100":"http:\/\/13.234.116.90\/storage\/news_mime\/100x100-1564488140-download (2).jpg","image_256x170":"http:\/\/13.234.116.90\/storage\/news_mime\/256x170-1564488140-download (2).jpg","image_264x200":"http:\/\/13.234.116.90\/storage\/news_mime\/264x200-1564488140-download (2).jpg","image_360x290":"http:\/\/13.234.116.90\/storage\/news_mime\/360x290-1564488140-download (2).jpg","video_thumb":"","video_original":"","video_zoom":"","video_100x100":"","video_256x170":"","video_264x200":"","video_360x290":"","source":"m","latitude":"28","longitude":"77","is_anonymous":"n"},{"id":"17","news_category_id":"2","title":"jai bharat jai bheej jai kishan jai gagan","slug":"jai-bharat-jai-bheej-jai-kishan-jai-gagan-1483065440","short_description":"jai bharat jai bheem jai kisan ","location_id":"1","total_likes":"1","total_dislikes":"1","total_comments":"0","total_views":"6","total_flags":"2","dt_added":"2019-07-30 15:41:34","dt_modified":"2019-07-30 15:41:34","status":"P","user_id":"4","image":"http:\/\/13.234.116.90\/storage\/news_mime\/256x170-1564481494-Ephimaries.JPG","image_zoom":"http:\/\/13.234.116.90\/storage\/news_mime\/748x420-1564481494-Ephimaries.JPG","image_original":"http:\/\/13.234.116.90\/storage\/news_mime\/1564481494-Ephimaries.JPG","image_100x100":"http:\/\/13.234.116.90\/storage\/news_mime\/100x100-1564481494-Ephimaries.JPG","image_256x170":"http:\/\/13.234.116.90\/storage\/news_mime\/256x170-1564481494-Ephimaries.JPG","image_264x200":"http:\/\/13.234.116.90\/storage\/news_mime\/264x200-1564481494-Ephimaries.JPG","image_360x290":"http:\/\/13.234.116.90\/storage\/news_mime\/360x290-1564481494-Ephimaries.JPG","video_thumb":"","video_original":"","video_zoom":"","video_100x100":"","video_256x170":"","video_264x200":"","video_360x290":"","source":"m","latitude":"28","longitude":"77","is_anonymous":"n"}]}

    @SerializedName("status")
     
    private Integer status;
    @SerializedName("message")
     
    private String message;
    @SerializedName("data")
     
    private ArrayList<DATAGETBOOK> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DATAGETBOOK> getData() {
        return data;
    }

    public void setData(ArrayList<DATAGETBOOK> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "getbookmarkmodel{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
