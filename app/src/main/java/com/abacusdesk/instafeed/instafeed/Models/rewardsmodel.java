package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class rewardsmodel {
    @SerializedName("status")
    
    private Integer status;
    @SerializedName("message")
    
    private String message;
    @SerializedName("user_reward_point")
    
    private String userRewardPoint;
    @SerializedName("wallet_balance")
    
    private String walletBalance;
    @SerializedName("data")
    
    private ArrayList<DataRewards> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserRewardPoint() {
        return userRewardPoint;
    }

    public void setUserRewardPoint(String userRewardPoint) {
        this.userRewardPoint = userRewardPoint;
    }

    public String getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }

    public ArrayList<DataRewards> getData() {
        return data;
    }

    public void setData(ArrayList<DataRewards> data) {
        this.data = data;
    }
}
