package com.abacusdesk.instafeed.instafeed.kyc;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.abacusdesk.instafeed.instafeed.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Instafeed2 on 8/23/2019.
 */

public class URLVerification extends AppCompatActivity {
    Spinner http_spinner;
    ImageView back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.url_verification);
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        http_spinner = (Spinner) findViewById(R.id.http_spinner);
        List<String> list = new ArrayList<String>();
        list.add("https://");
        list.add("list 2");
        list.add("list 3");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        http_spinner.setAdapter(dataAdapter);
        http_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(URLVerification.this,
                        "OnClickListener : " +
                                "\nSpinner  : "+ String.valueOf(http_spinner.getSelectedItem()),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
