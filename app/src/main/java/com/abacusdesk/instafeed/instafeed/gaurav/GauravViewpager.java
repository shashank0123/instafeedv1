package com.abacusdesk.instafeed.instafeed.gaurav;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import tcking.github.com.giraffeplayer2.VideoView;

public class GauravViewpager extends PagerAdapter {

    private LayoutInflater inflater;
    private Context context;
    ArrayList<HashMap<String, String>> arrayhashmap;

    private String type = "";
    //JCVideoPlayer jcVideoPlayer;
    static String audioUrl = "";
    public SeekBar seekBar;
    private double startTime = 0;
    private double finalTime = 0;
    private int forwardTime = 5000;
    private int backwardTime = 5000;
    public int oneTimeOnly = 0;

    private TextView txttime, txttimeend;
    private Boolean isplay = false;
    static MediaController mediaController;
    public ImageView btnplay, imgbackward, imgforward;
    public boolean playPause;
    public MediaPlayer mediaPlayer;
    boolean intialStage = true;
    public static VideoView videoView;
    Handler myHandler = new Handler();

    public GauravViewpager(Context context, ArrayList<HashMap<String, String>> arrayhashmap, String type) {
        this.context = context;
        this.type = type;
        this.arrayhashmap = arrayhashmap;
        inflater = LayoutInflater.from(context);
        notifyDataSetChanged();
        Log.e("arrayhashmap",""+arrayhashmap.size());
    }
    @Override
    public int getCount() {
        return arrayhashmap.size();
    }
    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout;
        Log.e("arrayhashmap",""+arrayhashmap.get(position).toString());
        if (arrayhashmap.get(position).containsKey("video")) {
            imageLayout = inflater.inflate(R.layout.item_videoview, view, false);
            videoView = (VideoView)
                    imageLayout.findViewById(R.id.video_view);
            Glide.with(context).load(arrayhashmap.get(position).get("image")).into(videoView.getCoverView());

            videoView.setVideoPath(arrayhashmap.get(position).get("video"));
            //videoView.getPlayer().start();
            view.addView(imageLayout);
        } else if (arrayhashmap.get(position).containsKey("audio")) {
            imageLayout = inflater.inflate(R.layout.item_audio, view, false);

            view.addView(imageLayout);
        } else {
            imageLayout = inflater.inflate(R.layout.item_viewimage, view, false);
            final ImageView imgview = (ImageView) imageLayout.findViewById(R.id.img_view);

            if (type.equalsIgnoreCase("news")) {
                if (!arrayhashmap.get(position).get("image").isEmpty()) {
                    Glide.with(context).load(arrayhashmap.get(position).get("image")).into(imgview);
                    // Picasso.with(context).load(arrayhashmap.get(position).get("image")).error(R.drawable.newsdefault).into(imgview);
                    Log.e("imge testtt1", arrayhashmap.get(position).get("image"));
                }
                view.addView(imageLayout);
            }
        }
            return imageLayout;
        }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


}