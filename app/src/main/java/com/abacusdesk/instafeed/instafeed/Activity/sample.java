package com.abacusdesk.instafeed.instafeed.Activity;


import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabWidget;

import com.abacusdesk.instafeed.instafeed.Adpater.ViewPagerAdapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.ErrorCallback;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Fragments.MyFragment;
import com.abacusdesk.instafeed.instafeed.Models.brandmodelcat;
import com.abacusdesk.instafeed.instafeed.Models.brandpromodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.squareup.picasso.Picasso;

import android.support.v4.app.FragmentActivity;
import android.widget.FrameLayout;
import android.widget.TabHost;
import android.widget.TextView;

public class sample extends FragmentActivity  {
    // TextInputLayout inputfname
    LinearLayout social, emaillogin, l1, l2;
    private String[] tabs;
    FragmentTabHost tabHost;
    ViewPagerAdapter pagerAdapter;
    ViewPager viewPager;
    private TabWidget tabWidget;
    private HorizontalScrollView horizontalScrollView;
    sample context;
    String username,id;
    TextView name;
    ImageView img_back,img_header_bg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(getWindow().FEATURE_NO_TITLE);
        setContentView(R.layout.brandprofile);
        context = this;
        img_back=(ImageView)findViewById(R.id.img_back);
        img_header_bg=(ImageView)findViewById(R.id.img_header_bg);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        viewPager = (ViewPager) findViewById(R.id.pager);
        name=(TextView)findViewById(R.id.name);
        tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabWidget = (TabWidget) findViewById(android.R.id.tabs);
        tabHost.setup(this, context.getSupportFragmentManager(), R.id.realTabContent);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            username = bundle.getString("username");
            id= bundle.getString("id");
        }
        CATdetail();
        CAT();

    }
    void setupData(String[] aa, String[] i)
    {
        initializeHorizontalTabs();
        initializeTabs(aa);
        setupTabHost();

        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), i);
        viewPager.setAdapter(pagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            /**
             * on swipe select the respective tab
             * */
            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();
                tabHost.setCurrentTab(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                invalidateOptionsMenu();
            }
        });

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                viewPager.setCurrentItem(tabHost.getCurrentTab());
                scrollToCurrentTab();
            }
        });
    }
    private void CAT() {
        final RequestInterface requestInterface = ApiFactory.createService(sample.this, RequestInterface.class);
        ErrorCallback.MyCall<brandmodelcat> myCall;
        myCall = requestInterface.getcatrun( "4");
        myCall.enqueue(new ErrorCallback.MyCallback<brandmodelcat>() {
            @Override
            public void success(final retrofit2.Response<brandmodelcat> res) {
                sample.this.runOnUiThread(new  Runnable() {
                    @Override
                    public void run() {
                        Log.e("contryFF", "started... size : "+res.body().getData().size());
                        String[] aa = new String[res.body().getData().size()];
                        String i[]= new String[res.body().getData().size()];
                        for(int l=0; l<=res.body().getData().size()-1; l++){
                            aa[l]=res.body().getData().get(l).getCategoryName();
                            i[l]=res.body().getData().get(l).getCategoryId();
   }
                        if(i.length>0)
                        {
                            setupData(aa,i);
                        }
                    }
                });
            }
            @Override
            public void error(String errorMessage) {
            }
        }, sample.this, Boolean.FALSE);

    }
    private void initializeHorizontalTabs() {
        LinearLayout ll = (LinearLayout) tabWidget.getParent();
        horizontalScrollView = new HorizontalScrollView(this);
        horizontalScrollView.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT));
        ll.addView(horizontalScrollView, 0);
        ll.removeView(tabWidget);
        horizontalScrollView.addView(tabWidget);
        horizontalScrollView.setHorizontalScrollBarEnabled(false);
    }

    private void scrollToCurrentTab() {
        final int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        final int leftX = tabWidget.getChildAt(tabHost.getCurrentTab()).getLeft();
        int newX = 0;

        newX = leftX + (tabWidget.getChildAt(tabHost.getCurrentTab()).getWidth() / 2) - (screenWidth / 2);
        if (newX < 0) {
            newX = 0;
        }
        horizontalScrollView.scrollTo(newX, 0);
    }
    private void initializeTabs(String[] aa) {
      //  tabs = new String[] { "TV Shows", "Movies", "Music", "News", "Weather" };
        tabs = aa;
    }

    private void setupTabHost() {

        for(int i=0; i<tabs.length; i++) {
            tabHost.addTab(tabHost.newTabSpec(String.format("%sTab", tabs[i].replace(" ","").toLowerCase())).setIndicator(tabs[i]), MyFragment.class, null);
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {

    }

    private void CATdetail() {
        final RequestInterface requestInterface = ApiFactory.createService(sample.this, RequestInterface.class);
        ErrorCallback.MyCall<brandpromodel> myCall;
        myCall = requestInterface.branprofiles(id);
        myCall.enqueue(new ErrorCallback.MyCallback<brandpromodel>() {
            @Override
            public void success(final retrofit2.Response<brandpromodel> res) {
                sample.this.runOnUiThread(new  Runnable() {
                    @Override
                    public void run() {
                        name.setText(res.body().getData().getUsername());
                        Picasso.with(context).load(res.body().getData().getAvatar()).error(R.drawable.app_icon).into(img_header_bg);

                    }
                });
            }
            @Override
            public void error(String errorMessage) {
            }
        }, sample.this, Boolean.FALSE);

    }
}
