package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Mycode.CommonMethod;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Responsemodel.forgotmodel;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class forgotpass extends Activity {
    // TextInputLayout inputfname
    LinearLayout social, emaillogin, l1, l2;
    TextInputEditText name;
    Button signup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpass);
        name=(TextInputEditText)findViewById(R.id.name);
        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        signup=(Button) findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgot();
            }
        });

    }
    private void forgot() {
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(forgotpass.this, RequestInterface.class);
        RequestBody Email = RequestBody.create(okhttp3.MultipartBody.FORM, name.getText().toString().trim());

        Call<forgotmodel> call = requestInterface.forgot(Email);
        call.enqueue(new Callback<forgotmodel>() {
            @Override
            public void onResponse(Call<forgotmodel> call, final Response<forgotmodel> response) {

                if (response.body().getMessage().toString().equals("success")) {
                    CommonMethod.showAlert(response.body().getData().getMessage().toString().trim(),  forgotpass.this);
                    //  SaveSharedPreference.setToken(loginwithmobile.this,response.body().getData().getToken());
                } else {

                }
            }
            @Override
            public void onFailure(Call<forgotmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}