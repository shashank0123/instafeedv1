package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Adpater.Search_Adapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;

import com.abacusdesk.instafeed.instafeed.Models.BrandN;
import com.abacusdesk.instafeed.instafeed.Models.DataN;
import com.abacusdesk.instafeed.instafeed.Models.DataPeople;
import com.abacusdesk.instafeed.instafeed.Models.NewsN;
import com.abacusdesk.instafeed.instafeed.Models.PeopleModel;
import com.abacusdesk.instafeed.instafeed.Models.Searchmodel;
import com.abacusdesk.instafeed.instafeed.Models.StarN;
import com.abacusdesk.instafeed.instafeed.R;

import java.util.ArrayList;
import java.util.HashMap;

import fontsClass.SemiBoldTextview;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Seachactivity extends Activity {
EditText edSearch;
String type = "news";
    Search_Adapter mSearch_Adapter;
    RecyclerView recyclerView;
    ArrayList<NewsN> citizenData ;
    ArrayList<BrandN> brandData ;
    ArrayList<StarN> starData ;
    ArrayList<DataPeople> peopleData ;

    ArrayList<HashMap<String,String>> hashMaps;
    LinearLayout searchoptionLayout;
    TextView no_post;
    SemiBoldTextview peopleSearch,brandSearch,citizenSearch,starSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        edSearch=(EditText)findViewById(R.id.edSearch);
        citizenData = new ArrayList<>();
        brandData = new ArrayList<>();
        starData = new ArrayList<>();
        peopleData = new ArrayList<>();
        hashMaps  = new ArrayList<>();
        findViewById(R.id.ivBackArrow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        peopleSearch = (SemiBoldTextview) findViewById(R.id.people_text);
        brandSearch = (SemiBoldTextview) findViewById(R.id.brand_text);
        starSearch = (SemiBoldTextview) findViewById(R.id.star_text);
        citizenSearch = (SemiBoldTextview) findViewById(R.id.citizen_text);
        no_post = (TextView) findViewById(R.id.no_post_text);

        brandSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brandSearch.setBackground(getDrawable(R.drawable.button_bg_search));
                starSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                citizenSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                peopleSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                setAdapter(2);
            }
        });
        starSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                brandSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                starSearch.setBackground(getDrawable(R.drawable.button_bg_search));
                citizenSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                peopleSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                setAdapter(3);

            }
        });
        citizenSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brandSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                starSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                citizenSearch.setBackground(getDrawable(R.drawable.button_bg_search));
                peopleSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                setAdapter(1);

            }
        });
        peopleSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brandSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                starSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                citizenSearch.setBackground(getDrawable(R.drawable.button_bg_search_unselect));
                peopleSearch.setBackground(getDrawable(R.drawable.button_bg_search));
                setAdapter(4);
              //  mSearch_Adapter.notifyDataSetChanged();

            }
        });

        searchoptionLayout = (LinearLayout) findViewById(R.id.search_options_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        mSearch_Adapter = new Search_Adapter(Seachactivity.this,hashMaps);
        recyclerView.setAdapter(mSearch_Adapter);
        layoutManager.setReverseLayout(false);
        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            @Override
            public void afterTextChanged(Editable editable) {

                try {
                    if (edSearch.getText().toString().trim().length() >= 3) {
                        SearchNews(1);
                        SearchPoeple(4);
                    } else {
                        setAdapter(5);
                       // Search();
                    }
                } catch (NumberFormatException npe) {
                    npe.printStackTrace();
                }
            }
        });

    }

    private void setAdapter(int id )
    {
        if(id ==1)
        {
            hashMaps.clear();
            for(NewsN newsN : citizenData) {
                HashMap<String,String> dataMap = new HashMap<>();
                dataMap.put("Type","Citi");
                dataMap.put("id",newsN.getId());
                dataMap.put("title", newsN.getTitle());
                dataMap.put("description", newsN.getDtAdded());
                hashMaps.add(dataMap);
            }

        }else if(id == 2)
        {
            hashMaps.clear();
            for(BrandN newsN : brandData) {
                HashMap<String,String> dataMap = new HashMap<>();

                dataMap.put("id",newsN.getId());
                dataMap.put("Type","Brand");
                dataMap.put("title", newsN.getTitle());
                dataMap.put("description", newsN.getDtAdded());
                hashMaps.add(dataMap);
            }
        }else if(id == 3)
        {
            hashMaps.clear();
            for(StarN newsN : starData) {
                HashMap<String,String> dataMap = new HashMap<>();
                dataMap.put("id",newsN.getId());
                dataMap.put("Type","Star");
                dataMap.put("title", newsN.getTitle());
                dataMap.put("description", newsN.getDtAdded());
                hashMaps.add(dataMap);
            }
        }else if(id==4)
        {
            hashMaps.clear();
            for(DataPeople newsN : peopleData) {
                Log.e("newsN", "peopleData..."+newsN.getFirst_name());
                HashMap<String,String> dataMap = new HashMap<>();

                dataMap.put("id",newsN.getId());
                dataMap.put("Type","users");
                dataMap.put("first_name", newsN.getFirst_name());
                dataMap.put("username", newsN.getUsername());
                dataMap.put("avatar", newsN.getAvatar());
                hashMaps.add(dataMap);
            }
//            Log.e("newsN", "peopleData final..."+  hashMaps.get(0).get("first_name") );
  //          Log.e("newsN", "peopleData final..."+  hashMaps.get(1).get("first_name") );

            mSearch_Adapter.notifyDataSetChanged();
        }else if(id==5)
        {
            hashMaps.clear();
        }
        if(hashMaps.size()>0)
        {
            no_post.setVisibility(View.GONE);

        }else
        {
            no_post.setVisibility(View.VISIBLE);
        }
        mSearch_Adapter.notifyDataSetChanged();

    }
    private synchronized void SearchPoeple(final int i) {
        type="users";

        RequestInterface requestInterface = ApiFactory.createService(Seachactivity.this, RequestInterface.class);
        RequestBody Qwery=RequestBody.create(MultipartBody.FORM,edSearch.getText().toString().trim());
        RequestBody TYPE=RequestBody.create(MultipartBody.FORM,type);
        Call<PeopleModel> call = requestInterface.SearchPeople(Qwery,TYPE);

        call.enqueue(new Callback<PeopleModel>() {
            @Override
            public void onResponse(Call<PeopleModel> call, final Response<PeopleModel> response) {
                Log.e("response",""+response.raw());
                if(response.isSuccessful()){
                    ArrayList<DataPeople> dataN = response.body().getData();
                    Log.e("response","DataPeople size: "+dataN.size());
                    for(DataPeople dataPeople : dataN)
                    {

                        Log.e("response","DataPeople first name: "+dataPeople.getFirst_name());
                    }
                    peopleData.clear();
                    peopleData.addAll(dataN);

                    setAdapter(i);
                }else {

                }
            }
            @Override
            public void onFailure(Call<PeopleModel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
    private void SearchNews(final int i) {
        type="news";

        RequestInterface requestInterface = ApiFactory.createService(Seachactivity.this, RequestInterface.class);
        RequestBody Qwery=RequestBody.create(MultipartBody.FORM,edSearch.getText().toString().trim());
        RequestBody TYPE=RequestBody.create(MultipartBody.FORM,type);
        Call<Searchmodel> call = requestInterface.Search(Qwery,TYPE);
        call.enqueue(new Callback<Searchmodel>() {
            @Override
            public void onResponse(Call<Searchmodel> call, final Response<Searchmodel> response) {
                Log.e("response",""+response.raw());
                if(response.isSuccessful()){
                    DataN dataN = response.body().getData();
                    Log.e("response","getBrands "+dataN.getBrands().size());
                    Log.e("response","getNews "+dataN.getNews().size());
                    Log.e("response","getStar "+dataN.getStar().size());
                    brandData.clear();
                    citizenData.clear();
                    starData.clear();

                    if(dataN.getBrands().size()>0)
                    {
                        brandData.addAll(dataN.getBrands());
                    }
                    if(dataN.getNews().size()>0)
                    {
                        citizenData.addAll(dataN.getNews());
                    }
                    if(dataN.getStar().size()>0)
                    {
                        starData.addAll(dataN.getStar());
                    }
                    setAdapter(i);
                }else {

                }
            }
            @Override
            public void onFailure(Call<Searchmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }


}