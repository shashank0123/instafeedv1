package com.abacusdesk.instafeed.instafeed.upload.helpers;

import android.net.Uri;

public class MK23_VideoModel {
    Uri videoURI;

    public MK23_VideoModel(Uri videoURI) {
        this.videoURI = videoURI;
    }

    public Uri getVideoURI() {
        return videoURI;
    }
}
