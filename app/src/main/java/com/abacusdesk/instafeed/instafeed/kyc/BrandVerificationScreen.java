package com.abacusdesk.instafeed.instafeed.kyc;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.abacusdesk.instafeed.instafeed.Fragments.Brand;
import com.abacusdesk.instafeed.instafeed.R;

/**
 * Created by Instafeed2 on 8/23/2019.
 */

public class BrandVerificationScreen extends AppCompatActivity {
    ImageView back;
    LinearLayout signedAgreement_brand,panCard_brand,rssFeed_brand,websiteUrl_brand;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.brand_verification_screen);
        back = (ImageView)findViewById(R.id.back_brand);
        signedAgreement_brand = (LinearLayout)findViewById(R.id.signed_agreement_brand);
        panCard_brand = (LinearLayout)findViewById(R.id.pan_card_brand);
        rssFeed_brand = (LinearLayout)findViewById(R.id.rss_feed_brand);
        websiteUrl_brand = (LinearLayout)findViewById(R.id.website_url_brand);

        signedAgreement_brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BrandVerificationScreen.this,SignedAgreementActivity.class));

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        panCard_brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BrandVerificationScreen.this,BrandPanVerification.class));
            }
        });websiteUrl_brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BrandVerificationScreen.this,BrandSubmitUrl.class));

            }
        });rssFeed_brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BrandVerificationScreen.this,URLVerification.class));

            }
        });
    }
}
