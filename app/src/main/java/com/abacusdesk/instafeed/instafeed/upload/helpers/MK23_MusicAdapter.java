package com.abacusdesk.instafeed.instafeed.upload.helpers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


import com.abacusdesk.instafeed.instafeed.R;

import java.util.List;




public class MK23_MusicAdapter extends RecyclerView.Adapter<MK23_MusicAdapter.ViewHolder> {

    private Context context;
    private List<MK23_MusicModel> musicList;

    public MK23_MusicAdapter(Context context, List<MK23_MusicModel> musicList) {
        this.context = context;
        this.musicList = musicList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.music_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.musicName.setText(musicList.get(position).getMusicName());
        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeMusic(position);
            }
        });
    }

    private void removeMusic(int position) {
        musicList.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView musicName;
        ImageButton removeButton;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            musicName = itemView.findViewById(R.id.music_layout_text);
            removeButton = itemView.findViewById(R.id.music_layout_removeButton);
        }
    }
}
