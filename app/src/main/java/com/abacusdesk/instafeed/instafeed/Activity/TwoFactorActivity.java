package com.abacusdesk.instafeed.instafeed.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.abacusdesk.instafeed.instafeed.R;


/**
 * Created by Instafeed2 on 9/6/2019.
 */

public class TwoFactorActivity extends Activity {
    Switch authenticationAppSwitch,textMessageSwitch;
    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two_factor_layout);
        authenticationAppSwitch =(Switch) findViewById(R.id.authentication_app_switch);
        textMessageSwitch =(Switch) findViewById(R.id.text_message_switch);
        authenticationAppSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                } else {
                    // The toggle is disabled
                }
            }
        });
        textMessageSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                } else {
                    // The toggle is disabled
                }
            }
        });
    }
}
