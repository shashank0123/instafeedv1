package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class DataCitizen {
    @SerializedName("user_id")
        
    private String userId;
    @SerializedName("username")
      
    private String username;
    @SerializedName("type")
      
    private String type;
    @SerializedName("type_name")
      
    private String typeName;
    @SerializedName("profile_url")
      
    private String profileUrl;
    @SerializedName("email")
      
    private String email;
    @SerializedName("avatar")
      
    private String avatar;
    @SerializedName("first_name")
      
    private String firstName;
    @SerializedName("middle_name")
      
    private String middleName;
    @SerializedName("last_name")
      
    private String lastName;
    @SerializedName("birth_date")
      
    private String birthDate;
    @SerializedName("sex")
      
    private String sex;
    @SerializedName("phone")
      
    private String phone;
    @SerializedName("dt_added")
      
    private String dtAdded;
    @SerializedName("dt_modified")
      
    private String dtModified;
    @SerializedName("status")
      
    private String status;
    @SerializedName("lat")
      
    private String lat;
    @SerializedName("long")
      
    private String _long;
    @SerializedName("totals")
      
    private Totals totals;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDtAdded() {
        return dtAdded;
    }

    public void setDtAdded(String dtAdded) {
        this.dtAdded = dtAdded;
    }

    public String getDtModified() {
        return dtModified;
    }

    public void setDtModified(String dtModified) {
        this.dtModified = dtModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

    public Totals getTotals() {
        return totals;
    }

    public void setTotals(Totals totals) {
        this.totals = totals;
    }
}
