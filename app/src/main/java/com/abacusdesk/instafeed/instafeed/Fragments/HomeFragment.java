package com.abacusdesk.instafeed.instafeed.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abacusdesk.instafeed.instafeed.Activity.Notificationlistingclass;
import com.abacusdesk.instafeed.instafeed.Activity.Seachactivity;
import com.abacusdesk.instafeed.instafeed.Adpater.TabAdapter;
import com.abacusdesk.instafeed.instafeed.MainActivity;
import com.abacusdesk.instafeed.instafeed.R;

public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private OnFragmentInteractionListener mListener;
    ImageView img_back;
    ViewGroup header;
    ImageView menu,i;
    LinearLayout Search;
    int currentposition;
    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view =inflater.inflate(R.layout.fragment_home, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        header=(ViewGroup)view.findViewById(R.id.header);
        menu=(ImageView) header.findViewById(R.id.menu);
        i=(ImageView) header.findViewById(R.id.i);
        i.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Notificationlistingclass.class);
                startActivity(in);
            }
        });
        Search=(LinearLayout) header.findViewById(R.id.Search);
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Seachactivity.class);
                startActivity(in);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.Open();
            }
        });
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragment(new Citizen(), "Citizen");
        adapter.addFragment(new Superstar(), "Superstar");
        adapter.addFragment(new Brand(), "Brand");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("index",""+"onResume");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("index",""+"onPause");
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
