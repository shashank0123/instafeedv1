package com.abacusdesk.instafeed.instafeed.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.Adpater.followadapter;
import com.abacusdesk.instafeed.instafeed.Adpater.followlintadapter;
import com.abacusdesk.instafeed.instafeed.Api.ApiFactory;
import com.abacusdesk.instafeed.instafeed.Api.RequestInterface;
import com.abacusdesk.instafeed.instafeed.Models.followlistingmodel;
import com.abacusdesk.instafeed.instafeed.R;
import com.abacusdesk.instafeed.instafeed.Util.SaveSharedPreference;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Followers extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String token;
    private String mParam1;
    private String mParam2;
    RecyclerView recyclerView;
    followlintadapter mfollowlintadapter;
    ImageView back;
       /* @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                FacebookSdk.sdkInitialize(getApplicationContext());
                setContentView(R.layout.follow);*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.follow, container, false);
        Log.e("Follow","Follow");
        token= SaveSharedPreference.getToken(getActivity());
        back=(ImageView)view.findViewById(R.id.fo);

        recyclerView = (RecyclerView)view. findViewById(R.id.recy);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        follow();
        return  view;
    }



    private void follow() {
        Log.e("token",""+token);
        //RequestInterface requestInterface = ApiFactory.getClient().create(RequestInterface.class);
        RequestInterface requestInterface = ApiFactory.createService(getActivity(), RequestInterface.class);
        RequestBody Token=RequestBody.create(MultipartBody.FORM,token);

        Call<followlistingmodel> call = requestInterface.followwer(Token);
        call.enqueue(new Callback<followlistingmodel>() {
            @Override
            public void onResponse(Call<followlistingmodel> call, final Response<followlistingmodel> response) {
                if(response.isSuccessful()){
                    mfollowlintadapter = new followlintadapter(response.body().getData(),  getActivity());
                    recyclerView.setAdapter(mfollowlintadapter);
                    //CommonMethod.showAlert(response.body().getMessage().trim(),  ((Activity)context));
                }else {
                    // CommonMethod.showAlert(response.body().getMessage().trim(), ((Activity)context));
                }
            }
            @Override
            public void onFailure(Call<followlistingmodel> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }
}
