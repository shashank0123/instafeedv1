package com.abacusdesk.instafeed.instafeed.kyc;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.abacusdesk.instafeed.instafeed.R;

/**
 * Created by Instafeed2 on 8/23/2019.
 */

public class BrandSubmitUrl extends Activity {
    ImageView ok,cancel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.website_url_verfication);

        ok = (ImageView)findViewById(R.id.ok);
        cancel = (ImageView)findViewById(R.id.cancel);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
