package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class DataF {

    @SerializedName("users_id")
    
    private String usersId;
    @SerializedName("follow_users_id")
    
    private String followUsersId;

    public String getUsersId() {
        return usersId;
    }

    public void setUsersId(String usersId) {
        this.usersId = usersId;
    }

    public String getFollowUsersId() {
        return followUsersId;
    }

    public void setFollowUsersId(String followUsersId) {
        this.followUsersId = followUsersId;
    }
}
