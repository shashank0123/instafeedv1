package com.abacusdesk.instafeed.instafeed.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SaveSharedPreference {
    static final String TOKEN_ID = "tokenid";
    static final String PREF_USER_NAME = "username";
    static final String PREF_LANGTYPE = "langtype";
    static final String PREF_LAST_NAME = "lastname";
    static final String PREF_USER_MOBILE = "mobile";
    static final String PREF_USER_EMAIL = "email";
    static final String PREF_USER_IMAGE = "image";
    static final String PREF_USER_AUTH = "place";
    static final String PREF_USER_ID = "id";
    static final String PREF_FIRSTNAME= "firstname";
    static final String PREF_GENDER="gender";
    static final String PREF_MOBILECODE="mobilecode";
    static final String PREF_MOBILENUMBER="5465465";
    static final String PREF_TOKEN="token";
    static final String IS_Welcome="iswelcom";
    static final String REWADS_POINTS="REWADSPOINTS";

    static final String FOLLOWERS="followers";
    static final String FOLLOWING="following";
    static final String MOBILE_LOGIN="mobilelogin";
    static final String LANGSTATUS="langstatus";

    static final String IS_SPLASH="issplash";



    public static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }


    public static void setToken(Context ctx, String tokenid) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(TOKEN_ID, tokenid);
        editor.commit();
    }

    public static String getToken(Context ctx) {
        return getSharedPreferences(ctx).getString(TOKEN_ID, "");
    }



    public static String getRewadsPoints(Context ctx) {
        return getSharedPreferences(ctx).getString(REWADS_POINTS, "");
    }


    public static void setLangstatus(Context ctx, String mobilelogin) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(LANGSTATUS, mobilelogin);
        editor.commit();
    }

    public static String getLangstatus(Context ctx) {
        return getSharedPreferences(ctx).getString(LANGSTATUS, "");
    }








    public static void setRewadsPoints(Context ctx, String REWADSPOINTS) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(REWADS_POINTS, REWADSPOINTS);
        editor.commit();
    }

    public static String getPrefToken(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_TOKEN, "");
    }

    public static void setPrefToken(Context ctx, String token) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_TOKEN, token);
        editor.commit();
    }

    public static void setMobileLogin(Context ctx, String mobilelogin) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(MOBILE_LOGIN, mobilelogin);
        editor.commit();
    }

    public static String getMobileLogin(Context ctx) {
        return getSharedPreferences(ctx).getString(MOBILE_LOGIN, "");
    }

    public static void setIsSplash(Context ctx, String issplash) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(IS_SPLASH, issplash);
        editor.commit();
    }
    public static void setIsSplashL(Context ctx, String issplash) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(IS_SPLASH, issplash);
        editor.commit();
    }

    public static void setPREF_langtype(Context ctx, String PREF_langtype) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_LANGTYPE, PREF_langtype);
        editor.commit();
    }

    public static String getPREF_langtype(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_LANGTYPE, "");
    }


    public static String getIsSplashL(Context ctx) {
        return getSharedPreferences(ctx).getString(IS_SPLASH, "");
    }
    public static String getIsSplash(Context ctx) {
        return getSharedPreferences(ctx).getString(IS_SPLASH, "");
    }
    public static void setFollowing(Context ctx, String following) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(FOLLOWING, following);
        editor.commit();
    }

    public static String getFollowing(Context ctx) {
        return getSharedPreferences(ctx).getString(FOLLOWING, "");
    }

    public static void setFollowers(Context ctx, String folloewers) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(FOLLOWERS, folloewers);
        editor.commit();
    }

    public static String getFollowers(Context ctx) {
        return getSharedPreferences(ctx).getString(FOLLOWERS, "");
    }

    public static void setFirstName(Context ctx, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_FIRSTNAME, userName);
        editor.commit();
    }

    public static String getFirstName(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_FIRSTNAME, "");
    }

    public static void setGender(Context ctx, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_GENDER, userName);
        editor.commit();
    }

    public static String getGender(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_GENDER, "");
    }
    public static void setMobilecode(Context ctx, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_MOBILECODE, userName);
        editor.commit();
    }

    public static String getMobilecode(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_MOBILECODE, "");
    }

    public static void setMobileNum(Context ctx, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_MOBILENUMBER, userName);
        editor.commit();
    }

    public static String getMobileNum(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_MOBILENUMBER, "");
    }


    public static void setUserName(Context ctx, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static String getUserName(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static void setLastName(Context ctx, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_LAST_NAME, userName);
        editor.commit();
    }

    public static String getLastName(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_LAST_NAME, "");
    }

    public static void setMobile(Context ctx, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_MOBILE, userName);
        editor.commit();
    }

    public static String getMobile(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_MOBILE, "");
    }

    public static void setUserID(Context ctx, String userid) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_ID, userid);
        editor.commit();
    }

    public static String getUserID(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_ID, "");
    }


    public static void setUserEMAIL(Context ctx, String useremail) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_EMAIL, useremail);
        editor.commit();
    }

    public static String getUserEMAIL(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_EMAIL, "");
    }

    public static void setUserIMAGE(Context ctx, String userimage) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_IMAGE, userimage);
        editor.commit();
    }

    public static String getUserIMAGE(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_IMAGE, "");
    }


    public static void setUSERAuth(Context ctx, String place) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_AUTH, place);
        editor.commit();
    }

    public static String getUSERAuth(Context ctx) {
        return getSharedPreferences(ctx).getString(PREF_USER_AUTH, "");
    }

}