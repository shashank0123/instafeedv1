package com.abacusdesk.instafeed.instafeed.Models;

import com.google.gson.annotations.SerializedName;

public class DataPro {
    @SerializedName("user_id")
        
    private String userId;
    @SerializedName("username")
        
    private String username;
    @SerializedName("profile_url")
        
    private String profileUrl;
    @SerializedName("email")
        
    private String email;
    @SerializedName("avatar")
        
    private String avatar;
    @SerializedName("first_name")
        
    private String firstName;
    @SerializedName("middle_name")
        
    private String middleName;
    @SerializedName("last_name")
        
    private String lastName;
    @SerializedName("birth_date")
        
    private String birthDate;
    @SerializedName("sex")
        
    private String sex;
    @SerializedName("phone")
        
    private String phone;
    @SerializedName("dt_added")
        
    private String dtAdded;
    @SerializedName("dt_modified")
        
    private String dtModified;
    @SerializedName("status")
        
    private String status;
    @SerializedName("group_id")
        
    private String groupId;
    @SerializedName("email_verified")
        
    private String emailVerified;
    @SerializedName("expired_at")
        
    private String expiredAt;
    @SerializedName("created_at")
        
    private String createdAt;
    @SerializedName("updated_at")
        
    private String updatedAt;
    @SerializedName("location_id")
        
    private String locationId;
    @SerializedName("bank_name")
        
    private String bankName;
    @SerializedName("bank_acc_no")
        
    private String bankAccNo;
    @SerializedName("bank_acc_type")
        
    private String bankAccType;
    @SerializedName("bank_ifsc_code")
        
    private String bankIfscCode;
    @SerializedName("city")
        
    private String city;
    @SerializedName("pincode")
        
    private String pincode;
    @SerializedName("address")
        
    private String address;
    @SerializedName("bio")
        
    private String bio;
    @SerializedName("website")
        
    private String website;
    @SerializedName("totals")
        
    private Totals totals;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDtAdded() {
        return dtAdded;
    }

    public void setDtAdded(String dtAdded) {
        this.dtAdded = dtAdded;
    }

    public String getDtModified() {
        return dtModified;
    }

    public void setDtModified(String dtModified) {
        this.dtModified = dtModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(String emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getExpiredAt() {
        return expiredAt;
    }

    public void setExpiredAt(String expiredAt) {
        this.expiredAt = expiredAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getBankAccType() {
        return bankAccType;
    }

    public void setBankAccType(String bankAccType) {
        this.bankAccType = bankAccType;
    }

    public String getBankIfscCode() {
        return bankIfscCode;
    }

    public void setBankIfscCode(String bankIfscCode) {
        this.bankIfscCode = bankIfscCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Totals getTotals() {
        return totals;
    }

    public void setTotals(Totals totals) {
        this.totals = totals;
    }
}
