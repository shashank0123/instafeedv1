package fontsClass;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class SemiBoldTextview extends TextView {

    public SemiBoldTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SemiBoldTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SemiBoldTextview(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Medium.ttf");
        setTypeface(tf);
    }

}