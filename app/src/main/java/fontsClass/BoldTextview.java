package fontsClass;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class BoldTextview extends TextView {

    public BoldTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public BoldTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoldTextview(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Bold.ttf");
        setTypeface(tf);
    }

}