package fontsClass;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class Regulartextview  extends TextView {

    public Regulartextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Regulartextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Regulartextview(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Regular.ttf");
        setTypeface(tf);
    }

}